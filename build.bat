rmdir /Q /S dist\necromanzer_windows
python -O -m PyInstaller -F --distpath=dist\necromanzer_windows --windowed --icon=res/img/icon.ico necromanzer.spec
xcopy /E /I res dist\necromanzer_windows\res
cd dist
jar -cMf necromanzer_windows.zip necromanzer_windows