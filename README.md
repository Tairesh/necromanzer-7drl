Running from source
===================
1. Install python3, pip and sdl2
2. Run `pip install -r requirements.txt`
3. Run `python src/necromanzer.py`

Building
========
* For linux use `./build.sh`
* For windows use `build.bat`