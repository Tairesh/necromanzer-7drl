# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['src/necromanzer.py'],
             pathex=['/home/ilya/Dev/necromanzer-7drl'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=['setuptools', 'urllib2', 'http', 'pydoc', '_osx_support',
                       'unicodedata', 'email', 'html', 'xml', 'bz2', 'pydoc', 'doctest', 'zipfile', 'locale'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='necromanzer',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False,
          icon="res/img/icon.ico")
