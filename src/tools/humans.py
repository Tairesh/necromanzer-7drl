import random

import engine
from engine.body import HumanBody, ChildBody
from brains.human import HumanBrain
from engine.consts import GAME_DATA


def generate_person_data(alive=True, gender=None, min_born=None, max_born=None, current_year = None) -> dict:
    if current_year is None:
        current_year = 330 if engine.world is None else engine.world.current_year

    if gender is None:
        gender = random.randint(0, 1)
    if random.random() > 0.99:
        skin_tone = 0
    else:
        skin_tone = random.randint(1, 3)
    if random.random() > 0.99:
        preferred_hand = 2
    elif random.random() > 0.9:
        preferred_hand = 1
    else:
        preferred_hand = 0
    first_name = random_name(gender)
    last_name = random_name()
    if alive:
        born = random.randint(current_year-50 if min_born is None else min_born, current_year-3 if max_born is None else max_born)
        death = None
        lived = current_year - born
    else:
        born = random.randint(current_year-180 if min_born is None else min_born, current_year if max_born is None else max_born)
        lived = random.randint(0, 50)
        if born + lived > current_year:
            lived = current_year - born
        death = born + lived

    if lived >= 16:
        profession = random.choice(GAME_DATA['professions'][gender])
    elif lived >= 6:
        profession = random.choice(GAME_DATA['children_professions'][gender])
    else:
        profession = 'boy' if gender == 0 else 'girl'

    return {
        'name': first_name + ' ' + last_name,
        'race': 'human',
        'gender': gender,
        'skin_tone': skin_tone,
        'preferred_hand': preferred_hand,
        'born': born,
        'death': death,
        'profession': profession,
    }


def random_name(gender=0):
    return random.choice(GAME_DATA['names'][gender][random.choice(tuple(GAME_DATA['names'][gender].keys()))])


def random_villager(pos):
    unit_data = generate_person_data()
    unit_data["alive"] = True
    unit_data["body_class"] = HumanBody if 330 - unit_data['born'] >= 16 else ChildBody
    unit_data["brain"] = (HumanBrain.proto, None)
    from engine.unit import Unit
    return Unit(pos, unit_data)
