import random

from engine import consts
from engine.body import DogBody, RottenDogBody, SkeletonDogBody
from engine.brain import BrainProto
from engine.unit import Unit


def random_dog(pos, gender=None):
    if gender is None:
        gender = 0 if random.random() > 0.5 else 1

    dog = Unit(pos, {
        'name': random.choice(consts.GAME_DATA['dog_names'][gender]),
        'race': 'dog',
        'gender': gender,
        'skin_tone': 3,
        'born': 328,
        'body_class': DogBody,
        'alive': True,
        'brain': (BrainProto.DOG, None),
        'wield': {}
    })
    return dog


def random_daemon_dog(pos):
    gender = 0 if random.random() > 0.5 else 1
    born = random.randint(270, 328)
    lived = random.randint(2, 12)
    death = born + lived
    if death > 330:
        death = 330

    dog = Unit(pos, {
        'name': random.choice(consts.GAME_DATA['dog_names'][gender]),
        'race': 'dog',
        'gender': gender,
        'skin_tone': 3,
        'born': born,
        'death': death,
        'body_class': RottenDogBody if death == 330 else SkeletonDogBody,
        'alive': True,
        'brain': (BrainProto.SMALL_DAEMON, None),
        'wield': {}
    })
    return dog
