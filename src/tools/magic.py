import tcod

import engine
from brains.zombie import ZombieBrain


def animate_dead_body(target):
    # TODO refactor this
    from engine.unit import Unit
    unit: Unit = target

    can_be_animated = True
    for part_key in unit.critical_bodyparts:
        if part_key not in unit.body.parts or not unit.body.parts[part_key]:
            can_be_animated = False
            break

    if can_be_animated:
        unit.alive = True
        unit.brain = ZombieBrain(unit)
        engine.log.add_line(f"{unit.name} stands up!", tcod.purple)
    else:
        engine.log.add_line(f"{unit.name} beats in a fit! Seems like it can't be animated anymore...", tcod.orange)
