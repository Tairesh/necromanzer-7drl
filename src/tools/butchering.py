import tcod
import engine


def cut_off_body_part(unit, part_key):
    part = unit.body.parts[part_key]
    if part:
        unit.body.parts[part_key] = None
        tile = engine.world.get_tile(unit.pos)
        # tile.items.append(part)
        engine.world.player.inventory.append(part)

        if part_key in unit.wield:
            weapon = unit.wield[part_key]
            if weapon:
                unit.wield[part_key] = None
                tile.items.append(weapon)

        engine.log.add_line(f"You cutting {part_key} off the {unit.name_apostrophe_s} body and adding it to your inventory",
                            tcod.lighter_azure)

        if part_key in unit.critical_bodyparts:
            unit.die()
    else:
        unit.body.parts.pop(part_key)


def insert_body_part(unit, part_key, item, item_pos):
    unit.body.parts[part_key] = item
    if item_pos:
        tile = engine.world.get_tile(item_pos)
        tile.items.remove(item)
    else:
        engine.world.player.inventory.remove(item)

    engine.log.add_line(f"You inserting {item.title} as {part_key} into {unit.name_apostrophe_s} body!", tcod.lighter_azure)


def replace_body_part(unit, part_key, item, item_pos):
    unit.body.parts[part_key] = item
    if item_pos:
        tile = engine.world.get_tile(item_pos)
        tile.items.remove(item)
    else:
        engine.world.player.inventory.remove(item)

    engine.log.add_line(f"You replacing {unit.name_apostrophe_s} {part_key} by {item.title}!", tcod.lighter_azure)
