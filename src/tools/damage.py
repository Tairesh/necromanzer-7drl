import random
from typing import Optional

import tcod

import engine
from engine.brain import Fraction
from engine.item import Item
from engine.tile import Tile, TileBase
from items.bodyparts import BodyPart


def smash_tile(unit, pos, part_key: str):
    part = unit.body.parts[part_key]
    if not part:
        return
    tile = engine.world.get_tile(pos)
    weapon = unit.wield[part_key] if part_key in unit.wield and unit.wield[part_key] else None

    attack_type = target_label = None
    if len(tile.units):
        target_unit = random.choice(tile.units)
        possible_target_keys = [key for key, part in target_unit.body.parts.items() if part and part.hp > 0]
        if len(possible_target_keys):
            target_key = random.choice(possible_target_keys)
            target_label = target_unit.name_apostrophe_s + ' ' + target_key
            attack_type = 'to_unit'
    elif len(tile.items):
        target_item = random.choice(tile.items)
        target_label = target_item.title
        attack_type = 'to_item_on_ground'

    if attack_type is None:
        target_label = tile.base.title
        attack_type = 'to_ground'

    attack_message = f"{unit.name} {weapon.attack_verb if weapon else part.attack_verb} {target_label}"
    if weapon:
        attack_message += f" with {weapon.title} ({part_key})"
    else:
        attack_message += f" by {part_key if part.attack_verb_title is None else part.attack_verb_title}!"

    if unit is engine.world.player or unit.brain is None:
        attack_color = tcod.chartreuse
    elif unit.brain.fraction == Fraction.ZOMBIE:
        attack_color = tcod.purple
    elif unit.brain.fraction in engine.world.player.brain.fraction.enemies and attack_type == 'to_unit' and \
            target_unit.brain and target_unit.brain.fraction not in engine.world.player.brain.fraction.enemies:
        attack_color = tcod.red
    else:
        attack_color = tcod.white
    engine.log.add_line(attack_message, attack_color)

    if attack_type == 'to_item_on_ground':
        # noinspection PyUnboundLocalVariable
        make_damage_to_item_on_ground(tile, target_item, part, weapon)
    elif attack_type == 'to_unit':
        # noinspection PyUnboundLocalVariable
        make_damage_to_unit(target_unit, target_key, part, weapon)
    elif attack_type == 'to_ground':
        make_damage_to_ground(tile, part, weapon)


def make_damage_to_item_on_ground(tile: Tile, item: Item, part: BodyPart, weapon: Optional[Item]):
    damage = weapon.attack_damage if weapon else part.attack_damage
    item.hp -= damage
    if item.hp <= 0:
        tile.items.remove(item)
        engine.log.add_line(f"{item.title.capitalize()} on the {tile.base.title} destroyed!", tcod.light_red)


def make_damage_to_unit(target_unit, target_key: str, part: BodyPart, weapon: Optional[Item]):
    damage = weapon.attack_damage if weapon else part.attack_damage
    target = target_unit.body.parts[target_key]
    if target:
        target.hp -= damage
        if target.hp <= 0:
            target_unit.body.parts[target_key] = None
            if target_key in target_unit.wield:
                weapon = target_unit.wield[target_key]
                if weapon:
                    tile = engine.world.get_tile(target_unit.pos)
                    tile.items.append(weapon)
                target_unit.wield.pop(target_key)
            engine.log.add_line(f"{target_unit.name_apostrophe_s} {target_key} destroyed!", tcod.light_red)
            if target_unit.alive and target_key in target_unit.critical_bodyparts:
                target_unit.die()


def make_damage_to_ground(tile: Tile, part: BodyPart, weapon: Optional[Item]):
    damage = weapon.attack_damage if weapon else part.attack_damage
    if tile.base == TileBase.FENCE and damage > 5:
        tile.base = TileBase.FENCE_BROKEN
        engine.log.add_line(f"Fence destroyed!", tcod.yellow)
    elif tile.base == TileBase.STONE_WALL_WITH_WINDOW and damage > 10:
        tile.base = TileBase.STONE_WALL_WITH_BROKEN_WINDOW
        engine.log.add_line(f"Window in stone wall destroyed!", tcod.yellow)
    elif tile.base == TileBase.WOODEN_WALL_WITH_WINDOW and damage > 5:
        tile.base = TileBase.WOODEN_WALL_WITH_BROKEN_WINDOW
        engine.log.add_line(f"Window in wooden wall destroyed!", tcod.yellow)
