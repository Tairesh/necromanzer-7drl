import random

import tcod
import engine
from items.common import GraveStone
from engine.body import RottenHumanBody, SkeletonHumanBody, RottenChildBody, SkeletalChildBody
from .humans import generate_person_data


def spawn_dead_body_from_gravestone(pos, gravestone: GraveStone):
    gravestone.is_grabable = True
    unit_data = gravestone.raw_data['person'].copy()
    unit_data['alive'] = False
    lived = unit_data['death'] - unit_data['born']
    rotten = engine.world.current_year == gravestone.raw_data['person']['death']
    if lived <= 3:
        engine.log.add_line(f"You finished excavation of {unit_data['name']} but body was too small and nothing remains", tcod.orange)
        return
    elif lived < 16:
        unit_data['body_class'] = RottenChildBody if rotten else SkeletalChildBody
    else:
        unit_data["body_class"] = RottenHumanBody if rotten else SkeletonHumanBody

    from engine.unit import Unit
    dummy = Unit(pos, unit_data)
    engine.log.add_line(f"You finished excavation of {dummy.name}", tcod.light_azure)
    engine.world.add_unit(dummy)


persons_for_graveyard = None


def person_for_graveyard(world=None):
    if world is None:
        world = engine.world
    global persons_for_graveyard
    if persons_for_graveyard is None:
        persons_for_graveyard = set([person for person in world.historical_persons.values() if person.death])
    if len(persons_for_graveyard) > 0:
        return persons_for_graveyard.pop().data
    return generate_person_data(False)


def generate_gravestone(world=None) -> GraveStone:
    person = person_for_graveyard(world)
    return GraveStone({
        'text': f"R.I.P. {person['profession']} {person['name']}, {person['born']}-{person['death']}",
        'person': person,
    })
