import random

from engine.chunk import Chunk, CHUNK_SIZE
from engine.tile import Tile, TileBase
from items.tools import PitchFork
from . import road
from .common import ground_tile, Structure


def generate_tile(x, y, structures) -> Tile:
    fields_chunks = structures[Structure.FIELD]
    houses_chunks = structures[Structure.HOUSE]
    graveyards_chunks = structures[Structure.GRAVEYARD]
    road_chunks = structures[Structure.ROAD]
    chunk_x, chunk_y = x//CHUNK_SIZE, y//CHUNK_SIZE
    chunk_left, chunk_top = chunk_x*CHUNK_SIZE, chunk_y*CHUNK_SIZE
    chunk_right, chunk_bottom = chunk_left + CHUNK_SIZE - 1, chunk_top + CHUNK_SIZE - 1
    left = (chunk_x-1, chunk_y) in fields_chunks or (chunk_x-1, chunk_y) in houses_chunks or (chunk_x-1, chunk_y) in graveyards_chunks
    right = (chunk_x+1, chunk_y) in fields_chunks or (chunk_x+1, chunk_y) in houses_chunks or (chunk_x+1, chunk_y) in graveyards_chunks
    top = (chunk_x, chunk_y-1) in fields_chunks or (chunk_x, chunk_y-1) in houses_chunks or (chunk_x, chunk_y-1) in graveyards_chunks
    bottom = (chunk_x, chunk_y+1) in fields_chunks or (chunk_x, chunk_y+1) in houses_chunks or (chunk_x, chunk_y+1) in graveyards_chunks
    left_road = (chunk_x-1, chunk_y) in road_chunks
    right_road = (chunk_x+1, chunk_y) in road_chunks
    top_road = (chunk_x, chunk_y-1) in road_chunks
    bottom_road = (chunk_x, chunk_y+1) in road_chunks

    if (x == chunk_left and left_road) or (x == chunk_right and right_road):
        if y-chunk_top < 5 or chunk_bottom-y < 5:
            return Tile((x, y), TileBase.FENCE if random.random() > 0.1 else TileBase.FENCE_BROKEN)
        else:
            return road.generate_tile(x, y, structures)
    elif (y == chunk_top and top_road) or (y == chunk_bottom and bottom_road):
        if x-chunk_left < 5 or chunk_right-x < 5:
            return Tile((x, y), TileBase.FENCE if random.random() > 0.1 else TileBase.FENCE_BROKEN)
        else:
            return road.generate_tile(x, y, structures)
    elif (x == chunk_left and not left)\
        or (x == chunk_right and not right)\
        or (y == chunk_top and not top)\
        or (y == chunk_bottom and not bottom):
        tile = Tile((x, y), TileBase.FENCE if random.random() > 0.1 else TileBase.FENCE_BROKEN)
        return tile
    else:
        if x % 4 <= 1 or random.random() > 0.9:
            tile = ground_tile(x, y)
        else:
            tile = Tile((x, y), TileBase.ARABLE)
        if random.random() > 0.999:
            tile.items.append(PitchFork())
        return tile


def generate_chunk(chunk_x, chunk_y, structures) -> Chunk:
    chunk_left, chunk_top = chunk_x*CHUNK_SIZE, chunk_y*CHUNK_SIZE
    tiles = []
    for i in range(CHUNK_SIZE):
        for j in range(CHUNK_SIZE):
            tile = generate_tile(chunk_left + i, chunk_top + j, structures)
            tiles.append(tile)
    return Chunk((chunk_x, chunk_y), tiles)
