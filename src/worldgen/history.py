import json
import random
from typing import List

from engine import consts
from engine.consts import GAME_DATA
from tools.humans import generate_person_data

NAME_VILLAGE_START = ("Ho", "Kho", "Lo", "Oh", "New", "Ed", "Nor", "Dub", "Be", "Lee", "Man", "Ra", "Ree", "Ad")
NAME_VILLAGE_MIDDLE = ("", "", "", "", "", "", "gh", "ng", "nd", "in", "che", "ver", "l")
NAME_VILLAGE_END = ("smith", "don", "ford", "castle", "burg", "viz", "weez", "inn", "fast", "pool", "ster", "vill")


last_id = 0


def unique_id():
    global last_id
    last_id += 1
    return last_id


class Person:
    def __init__(self, data):
        self.id = unique_id()
        self.name = data['name']
        self.race = data['race']
        self.gender = data['gender']
        self.skin_tone = data['skin_tone']
        self.preferred_hand = data['preferred_hand']
        self.born = data['born']
        self.death = data['death']
        self.profession = data['profession']
        self.profession_init = data['profession']
        self.spouses: List[Person] = []
        self.parents: List[Person] = []
        self.children: List[Person] = []
        self.pregnant_from = None

    def serialize(self):
        return json.dumps(self.data, separators=(',', ':'))

    @staticmethod
    def deserialize(data):
        return Person(json.loads(data))

    @property
    def pregnant(self) -> bool:
        return self.pregnant_from is not None

    @property
    def kins(self) -> set:
        kins = set()
        for parent in self.parents:
            kins.add(parent)
            for grandparent in parent.parents:
                kins.add(grandparent)
            for sister in parent.children:
                kins.add(sister)
        for child in self.children:
            kins.add(child)
            for grandchild in child.children:
                kins.add(grandchild)
        return kins

    @property
    def spouse(self):
        if len(self.spouses) and not self.spouses[len(self.spouses)-1].death:
            return self.spouses[len(self.spouses)-1]
        return None

    @property
    def displayed_name(self):
        return self.profession + ' ' + self.name

    def age(self, current_year):
        return (self.death if self.death else current_year) - self.born

    @property
    def data(self):
        return {
            'name': self.name,
            'race': self.race,
            'gender': self.gender,
            'skin_tone': self.skin_tone,
            'preferred_hand': self.preferred_hand,
            'born': self.born,
            'death': self.death,
            'profession': self.profession,
            'profession_init': self.profession_init,
        }


def random_village_name():
    return random.choice(NAME_VILLAGE_START) + random.choice(NAME_VILLAGE_MIDDLE) + random.choice(NAME_VILLAGE_END)


def generate_history():
    name = random_village_name()
    persons = {}
    headman = None
    current_year = 150
    history = {}

    colonial_families = random.randint(4, 6)
    for i in range(colonial_families):
        person = Person(generate_person_data(True, 0, 110, 130))
        while person.profession == 'priest':
            person.profession = random.choice(GAME_DATA['professions'][0])

        wife = Person(generate_person_data(True, 1, person.born-2, min(person.born+15, 135)))
        wife.name = wife.name.split(' ')[0] + ' ' + person.name.split(' ')[1]
        while wife.profession in {'nun', 'prostitute'}:
            wife.profession = random.choice(GAME_DATA['professions'][1])

        person.spouses.append(wife)
        wife.spouses.append(person)

        if headman is None:
            person.profession = 'headman'
            headman = person
        persons[person.id] = person
        persons[wife.id] = wife

    def find_a_lover(to_person: Person):

        lovers = []
        for person_id, person in persons.items():
            if person.death or person.gender != 0 or person.age(current_year) < 16 or person.age(current_year) > 60:
                continue
            if person in to_person.kins and random.random() > 0.01:
                continue
            lovers.append(person)
        return random.choice(lovers) if len(lovers) else None

    def generate_child(mother, father):
        child = Person(generate_person_data(True, None))
        child.born = current_year
        child.profession = 'boy' if child.gender == 0 else 'girl'
        child.profession_init = child.profession
        if random.random() < 0.3:
            child.death = current_year
        child.name = child.name.split(' ')[0] + ' ' + mother.name.split(' ')[1]
        child.parents = (mother, father)
        return child

    while current_year < 330:
        # print(f"Year {current_year}:")
        history[current_year] = []
        new_persons = set()

        if current_year == 150:
            names = []
            for person in persons.values():
                if person.gender == 0:
                    names.append(person.displayed_name + ' and his wife ' + person.spouse.displayed_name)
            history[current_year].append(', '.join(names) + ' founded the village of ' + name)

        for person_id, person in persons.items():

            if person.death:
                continue

            if person.age(current_year) == 6:
                person.profession = random.choice(GAME_DATA['children_professions'][person.gender])
                person.profession_init = person.profession
                history[current_year].append(person.name + ' become 6 and now ' +
                                             ('he' if person.gender == 0 else 'she') + ' is ' + person.profession)

            if person.age(current_year) == 16:
                person.profession = random.choice(GAME_DATA['professions'][person.gender])
                person.profession_init = person.profession
                history[current_year].append(person.name + ' become 16 and now ' +
                                             ('he' if person.gender == 0 else 'she') + ' is ' + person.profession)

            if person.pregnant:
                child = generate_child(person, person.pregnant_from)
                history[current_year].append(person.name + ' gives birth to a ' + child.displayed_name +
                                             ". It's a child of " +
                                             ('her spouse ' if person.pregnant_from is person.spouse else '') +
                                             person.pregnant_from.name)
                if child.death:
                    history[current_year].append(child.displayed_name + ' born already dead')
                new_persons.add(child)
                person.children.append(child)
                person.pregnant_from.children.append(child)
                person.pregnant_from = None

                chance_to_die_after_burn = 0.05
                if person.age(current_year) < 16:
                    chance_to_die_after_burn += 0.1
                if person.profession == "frail girl":
                    chance_to_die_after_burn += 0.9
                if person.profession == "prostitute":
                    chance_to_die_after_burn += 0.1
                if random.random() < chance_to_die_after_burn:
                    person.death = current_year
                    history[current_year].append(person.name + ' dies in childbirth')

            elif person.gender == 1 and not person.pregnant and person.age(current_year) >= 12:
                chance_to_pregnant = 0.01
                if person.spouse:
                    chance_to_pregnant += 0.9
                if person.profession == 'prostitute':
                    chance_to_pregnant += 0.9
                if person.age(current_year) > 40:
                    chance_to_pregnant = 0
                elif person.age(current_year) > 30:
                    chance_to_pregnant /= 4
                elif person.age(current_year) > 20:
                    chance_to_pregnant /= 2
                elif person.age(current_year) < 14:
                    chance_to_pregnant /= 2

                if random.random() < chance_to_pregnant:
                    chance_to_treason = 0.01
                    if person.profession == 'prostitute':
                        chance_to_treason += 0.5

                    if not person.spouse or random.random() < chance_to_treason:
                        pregranter = find_a_lover(person)
                    else:
                        pregranter = person.spouse

                    person.pregnant_from = pregranter
                    if person.pregnant:
                        history[current_year].append(person.name + ' become pregnant from ' + ('her spouse ' if person.pregnant_from is person.spouse else '') + person.pregnant_from.name)

            if person.gender == 0 and person.age(current_year) >= 18 and person.spouse is None and \
                person.profession not in {"priest", "local fool"}:
                chance_to_wanna_wife = 0.5
                if random.random() < chance_to_wanna_wife:
                    girls = [girl for girl in persons.values()
                             if girl.gender == 1 and not girl.death and girl.spouse is None and not girl.pregnant and
                             13 < girl.age(current_year) < person.age(current_year) + 3 and
                             girl.profession not in {"nun", "prostitute"} and
                             girl not in person.kins]
                    if len(girls):
                        girl = random.choice(girls)
                        history[current_year].append(person.name + ' marries ' + girl.name)
                        girl.name = girl.name.split(' ')[0] + ' ' + person.name.split(' ')[1]
                        person.spouses.append(girl)
                        girl.spouses.append(person)

            chance_to_random_death = 0.005
            if person.age(current_year) < 5:
                chance_to_random_death += 0.02
            elif person.age(current_year) > 50:
                chance_to_random_death += (person.age(current_year) - 30) / 500

            if len([person for person in persons.values() if not person.death]) > 30:
                chance_to_random_death *= 2

            if len([person for person in persons.values() if not person.death]) > 60:
                chance_to_random_death *= 5

            if person.profession in {"frail girl", "prostitute", "frail boy"}:
                chance_to_random_death *= 2

            if person.gender == 0:
                chance_to_random_death *= 1.1

            if random.random() < chance_to_random_death:
                reasons = ('by unknown ill', 'by accident', 'by natural causes')
                death_reason = random.choice(reasons)
                person.death = current_year
                history[current_year].append(person.name + ' dies in ' + str(person.age(current_year)) + ' by ' + death_reason)

            chance_to_death_by_nightmare = 0
            if current_year >= 300:
                chance_to_death_by_nightmare = 0.02
                if person.gender == 0:
                    chance_to_death_by_nightmare += 0.01

            if random.random() < chance_to_death_by_nightmare:
                reasons = ('zombie', 'zombie dog', 'skeleton', 'skeleton dog', 'nightmare', 'night daemon', 'daemon',
                           'necromant', 'night creature', 'undead creature', 'monster')
                death_reason = random.choice(reasons)
                person.death = current_year
                history[current_year].append(person.name + ' killed in ' + str(person.age(current_year)) + ' by ' + death_reason)

        if random.random() < 0.03:
            migrants_count = random.randint(1, 4)
            for i in range(migrants_count):
                person = Person(generate_person_data(True, None, current_year-50, current_year-14))
                history[current_year].append(str(person.age(current_year)) + 'yo ' + person.displayed_name
                                             + ' incoming to ' + name)
                new_persons.add(person)

        if headman is None or headman.death:
            candidates = [person for person in persons.values() if not person.death and person.age(current_year) > 20]
            if len(candidates):
                chances = []
                for candidate in candidates:
                    balls = 0
                    if candidate.age(current_year) > 40:
                        balls += 10
                    if candidate.gender == 0:
                        balls += 10
                    if candidate.profession in {"prostitute", "thief", "local fool", "nun"}:
                        balls -= 30
                    elif candidate.profession in {"teacher", "priest", "innkeeper", "merchant", "trader"}:
                        balls += 1
                    elif candidate.profession in {"guard", "farmer", "bartender", "waitress", "mercenary"}:
                        balls -= 1
                    chances.append((candidate, balls))
                chances = sorted(chances, key=lambda row: row[1], reverse=False)
                winner, _ = chances.pop()
                history[current_year].append(str(winner.age(current_year)) + 'yo ' + winner.displayed_name +
                                             ' elected as new head' + ('man' if winner.gender == 0 else 'woman'))
                headman = winner
                winner.profession = 'head' + ('man' if winner.gender == 0 else 'woman')

        for person in new_persons:
            persons[person.id] = person
        current_year += 1

    return name, persons, history
