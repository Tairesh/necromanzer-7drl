import random

from engine.chunk import CHUNK_SIZE, Chunk
from engine.tile import Tile, TileBase
from items.common import Pot, Jug, Barrel, Bucket, Cloth, Thread, Coins
from items.furniture import Door, Bench, Stool, Table, Bed, Chest
from items.tools import Knife, Axe, Shovel
from .common import Structure, gravel_tile, ground_tile, Rect


def generate_tile(x, y, structures, house_map, wall_type) -> Tile:
    houses_chunks = structures[Structure.HOUSE]
    road_chunks = structures[Structure.ROAD]
    chunk_x, chunk_y = x//CHUNK_SIZE, y//CHUNK_SIZE
    chunk_left, chunk_top = chunk_x*CHUNK_SIZE, chunk_y*CHUNK_SIZE
    chunk_right, chunk_bottom = chunk_left + CHUNK_SIZE - 1, chunk_top + CHUNK_SIZE - 1
    x_in_chunk, y_in_chunk = x-chunk_left, y-chunk_top
    left = (chunk_x-1, chunk_y) in houses_chunks
    top = (chunk_x, chunk_y-1) in houses_chunks
    left_road = (chunk_x-1, chunk_y) in road_chunks
    right_road = (chunk_x+1, chunk_y) in road_chunks
    top_road = (chunk_x, chunk_y-1) in road_chunks
    bottom_road = (chunk_x, chunk_y+1) in road_chunks

    try:
        s = house_map[x_in_chunk][y_in_chunk]
    except Exception:
        s = ' '

    if s == 'w':
        tile = Tile((x, y), wall_type)
    elif s == 'f':
        tile = Tile((x, y), TileBase.WOODEN_FLOOR)
        if random.random() < 0.05:
            if random.random() > 0.5:
                tile.items.append(Bucket())
            else:
                tile.items.append(Barrel())
    elif s == 't':
        tile = Tile((x, y), TileBase.WOODEN_FLOOR)
        tile.items.append(Table())
        if random.random() > 0.66:
            if random.random() > 0.5:
                tile.items.append(Pot())
            else:
                tile.items.append(Jug())
    elif s == 's':
        tile = Tile((x, y), TileBase.WOODEN_FLOOR)
        tile.items.append(Stool())
    elif s == 'b':
        tile = Tile((x, y), TileBase.WOODEN_FLOOR)
        tile.items.append(Bench())
        if random.random() < 0.2:
            r = random.random()
            if r < 0.3:
                tile.items.append(Knife())
            elif r < 0.6:
                tile.items.append(Shovel())
            else:
                tile.items.append(Axe())
    elif s == '0':
        tile = Tile((x, y), TileBase.WOODEN_FLOOR)
        tile.items.append(Bed())
    elif s == 'r':
        tile = Tile((x, y), TileBase.FIREPLACE)
    elif s == 'i':
        tile = Tile((x, y), wall_type)
        # tile = Tile((x, y), TileBase.WOODEN_WALL_WITH_WINDOW if wall_type is TileBase.WOODEN_WALL else TileBase.STONE_WALL_WITH_WINDOW)
    elif s == 'c':
        tile = Tile((x, y), TileBase.WOODEN_FLOOR)
        tile.items.append(Chest())
        if random.random() < 0.3:
            tile.items.append(Cloth())
        if random.random() < 0.3:
            tile.items.append(Thread())
        if random.random() < 0.3:
            tile.items.append(Coins())
    elif s == '+':
        tile = Tile((x, y), TileBase.WOODEN_FLOOR_BELOW_DOOR)
        tile.items.append(Door())
    elif (x == chunk_left and not left) or x == chunk_right or (y == chunk_top and not top) or y == chunk_bottom:
        if (left_road and x == chunk_left and 5 < y_in_chunk < CHUNK_SIZE - 5) or \
                (right_road and x == chunk_right and 5 < y_in_chunk < CHUNK_SIZE - 5) or \
                (top_road and y == chunk_top and 3 < x_in_chunk < CHUNK_SIZE - 3) or \
                (bottom_road and y == chunk_bottom and 3 < x_in_chunk < CHUNK_SIZE - 3):
            return gravel_tile(x, y)
        else:
            return Tile((x, y), TileBase.FENCE if random.random() > 0.1 else TileBase.FENCE_BROKEN)
    else:
        tile = ground_tile(x, y)
    return tile


def generate_chunk(chunk_x, chunk_y, structures) -> Chunk:
    road_chunks = structures[Structure.ROAD]
    left_road = (chunk_x-1, chunk_y) in road_chunks
    right_road = (chunk_x+1, chunk_y) in road_chunks
    top_road = (chunk_x, chunk_y-1) in road_chunks
    bottom_road = (chunk_x, chunk_y+1) in road_chunks

    house_map = [[' ' for j in range(CHUNK_SIZE)] for i in range(CHUNK_SIZE)]

    box = Rect(random.randint(1, 4), random.randint(1, 4), random.randint(6, 8), random.randint(6, 8))
    d_left = box.left
    d_right = CHUNK_SIZE - box.right
    d_top = box.top
    d_bottom = CHUNK_SIZE - box.bottom
    if d_left > d_right and d_left > d_top and d_left > d_bottom:
        h = random.randint(4, box.height)
        if random.random() > 0.5:
            y = box.top
        else:
            y = box.bottom-h
        box2 = Rect(1, y, d_left-1, h)
        box_mode = 'left'
    elif d_top > d_right and d_top > d_left and d_top > d_bottom:
        w = random.randint(5, box.width)
        if random.random() > 0.5:
            x = box.left
        else:
            x = box.right-w
        box2 = Rect(x, 1, w, d_top-1)
        box_mode = 'top'
    elif d_right > d_top and d_right > d_left and d_right > d_bottom:
        h = random.randint(4, box.height)
        if random.random() > 0.5:
            y = box.top
        else:
            y = box.bottom-h
        box2 = Rect(box.right, y, d_right-1, h)
        box_mode = 'right'
    else:
        w = random.randint(5, box.width)
        if random.random() > 0.5:
            x = box.left
        else:
            x = box.right-w
        box2 = Rect(x, box.bottom, w, d_bottom-1)
        box_mode = 'bottom'
    for i in range(box.left, min(box.right+1, CHUNK_SIZE-1)):
        for j in range(box.top, min(box.bottom + 1, CHUNK_SIZE - 1)):
            house_map[i][j] = 'f'
    for i in range(box.left, min(box.right+1, CHUNK_SIZE-1)):
        house_map[i][box.top] = 'w'
        house_map[i][box.bottom] = 'w'
    for j in range(box.top, min(box.bottom+1, CHUNK_SIZE-1)):
        house_map[box.left][j] = 'w'
        house_map[min(box.right, CHUNK_SIZE-1)][j] = 'w'
    for i in range(box2.left, min(box2.right+1, CHUNK_SIZE-1)):
        for j in range(box2.top, min(box2.bottom + 1, CHUNK_SIZE - 1)):
            house_map[i][j] = 'f'
    for i in range(box2.left, min(box2.right+1, CHUNK_SIZE-1)):
        house_map[i][box2.top] = 'w'
        house_map[i][box2.bottom] = 'w'
    for j in range(box2.top, min(box2.bottom+1, CHUNK_SIZE-1)):
        house_map[box2.left][j] = 'w'
        house_map[box2.right][j] = 'w'

    b2cx, b2cy = box2.center
    if box_mode == 'bottom':
        house_map[b2cx][box2.top] = '+'
    elif box_mode == 'top':
        house_map[b2cx][box2.bottom] = '+'
    elif box_mode == 'left':
        house_map[box2.right][b2cy] = '+'
    else:
        house_map[box2.left][b2cy] = '+'

    fx, fy = box.center
    house_map[fx][fy] = 'r'

    tx, ty = b2cx, b2cy
    if box2.width % 2 == 0:
        if box2.width >= 8:
            table_width = 3
            tx -= 1
        else:
            table_width = 1
    else:
        table_width = 2

    if box2.height % 2 == 0:
        if box2.height >= 8:
            table_height = 3
            ty -= 1
        else:
            table_height = 1
    else:
        table_height = 2

    for i in range(tx, tx+table_width):
        for j in range(ty, ty+table_height):
            house_map[i][j] = 't'

    if box2.width > box2.height:
        for j in range(ty, ty+table_height):
            house_map[tx-1][j] = 's'
            house_map[tx+table_width][j] = 's'
    else:
        for i in range(tx, tx+table_width):
            house_map[i][ty-1] = 's'
            house_map[i][ty+table_height] = 's'

    if bottom_road:
        if box2.bottom > box.bottom:
            x = box2.center[0]
            y = box2.bottom
        else:
            x = box.center[0]
            y = box.bottom
        house_map[x][y] = '+'
        if box_mode == 'top':
            for i in range(box.height-1):
                house_map[box.left+1][box.top+i+1] = 'b'
            for i in range(box.height-1):
                house_map[box.right-1][box.top+i+1] = random.choice(('0', 'c'))
        else:
            for i in range(box.width-1):
                house_map[box.left+i+1][box.top+1] = 'b'
            if box_mode == 'left':
                for i in range(box.height-1):
                    house_map[box.right-1][box.top+i+1] = random.choice(('0', 'c'))
            else:
                for i in range(box.height-1):
                    house_map[box.left+1][box.top+i+1] = random.choice(('0', 'c'))
    elif top_road:
        if box2.top < box.top:
            x = box2.center[0]
            y = box2.top
        else:
            x = box.center[0]
            y = box.top
        house_map[x][y] = '+'
        if box_mode == 'bottom':
            for i in range(box.height-1):
                house_map[box.left+1][box.top+i+1] = 'b'
            for i in range(box.height-1):
                house_map[box.right-1][box.top+i+1] = random.choice(('0', 'c'))
        else:
            for i in range(box.width-1):
                house_map[box.left+i+1][box.bottom-1] = 'b'
            if box_mode == 'left':
                for i in range(box.height-1):
                    house_map[box.right-1][box.top+i+1] = random.choice(('0', 'c'))
            else:
                for i in range(box.height-1):
                    house_map[box.left+1][box.top+i+1] = random.choice(('0', 'c'))
    elif left_road:
        if box2.left < box.left:
            y = box2.center[1]
            x = box2.left
        else:
            y = box.center[1]
            x = box.left
        house_map[x][y] = '+'
        if box_mode == 'right':
            for i in range(box.width-1):
                house_map[box.left+i+1][box.bottom-1] = 'b'
            for i in range(box.width-1):
                house_map[box.left+i+1][box.top+1] = random.choice(('0', 'c'))
        else:
            for i in range(box.height-1):
                house_map[box.right-1][box.top+i+1] = 'b'
            if box_mode == 'bottom':
                for i in range(box.width-1):
                    house_map[box.left+i+1][box.top+1] = random.choice(('0', 'c'))
            else:
                for i in range(box.width-1):
                    house_map[box.left+i+1][box.bottom-1] = random.choice(('0', 'c'))
    else:
        if box2.right > box.right:
            y = box2.center[1]
            x = box2.right
        else:
            y = box.center[1]
            x = box.right
        house_map[x][y] = '+'
        if box_mode == 'left':
            for i in range(box.width-1):
                house_map[box.left+i+1][box.bottom-1] = 'b'
            for i in range(box.width-1):
                house_map[box.left+i+1][box.top+1] = random.choice(('0', 'c'))
        else:
            for i in range(box.height-1):
                house_map[box.left+1][box.top+i+1] = 'b'
            if box_mode == 'bottom':
                for i in range(box.width-1):
                    house_map[box.left+i+1][box.top+1] = random.choice(('0', 'c'))
            else:
                for i in range(box.width-1):
                    house_map[box.left+i+1][box.bottom-1] = random.choice(('0', 'c'))

    floor = {'f', 'b', 'c', '0', 's', 't'}
    for i in range(CHUNK_SIZE):
        for j in range(CHUNK_SIZE):
            p = house_map[i][j]
            if p == 'w' and random.random() < 0.3:
                left = house_map[i-1][j] if i > 0 else ' '
                right = house_map[i+1][j] if i < CHUNK_SIZE-1 else ' '
                top = house_map[i][j-1] if j > 0 else ' '
                bottom = house_map[i][j+1] if j < CHUNK_SIZE-1 else ' '

                if (left in floor and right == ' ') or \
                    (right in floor and left == ' ') or \
                    (top in floor and bottom == ' ') or \
                    (bottom in floor and top == ' '):
                    house_map[i][j] = 'i'

    if random.random() < 0.3:
        wall_type = TileBase.STONE_WALL
    else:
        wall_type = TileBase.WOODEN_WALL

    chunk_left, chunk_top = chunk_x*CHUNK_SIZE, chunk_y*CHUNK_SIZE
    tiles = []
    for i in range(CHUNK_SIZE):
        for j in range(CHUNK_SIZE):
            tile = generate_tile(chunk_left + i, chunk_top + j, structures, house_map, wall_type)
            tiles.append(tile)
    return Chunk((chunk_x, chunk_y), tiles)

