from engine.chunk import CHUNK_SIZE, Chunk
from engine.tile import Tile, TileBase
from . import badlands
from .common import Structure, gravel_tile


def generate_tile(x, y, structures, well=False) -> Tile:
    graveyard_chunks = structures[Structure.GRAVEYARD]
    road_chunks = structures[Structure.ROAD]
    houses_chunks = structures[Structure.HOUSE]
    fields_chunks = structures[Structure.FIELD]
    chunk_x, chunk_y = x//CHUNK_SIZE, y//CHUNK_SIZE
    chunk_left, chunk_top = chunk_x*CHUNK_SIZE, chunk_y*CHUNK_SIZE
    x_in_chunk, y_in_chunk = x-chunk_left, y-chunk_top
    left = (chunk_x-1, chunk_y) in road_chunks or (chunk_x-1, chunk_y) in graveyard_chunks or (chunk_x-1, chunk_y) in houses_chunks or (chunk_x-1, chunk_y) in fields_chunks
    right = (chunk_x+1, chunk_y) in road_chunks or (chunk_x+1, chunk_y) in graveyard_chunks or (chunk_x+1, chunk_y) in houses_chunks or (chunk_x+1, chunk_y) in fields_chunks
    top = (chunk_x, chunk_y-1) in road_chunks or (chunk_x, chunk_y-1) in graveyard_chunks or (chunk_x, chunk_y-1) in houses_chunks or (chunk_x, chunk_y-1) in fields_chunks
    bottom = (chunk_x, chunk_y+1) in road_chunks or (chunk_x, chunk_y+1) in graveyard_chunks or (chunk_x, chunk_y+1) in houses_chunks or (chunk_x, chunk_y+1) in fields_chunks

    left_top = (chunk_x-1, chunk_y-1) in road_chunks or (chunk_x-1, chunk_y-1) in houses_chunks
    right_top = (chunk_x+1, chunk_y-1) in road_chunks or (chunk_x+1, chunk_y-1) in houses_chunks
    left_bottom = (chunk_x-1, chunk_y+1) in road_chunks or (chunk_x-1, chunk_y+1) in houses_chunks
    right_bottom = (chunk_x+1, chunk_y+1) in road_chunks or (chunk_x+1, chunk_y+1) in houses_chunks

    base = 'dirt'
    if x_in_chunk == CHUNK_SIZE//2 and y_in_chunk == CHUNK_SIZE//2 and well:
        base = 'well'
    elif 3 <= x_in_chunk < CHUNK_SIZE-3 and 5 <= y_in_chunk < CHUNK_SIZE-5:
        base = 'gravel'
    else:
        if (x_in_chunk < 3 and 5 <= y_in_chunk < CHUNK_SIZE-5 and left)\
           or (x_in_chunk >= CHUNK_SIZE-3 and CHUNK_SIZE-5 > y_in_chunk >= 5 and right)\
           or (y_in_chunk < 5 and 3 <= x_in_chunk < CHUNK_SIZE-3 and top)\
           or (y_in_chunk >= CHUNK_SIZE-5 and CHUNK_SIZE-3 > x_in_chunk >= 3 and bottom)\
           or (x_in_chunk < 3 and y_in_chunk < 5 and left_top)\
           or (x_in_chunk >= CHUNK_SIZE-3 and y_in_chunk < 5 and right_top)\
           or (x_in_chunk < 3 and y_in_chunk >= CHUNK_SIZE-5 and left_bottom)\
           or (x_in_chunk >= CHUNK_SIZE-3 and y_in_chunk >= CHUNK_SIZE-5 and right_bottom):
            base = 'gravel'

    if base == 'gravel':
        return gravel_tile(x, y)
    elif base == 'well':
        return Tile((x, y), TileBase.WELL)
    else:
        return badlands.generate_tile(x, y)


def _middle_road_chunk(structures) -> tuple:
    middle_x, middle_y = 0, 0
    for r_x, r_y in structures[Structure.ROAD]:
        middle_x += r_x
        middle_y += r_y
    middle_x /= len(structures[Structure.ROAD])
    middle_y /= len(structures[Structure.ROAD])
    min_dist = 99999
    middle = None
    for r_x, r_y in structures[Structure.ROAD]:
        dist = abs(r_x - middle_x) + abs(r_y - middle_y)
        if dist < min_dist:
            min_dist = dist
            middle = r_x, r_y
    return middle


def generate_chunk(chunk_x, chunk_y, structures) -> Chunk:
    well = (chunk_x, chunk_y) == _middle_road_chunk(structures)
    chunk_left, chunk_top = chunk_x*CHUNK_SIZE, chunk_y*CHUNK_SIZE
    tiles = []
    for i in range(CHUNK_SIZE):
        for j in range(CHUNK_SIZE):
            tile = generate_tile(chunk_left + i, chunk_top + j, structures, well)
            tiles.append(tile)
    return Chunk((chunk_x, chunk_y), tiles)
