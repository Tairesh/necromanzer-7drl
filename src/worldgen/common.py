import random
from enum import Enum, auto

from engine.tile import Tile, TileBase


class Structure(Enum):
    GRAVEYARD = auto()
    ROAD = auto()
    HOUSE = auto()
    FIELD = auto()


class Rect:
    def __init__(self, x, y, w, h):
        self.left = x
        self.top = y
        self.right = x + w
        self.bottom = y + h

    @property
    def width(self):
        return self.right - self.left

    @property
    def height(self):
        return self.bottom - self.top

    @property
    def center(self):
        return self.left + (self.right-self.left)//2, self.top + (self.bottom-self.top)//2


def ground_tile(x, y) -> Tile:
    ground_chances = (4, 2, 1, 3, 0, 0, 5, 0, 0, 0, 1, 7, 1, 0, 3, 1, 1, 2, 1, 6, 4, 0, 0, 2, 3, 1, 6, 4, 0, 5, 0, 1, 2, 3, 0, 1, 2, 0, 1, 2)
    c = random.choice(ground_chances)
    bases = (TileBase.DIRT1, TileBase.DIRT2, TileBase.DIRT3, TileBase.DIRT4,
             TileBase.GRASS1, TileBase.GRASS2, TileBase.GRASS3, TileBase.GRASS4)
    base = bases[c]
    tile = Tile((x, y), base)
    return tile


def gravel_tile(x, y) -> Tile:
    if random.random() > 0.4:
        return Tile((x, y), random.choice((TileBase.GRAVEL1, TileBase.GRAVEL2)))
    else:
        return ground_tile(x, y)
