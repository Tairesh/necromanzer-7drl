import random

from engine.chunk import Chunk
from .common import Structure
from . import badlands, graveyard, road, house, fields


def structures_by_seed(seed):
    random.seed(seed + f"structures")

    graveyard_width = random.randint(4, 8)
    graveyard_height = random.randint(3, 5)
    graveyard_start = 0, 0
    graveyard_chunks = set()
    for i in range(graveyard_start[0] + graveyard_width):
        for j in range(graveyard_start[1] + graveyard_height):
            graveyard_chunks.add((graveyard_start[0] + i, graveyard_start[1] + j))

    if random.random() > 0.5:
        last_road_chunk = (random.randint(graveyard_start[0], graveyard_start[0]+graveyard_width-1), graveyard_start[1]-1)
        moving = 'left'
    else:
        last_road_chunk = (graveyard_start[0]-1, random.randint(graveyard_start[1], graveyard_start[1]+graveyard_height-1))
        moving = 'up'

    road_chunks = [last_road_chunk, ]
    start_moving = moving
    while len(road_chunks) < 50:
        if len(road_chunks) > 5 and random.random() > len(road_chunks) / 50:
            dirs = ('up', 'down', 'left', 'right')
            if moving == 'up':
                dirs = ('up', 'left', 'right')
            elif moving == 'left':
                dirs = ('up', 'down', 'left')
            elif moving == 'down':
                dirs = ('down', 'left', 'right')
            elif moving == 'right':
                dirs = ('up', 'down', 'right')
            moving = random.choice(dirs)
            if start_moving == 'up' and moving == 'down':
                moving = 'left'
            elif start_moving == 'left' and moving == 'right':
                moving = 'up'

        new_chunk = None
        if moving == 'up':
            new_chunk = (last_road_chunk[0] - 1, last_road_chunk[1])
        elif moving == 'down':
            new_chunk = (last_road_chunk[0] + 1, last_road_chunk[1])
        elif moving == 'left':
            new_chunk = (last_road_chunk[0], last_road_chunk[1] - 1)
        elif moving == 'right':
            new_chunk = (last_road_chunk[0], last_road_chunk[1] + 1)

        if new_chunk not in road_chunks:
            road_chunks.append(new_chunk)
        last_road_chunk = new_chunk

    houses = []
    neighbours = ((-1, 0), (1, 0), (0, -1), (0, 1))
    for road in road_chunks[10:]:
        if random.random() < 0.4:
            continue
        for dx, dy in neighbours:
            if random.random() < 0.3:
                continue
            place = dx + road[0], dy + road[1]
            if place not in road_chunks and place not in houses:
                houses.append(place)

    fields = []
    for road in road_chunks[2:]:
        for dx, dy in neighbours:
            if random.random() < 0.3:
                continue
            place = dx + road[0], dy + road[1]
            if place not in road_chunks and place not in houses and place not in fields:
                fields.append(place)
                width = random.randint(5, 20)
                height = random.randint(5, 10)
                for i in range(width):
                    for j in range(height):
                        place = dx*i + road[0], dy*j + road[1]
                        if place not in road_chunks and place not in houses and place not in fields:
                            fields.append(place)

    return {
        Structure.GRAVEYARD: graveyard_chunks,
        Structure.ROAD: road_chunks,
        Structure.HOUSE: houses,
        Structure.FIELD: fields,
    }


def generate_chunk(chunk_x, chunk_y, seed: str, structures: dict) -> Chunk:
    structure = None
    for s, chunks in structures.items():
        if (chunk_x, chunk_y) in chunks:
            structure = s
            break
    func = badlands.generate_chunk
    if structure:
        func = {
            Structure.GRAVEYARD: graveyard.generate_chunk,
            Structure.ROAD: road.generate_chunk,
            Structure.HOUSE: house.generate_chunk,
            Structure.FIELD: fields.generate_chunk,
        }[structure]

    random.seed(seed + f"{chunk_x}x{chunk_y}")
    chunk = func(chunk_x, chunk_y, structures)
    # print(f"Generated new chunk {chunk_x}x{chunk_y}")
    return chunk
