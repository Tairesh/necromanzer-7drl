import random

from engine.chunk import Chunk, CHUNK_SIZE
from engine.tile import Tile
from items.common import Boulder
from .common import ground_tile


def generate_tile(x, y, _=None) -> Tile:
    tile = ground_tile(x, y)
    if random.random() > 0.98:
        tile.items.append(Boulder())
    return tile


def generate_chunk(chunk_x, chunk_y, _) -> Chunk:
    chunk_left, chunk_top = chunk_x*CHUNK_SIZE, chunk_y*CHUNK_SIZE
    tiles = []
    for i in range(CHUNK_SIZE):
        for j in range(CHUNK_SIZE):
            tile = generate_tile(chunk_left + i, chunk_top + j)
            tiles.append(tile)
    return Chunk((chunk_x, chunk_y), tiles)
