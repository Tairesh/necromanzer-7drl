import engine
import traceback

try:
    engine.init()
    engine.loop()
except Exception as e:
    traceback.print_exc()
    traceback.print_exc(file=open('last_crash_traceback.txt', 'w'))
