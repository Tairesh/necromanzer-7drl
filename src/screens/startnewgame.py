import tcod
import tcod.event
import engine
from engine.drawable.boxes import BoxParams
from engine.drawable.buttons import Button
from tools.humans import random_name
from . import mainmenu, maingamescreen, worldgen
from engine.drawable.inputs import TextInput, SelectInput, NumberSelect
from engine.drawable.labels import Label, Help
from engine.drawable.interactive import InteractiveSet
from engine import savefile
import random


entities = {}
new_game_params = {
    'name': None,
    'gender': 0,
    'preferred_hand': 0,
    'skin_tone': 2,
    'age': 20,
    'seed': str(random.randint(1000000, 9999999999)),
}


def init():
    if new_game_params['name'] is None:
        new_game_params['name'] = random_name()
    name_input = TextInput((0, 0), 14, value=new_game_params['name'],
                           on_change=lambda inp: _change_params('name', inp.value))
    gender_input = SelectInput((0, 1), 14, new_game_params['gender'], ('Male', 'Female'), icons=(11, 12),
                               on_change=lambda inp: _change_params('gender', inp.value), roll=True)
    preferred_hand_input = SelectInput((0, 2), 14, new_game_params['preferred_hand'], ('Right', 'Left', 'Ambidexter'),
                                       on_change=lambda inp: _change_params('preferred_hand', inp.value), roll=True)
    skin_tone_input = SelectInput((0, 3), 14, new_game_params['skin_tone'], ('Albino', 'Pale', 'Olive', 'Dark'),
                                  icons=(219, 219, 219, 219), icons_colors=((0xF7, 0xEB, 0xDB), (0xF3, 0xC6, 0xA4),
                                                                            (0xED, 0xB9, 0x8A), (0x97, 0x5B, 0x43)),
                                  on_change=lambda inp: _change_params('skin_tone', inp.value), roll=True)
    age_input = NumberSelect((0, 4), 14, new_game_params['age'], 16, 99, lambda inp: _change_params('age', inp.value))
    seed_input = TextInput((0, 6), 14, value=new_game_params['seed'],
                           on_change=lambda inp: _change_params('seed', inp.value))
    entities['name_label'] = Label((engine.console.width//2-2, 2, 'right', 'top'), "Name: ", target=name_input)
    entities['gender_label'] = Label((engine.console.width//2-2, 3, 'right', 'top'), "Sex: ", target=gender_input)
    entities['preferred_hand_label'] = Label((engine.console.width//2-2, 4, 'right', 'top'), "Preferred hand: ", target=preferred_hand_input)
    entities['skin_tone_label'] = Label((engine.console.width//2-2, 5, 'right', 'top'), "Skin tone: ", target=skin_tone_input)
    entities['age_label'] = Label((engine.console.width//2-2, 6, 'right', 'top'), "Age: ", target=age_input)

    entities['seed_label'] = Label((engine.console.width//2-2, 8, 'right', 'top'), "World seed: ", target=seed_input)

    entities['input_set'] = InteractiveSet((engine.console.width//2-2, 2), [
        name_input,
        gender_input,
        preferred_hand_input,
        skin_tone_input,
        age_input,
        seed_input,
    ], 'vertical_custom', None)
    entities['help_label'] = Help((engine.console.width//2, engine.console.height-1, 'center', 'bottom'), (
        ('ESC', 'Back'),
        ('Enter', 'Continue'),
        ('*', 'Random name'),
        (18, 'Select'),
        (29, 'Change'),
    ))


def _change_params(key, value):
    new_game_params[key] = value


def call(event):
    if 'confirm' in entities:
        if event.type == 'KEYDOWN':
            if event.scancode == tcod.event.SCANCODE_ESCAPE:
                _cancel()
        return

    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            engine.change_screen(mainmenu)
        elif event.scancode == tcod.event.SCANCODE_KP_MULTIPLY or (
                event.scancode == tcod.event.SCANCODE_8 and event.mod & tcod.event.KMOD_LSHIFT
                ):
            new_game_params['name'] = random_name(new_game_params['gender'])
            entities['input_set'].entities[0].value = new_game_params['name']
            entities['input_set'].entities[0].render()
        elif event.scancode in {tcod.event.SCANCODE_RETURN, tcod.event.SCANCODE_KP_ENTER}:
            entities['confirm'] = InteractiveSet('center', [
                Button(None, '[y] Yes', lambda btn: _confirm(), tcod.event.SCANCODE_Y),
                Button(None, '[n] No', lambda btn: _cancel(), tcod.event.SCANCODE_N),
            ], 'horizontal', BoxParams('Are you sure you want to start?'), 35, True)
            entities['input_set'].hidden = True


def _confirm():
    entities.pop('confirm')
    entities['input_set'].hidden = False
    worldgen.new_game_params = new_game_params
    engine.change_screen(worldgen)


def _cancel():
    entities.pop('confirm')
    entities['input_set'].hidden = False
    engine.console.clear()


def tick():
    pass


def get_entities():
    return entities.values()


def draw():
    pass
