import engine
from engine import savefile, keys
from engine.drawable.labels import Help
from engine.drawable.log import LogBox
from . import maingamescreen, mainmenu
from worldgen.history import generate_history
import tcod.event
import tcod.constants


stage = 'creating'
new_game_params: dict
drawables = {}


def init():
    global stage
    stage = 'creating'
    engine.log.lines.clear()


def call(event):
    global stage
    if stage == 'end':
        if event.type == 'KEYDOWN':
            if event.scancode in keys.SELECT_KEYS:
                savefile.save(engine.world)
                engine.change_screen(maingamescreen)
            elif event.scancode in keys.CANCEL_KEYS:
                engine.change_screen(mainmenu)
            elif event.scancode == tcod.event.SCANCODE_R:
                stage = 'history'
                drawables.clear()
                engine.console.clear()
                engine.flush()
            elif event.scancode in drawables['log'].scancodes_subscribe:
                drawables['log'].call(event)


def tick():
    global stage
    if stage == 'creating':
        engine.world = savefile.create(new_game_params)
        stage = 'history'
    elif stage == 'history':
        population = 0
        while population < 20:
            name, persons, history = generate_history()
            population = len([person for person in persons.values() if not person.death])
        engine.world.generate_units(name, persons, history)
        lines = []
        for year in history:
            lines.append((f"* Year {year}:", tcod.chartreuse))
            for line in history[year]:
                if 'become pregnant' in line:
                    color = tcod.lighter_green
                elif ' dead' in line or ' dies ' in line or 'killed ' in line:
                    color = tcod.light_red
                elif 'gives birth to a boy' in line:
                    color = tcod.lighter_blue
                elif 'gives birth to a girl' in line:
                    color = tcod.lighter_pink
                elif 'elected' in line:
                    color = tcod.gold
                else:
                    color = tcod.white
                lines.append((line[0].upper() + line[1:], color))
        lines[1] = lines[1][0], tcod.lighter_azure
        drawables['log'] = LogBox(lines)
        drawables['uptext'] = Help((engine.console.width//2, drawables['log'].pos.top - 1, 'center', 'top'), (
            ('Village name', ' ' + name),
            ('Population', ' ' + str(population)),
        ), fg_alt=tcod.lighter_gray)
        drawables['help'] = Help((engine.console.width//2, drawables['log'].pos.bottom+1, 'center', 'top'), (
            ('Enter', 'Accept'),
            ('r', 'Reroll'),
            (18, 'Scroll'),
            ('Home', 'To begining'),
            ('End', 'To end'),
            ('Esc', 'Cancel'),
        ))
        stage = 'end'


def get_entities():
    return drawables.values()


def draw():
    if stage == 'creating':
        engine.console.print(engine.console.width//2, engine.console.height//2, "Creating world...",
                             tcod.chartreuse, alignment=tcod.CENTER)
    elif stage == 'history':
        engine.console.print(engine.console.width // 2, engine.console.height // 2, "Generating history...",
                             tcod.chartreuse, alignment=tcod.CENTER)
