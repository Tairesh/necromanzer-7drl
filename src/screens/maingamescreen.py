import engine
import tcod.event

from engine.drawable.log import LogBox
from engine.drawable.mapview import MapView
from engine.drawable.labels import Label
from engine.drawable.playerstatus import PlayerStatus, HelpBox
from .gamemodes import moving


drawables = {}
mode = moving
skipping_time = False
popup = None


def init():
    global popup, mode, skipping_time
    popup = None
    mode = moving
    skipping_time = False

    drawables['view'] = MapView(engine.world.player.pos.x, engine.world.player.pos.y)
    drawables['mode_label'] = Label((0, 0), mode.label, tcod.chartreuse)

    drawables['use_arrows_label'] = Label((engine.console.width//2, 1, 'center', 'top'),
                                          'Use arrows to select direction:')
    drawables['use_arrows_label'].hidden = True

    drawables['player_status'] = PlayerStatus()
    drawables['at_screen'] = None
    drawables['menu'] = None

    drawables['help_window'] = HelpBox()
    drawables['help_window'].hidden = True
    drawables['log_window'] = LogBox(engine.world.log)
    drawables['log_window'].hidden = True

    mode.start()


def change_mode(new_mode, data=None):
    global mode
    mode.end()
    mode = new_mode
    if data:
        mode.start(data)
    else:
        mode.start()
    drawables['mode_label'] = Label((0, 0), mode.label, tcod.chartreuse)
    engine.console.clear()


def call(event):
    global popup
    if popup:
        if popup is drawables['help_window'] or popup is drawables['log_window']:
            if event.type == 'KEYDOWN':
                if event.scancode == tcod.event.SCANCODE_ESCAPE:
                    popup.hidden = True
                    popup = None
                elif event.scancode in popup.scancodes_subscribe:
                    popup.call(event)
            return

    mode.call(event)


def tick():
    mode.tick()
    player = engine.world.player
    if player.current_action or skipping_time:
        if player.current_action and player.current_action.end_tick == engine.world.current_tick:
            engine.world.act()
        else:
            engine.world.tick()
        drawables['view'].move_to(player.pos)
        drawables['player_status'].render()


def get_entities():
    return drawables.values()


def draw():
    mode.draw()
    for i, label in enumerate(engine.log.get_last(5)):
        label.blit(engine.console, 0, engine.console.height-1-i)
