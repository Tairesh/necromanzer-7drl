import engine
from . import mainmenu
import tcod.event
import tcod.constants
import random
import time


def init():
    pass


def call(event):
    if event.type == 'KEYUP' and event.scancode == tcod.event.SCANCODE_ESCAPE:
        engine.change_screen(mainmenu)
    elif event.type == 'KEYUP' and event.scancode == tcod.event.SCANCODE_RETURN:
        global mode
        mode = 2 if mode == 1 else 1


def tick():
    pass


def get_entities():
    return ()


last_updated = 0
mode = 1


def draw():
    global last_updated
    if mode == 1:
        if time.time() - last_updated > 0.05:
            for i in range(engine.console.width):
                for j in range(engine.console.height):
                    engine.console.default_bg = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
                    engine.console.put_char(i, j, 0, tcod.BKGND_SET)
            last_updated = time.time()
    elif mode == 2:
        engine.console.clear()
        engine.console.default_bg = tcod.black
        for i in range(16):
            for j in range(16):
                o = j + i * 16
                engine.console.put_char(j, i, o)

