from typing import Optional
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.labels import Label
from engine.drawable.boxes import BoxParams
import engine
from engine import consts, savefile
from . import emptyscreen, settings, startnewgame, maingamescreen
from tcod.event import SCANCODE_X, SCANCODE_T, SCANCODE_L, SCANCODE_S
import tcod
import os


buttons_set: InteractiveSet
version_label: Label
popup_select_save: Optional[InteractiveSet] = None
loading_world = False


def _list_savefiles() -> list:
    if not os.path.isdir('save'):
        os.mkdir('save')
    return [file for file in os.listdir('save') if not file.startswith('.') and file.endswith('.save')]


def init():
    global buttons_set, version_label, popup_select_save
    buttons_set = InteractiveSet((engine.console.width//2, engine.console.height//2, 'center', 'center'), [
        Button(None, '[l] Load game', click=lambda b: _open_popup(), key=SCANCODE_L, disabled=len(_list_savefiles()) == 0),
        Button((0, 0), '[s] Start game', click=lambda b: engine.change_screen(startnewgame), key=SCANCODE_S),
        Button((0, 0), '[t] Settings', click=lambda b: engine.change_screen(settings), key=SCANCODE_T),
        Button((0, 0), "[x] Exit", click=lambda b: engine.stop(), key=SCANCODE_X),
    ], 'vertical', None)
    version_label = Label((1, 1), f"v{consts.VERSION}", tcod.chartreuse)
    popup_select_save = None


def _open_popup():
    global popup_select_save
    savefiles = _list_savefiles()
    if len(savefiles) == 1:
        _select_world(savefiles[0])
    else:
        popup_select_save = InteractiveSet((engine.console.width // 2, engine.console.height // 2, 'center', 'center'), [
            Button(None, file.replace('.save', ''), click=lambda b, file=file: _select_world(file)) for file in savefiles
        ], 'vertical', BoxParams('Select savefile'), min_width=20, entities_centered=True)


def _select_world(file_name):
    global loading_world
    loading_world = True
    engine.flush()
    engine.world = savefile.load(f"save/{file_name}")
    engine.log.lines.clear()
    loading_world = False
    engine.change_screen(maingamescreen)


def call(event):
    global popup_select_save

    if event.type == 'KEYUP' and event.scancode == tcod.event.SCANCODE_F2:
        engine.change_screen(emptyscreen)
    elif event.type == 'KEYDOWN' and event.scancode == tcod.event.SCANCODE_ESCAPE and popup_select_save:
        popup_select_save = None
        engine.console.clear()


def tick():
    pass


def get_entities():
    if loading_world:
        return ()
    elif popup_select_save:
        return popup_select_save, version_label
    else:
        return buttons_set, version_label


def draw():
    if loading_world:
        engine.console.clear()
        engine.console.print(engine.console.width//2, engine.console.height//2, "Loading world...", tcod.chartreuse, alignment=tcod.CENTER)
