import tcod
import tcod.event
import engine
from engine import keys
from screens import maingamescreen
from . import moving
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.boxes import BoxParams
from engine.drawable.labels import Label
from engine.unit import Unit


label = "GRABBING"
selected_for_grab = None


def _grab_item(item, tile):
    maingamescreen.drawables['select_item_to_grab'] = None
    alert = Label((engine.console.width//2, 1, 'center', 'top'), 'Use arrows to select designation:')
    maingamescreen.drawables['select_item_to_grab'] = alert
    maingamescreen.popup = alert

    global selected_for_grab
    selected_for_grab = item, tile


def _try_end_grab(dx, dy):
    global selected_for_grab
    item, tile = selected_for_grab
    tile_dest = engine.world.get_tile(tile.pos.x + dx, tile.pos.y + dy)

    if not tile_dest.is_walkable:
        engine.log.add_line(f"You can't move {(item.name if isinstance(item, Unit) else item.title).capitalize()} on {tile_dest.base.title}", tcod.orange)
    elif item in tile.items:
        tile.items.remove(item)
        tile_dest.items.append(item)
        engine.log.add_line(f"You moving {item.title}!")
        maingamescreen.drawables['view'].render()
    elif item in tile.units:
        item.teleport_to(tile.pos.x + dx, tile.pos.y + dy)
        engine.log.add_line(f"You moving {item.name}!")
        maingamescreen.drawables['view'].render()
    else:
        engine.log.add_line(f"{(item.name if isinstance(item, Unit) else item.title).capitalize()} moved away!", tcod.red)
    selected_for_grab = None
    maingamescreen.drawables['select_item_to_grab'] = None
    maingamescreen.change_mode(moving)


def _try_grab(dx, dy):
    if selected_for_grab:
        _try_end_grab(dx, dy)
        return

    if dx == 0 and dy == 0:
        engine.log.add_line(f"You can't grab yourself!", tcod.orange)
        maingamescreen.change_mode(moving)
    else:
        tile = engine.world.get_tile(engine.world.player.pos.x + dx, engine.world.player.pos.y + dy)
        items_to_grab = [item for item in tile.items if item.is_grabable] + [unit for unit in tile.units if unit.brain is None]
        if len(items_to_grab) == 0:
            engine.log.add_line(f"Nothing to grab here", tcod.orange)
            maingamescreen.change_mode(moving)
        elif len(items_to_grab) == 1:
            maingamescreen.popup.hidden = True
            _grab_item(items_to_grab[0], tile)
        else:
            maingamescreen.popup.hidden = True
            select = InteractiveSet((engine.console.width//2, engine.console.height//2, 'center', 'center'), [
                Button(None, f"{item.displayed_name if isinstance(item, Unit) else item.displayed_name}",
                       lambda btn, item=item: _grab_item(item, tile)) for item in items_to_grab
            ], 'vertical', BoxParams('Select an item to grab'), min_width=28)
            maingamescreen.drawables['select_item_to_grab'] = select
            maingamescreen.popup = select


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)
        if type(maingamescreen.popup) is not InteractiveSet:
            if event.scancode in keys.DIRECTION_KEYS:
                direction = keys.DIRECTION_KEYS[event.scancode]
                dx, dy = keys.DIRECTION_DELTA[direction]
                _try_grab(dx, dy)


def tick():
    pass


def draw():
    pass


def start():
    parts_can_grab = engine.world.player.body.parts_can_grab()
    if len(parts_can_grab) == 0:
        engine.log.add_line("You can't grab without arms!", tcod.orange)
        maingamescreen.change_mode(moving)
        return

    free_hands_keys = [key for key in parts_can_grab if key not in engine.world.player.wield or not engine.world.player.wield[key]]
    if len(free_hands_keys) == 0:
        engine.log.add_line("You can't grab without any free arm!", tcod.orange)
        maingamescreen.change_mode(moving)
        return

    maingamescreen.popup = maingamescreen.drawables['use_arrows_label']
    maingamescreen.popup.hidden = False


def end():
    if maingamescreen.popup:
        maingamescreen.popup.hidden = True
    maingamescreen.popup = None
    maingamescreen.drawables['select_item_to_grab'] = None
