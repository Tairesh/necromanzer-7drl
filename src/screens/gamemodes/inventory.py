import random

import tcod
import tcod.event
import engine
from engine.drawable.labels import Help
from engine.drawable.playerstatus import InventoryBox
from screens import maingamescreen
from . import moving


label = "CHECKING INVENTORY"
inv_box: InventoryBox


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)
        elif event.scancode in {tcod.event.SCANCODE_UP, tcod.event.SCANCODE_KP_8, tcod.event.SCANCODE_8}:
            inv_box.up()
        elif event.scancode in {tcod.event.SCANCODE_DOWN, tcod.event.SCANCODE_KP_2, tcod.event.SCANCODE_2}:
            inv_box.down()
        elif engine.world.player.alive and event.scancode == tcod.event.SCANCODE_D and inv_box.selected is not None:
            item = inv_box.unit.inventory[inv_box.selected]
            inv_box.unit.inventory.remove(item)
            engine.world.get_tile(inv_box.unit.pos).items.append(item)
            engine.log.add_line(f"{inv_box.unit.name} drops {item.title} to the ground!")
            inv_box.render()
            maingamescreen.drawables['view'].render()
        elif engine.world.player.alive and event.scancode == tcod.event.SCANCODE_W and inv_box.selected is not None:
            item = inv_box.unit.inventory[inv_box.selected]

            def _sort_hands_func(hand_key) -> int:
                if engine.world.player.raw_params['preferred_hand'] == 0:
                    return 1 if hand_key.startswith('left') else 0
                elif engine.world.player.raw_params['preferred_hand'] == 1:
                    return 1 if hand_key.startswith('right') else 0
                elif engine.world.player.raw_params['preferred_hand'] == 2:
                    return random.randint(0, 9)
                return 0
            parts_can_grab = engine.world.player.body.parts_can_grab()
            if len(parts_can_grab) == 0:
                engine.log.add_line("You can't wield without arms!", tcod.orange)
                maingamescreen.change_mode(moving)
                return
            free_hands_keys = [key for key in parts_can_grab if
                               key not in engine.world.player.wield or not engine.world.player.wield[key]]
            if len(free_hands_keys) == 0:
                engine.log.add_line("You can't wield more items!", tcod.orange)
                maingamescreen.change_mode(moving)
                return

            free_hands_keys = sorted(free_hands_keys, key=_sort_hands_func)

            if len(free_hands_keys) >= item.hands_to_grab:
                selected_parts = free_hands_keys[:item.hands_to_grab]
                for part in selected_parts:
                    engine.world.player.wield[part] = item
                inv_box.unit.inventory.remove(item)
                engine.log.add_line(f"You wield {item.title} in your {' and '.join(selected_parts)}")
                inv_box.render()
                maingamescreen.drawables['player_status'].render()
            else:
                engine.log.add_line(f"You haven't enough hands to wield {item.title}!", tcod.orange)


def tick():
    pass


def draw():
    pass


def start():
    global inv_box
    inv_box = InventoryBox(engine.world.player)
    maingamescreen.popup = inv_box
    maingamescreen.drawables['inventory_box'] = inv_box
    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height - 2, 'right'), (
        (18, 'Select'),
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height - 1, 'right'), (
        ('w', 'Wield'),
        ('d', 'Drop'),
    ))


def end():
    global inv_box
    inv_box = None
    maingamescreen.popup = None
    maingamescreen.drawables['inventory_box'] = None
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
