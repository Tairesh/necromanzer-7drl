from time import time

import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.labels import Help, Label
from engine.orders import OrderProto, Order
from screens import maingamescreen
from . import moving
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.boxes import BoxParams


label = "GIVING ORDERS"
mode = 'default'
moving_keys_pressed = set()
shift_pressed = False
last_moved_time = 0
MOVING_TIMEOUT = 0.07
blink_last_time = 0
BLINK_PERIOD = 0.5
cursor = None


def _move_cursor(new_pos):
    global last_moved_time, blink_last_time, cursor
    cursor = new_pos
    last_moved_time = time()
    blink_last_time = time() - BLINK_PERIOD


def call(event):
    global mode, cursor, shift_pressed, blink_last_time
    if mode in {'select_tile', 'select_unit'}:
        if event.type == 'KEYDOWN':
            if event.scancode == tcod.event.SCANCODE_ESCAPE:
                mode = 'default'
                cursor = None
                maingamescreen.popup.hidden = True
                maingamescreen.popup = maingamescreen.drawables['orders_menu']
                maingamescreen.popup.hidden = False
                maingamescreen.drawables['current_order_label'].hidden = False
                maingamescreen.drawables['view'].move_to(engine.world.player.pos)
            elif event.scancode in keys.SELECT_KEYS:
                if mode == 'select_tile':
                    _move_to_confirm()
                elif mode == 'select_unit':
                    _kill_confirm()
            elif event.scancode in keys.DIRECTION_KEYS and not event.repeat:
                direction = keys.DIRECTION_KEYS[event.scancode]
                if not direction in moving_keys_pressed:
                    moving_keys_pressed.add(direction)
            elif event.scancode == tcod.event.SCANCODE_LSHIFT:
                shift_pressed = True
        elif event.type == 'KEYUP':
            if event.scancode in keys.DIRECTION_KEYS and not event.repeat:
                direction = keys.DIRECTION_KEYS[event.scancode]
                if direction in moving_keys_pressed:
                    moving_keys_pressed.remove(direction)
            elif event.scancode == tcod.event.SCANCODE_LSHIFT:
                shift_pressed = False
        elif event.type == "MOUSEMOTION":
            _move_cursor(maingamescreen.drawables['view'].screen_to_map_pos(event.tile.x, event.tile.y))
        elif event.type == "MOUSEBUTTONUP" and event.button == tcod.event.BUTTON_LEFT:
            _move_cursor(maingamescreen.drawables['view'].screen_to_map_pos(event.tile.x, event.tile.y))
            if mode == 'select_tile':
                _move_to_confirm()
            elif mode == 'select_unit':
                _kill_confirm()
    elif event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)


def tick():
    if mode in {'select_tile', 'select_unit'}:
        global last_moved_time, blink_last_time, cursor
        if len(moving_keys_pressed) and time() > last_moved_time+MOVING_TIMEOUT:
            view = maingamescreen.drawables['view']
            dx, dy = 0, 0
            for direction in moving_keys_pressed:
                delta_x, delta_y = keys.DIRECTION_DELTA[direction]
                dx += delta_x
                dy += delta_y
            dx = int(dx / abs(dx)) if dx != 0 else 0
            dy = int(dy / abs(dy)) if dy != 0 else 0
            if shift_pressed:
                dx *= 10
                dy *= 10
            view.move(dx, dy)
            _move_cursor((view.center_x, view.center_y))


def draw():
    if mode in {'select_tile', 'select_unit'}:
        global blink_last_time
        view = maingamescreen.drawables['view']
        tx, ty = view.map_pos_to_screen(cursor)
        if time() > blink_last_time + BLINK_PERIOD*2:
            blink_last_time = time()
        elif time() > blink_last_time + BLINK_PERIOD:
            engine.console.default_fg = tcod.yellow
            engine.console.put_char(tx, ty, 88)
        if mode == 'select_unit':
            tile = engine.world.get_tile(cursor[0], cursor[1], False, True)
            if tile:
                for unit in tile.units:
                    if not unit.alive:
                        continue
                    engine.console.print(tx+1, ty+1, unit.displayed_name, unit.color)

    else:
        if engine.world.order.proto == OrderProto.MOVE_TO:
            view = maingamescreen.drawables['view']
            tx, ty = view.map_pos_to_screen(engine.world.order.data)
            if 0 <= tx < engine.console.width - 1 and 0 <= ty < engine.console.height - 1:
                engine.console.default_fg = tcod.purple
                engine.console.put_char(tx, ty, 88)
        elif engine.world.order.proto == OrderProto.KILL:
            view = maingamescreen.drawables['view']
            unit = engine.world.units[engine.world.order.data]
            tx, ty = view.map_pos_to_screen(unit.pos)
            if 0 <= tx < engine.console.width - 1 and 0 <= ty < engine.console.height - 1:
                engine.console.default_fg = tcod.purple
                engine.console.put_char(tx, ty, 88)


def _follow_me():
    engine.log.add_line("You give magical order for your cadavers to follow you!", tcod.purple)
    engine.world.order = Order(OrderProto.FOLLOW_ME)
    maingamescreen.change_mode(moving)


def _no_order():
    engine.log.add_line("You remove magical order from your cadavers!", tcod.purple)
    engine.world.order = Order(OrderProto.NONE)
    maingamescreen.change_mode(moving)


def _move_to():
    global mode, cursor, blink_last_time
    maingamescreen.drawables['orders_menu'].hidden = True
    maingamescreen.drawables['current_order_label'].hidden = True
    maingamescreen.popup = maingamescreen.drawables['use_arrows_label']
    maingamescreen.popup.hidden = False
    mode = 'select_tile'
    _move_cursor((maingamescreen.drawables['view'].center_x, maingamescreen.drawables['view'].center_y))


def _kill():
    global mode, cursor, blink_last_time
    maingamescreen.drawables['orders_menu'].hidden = True
    maingamescreen.drawables['current_order_label'].hidden = True
    maingamescreen.popup = maingamescreen.drawables['use_arrows_label']
    maingamescreen.popup.hidden = False
    mode = 'select_unit'
    _move_cursor((maingamescreen.drawables['view'].center_x, maingamescreen.drawables['view'].center_y))


def _move_to_confirm():
    global mode
    engine.log.add_line(f"You give magical order for your cadavers to move to selected place!", tcod.purple)
    engine.world.order = Order(OrderProto.MOVE_TO, cursor)
    mode = 'default'
    maingamescreen.drawables['view'].move_to(engine.world.player.pos)
    maingamescreen.change_mode(moving)


def _kill_confirm():
    maingamescreen.drawables['use_arrows_label'].hidden = True
    tile = engine.world.get_tile(cursor[0], cursor[1], False)
    if len(tile.units) == 0:
        engine.log.add_line(f"There is creatures here!", tcod.orange)
    elif len(tile.units) == 1:
        _kill_confirm_select(tile.units[0])
    else:
        unit_select_menu = InteractiveSet('center', [
            Button(None, unit.name, lambda btn, unit=unit: _kill_confirm_select(unit)) for unit in tile.units
        ], 'vertical', BoxParams("Select creature to kill"), 28)
        maingamescreen.drawables['unit_select_menu'] = unit_select_menu


def _kill_confirm_select(unit):
    global mode
    engine.log.add_line(f"You give magical order for your cadavers to kill {unit.name}!", tcod.purple)
    engine.world.order = Order(OrderProto.KILL, unit.id)
    mode = 'default'
    maingamescreen.drawables['view'].move_to(engine.world.player.pos)
    maingamescreen.drawables['unit_select_menu'] = None
    maingamescreen.change_mode(moving)


def start():
    orders_menu = InteractiveSet('center', [
        Button(None, '[n] No order', lambda btn: _no_order(), tcod.event.SCANCODE_N),
        Button(None, '[f] Follow me', lambda btn: _follow_me(), tcod.event.SCANCODE_F),
        Button(None, '[m] Move to', lambda btn: _move_to(), tcod.event.SCANCODE_M),
        Button(None, '[k] Kill', lambda btn: _kill(), tcod.event.SCANCODE_K),
    ], 'vertical', BoxParams("Select an order"), 19)
    maingamescreen.drawables['orders_menu'] = orders_menu
    maingamescreen.popup = orders_menu
    current_order = Label((orders_menu.pos.left, orders_menu.pos.top, 'left', 'bottom'),
                          "Current: " + engine.world.order.label, tcod.white)
    maingamescreen.drawables['current_order_label'] = current_order
    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height - 2, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height - 1, 'right'), (
        (18, 'Select'),
        ('Enter', 'Confirm')
    ))


def end():
    maingamescreen.drawables['use_arrows_label'].hidden = True
    maingamescreen.drawables['orders_menu'] = None
    maingamescreen.drawables['current_order_label'] = None
    maingamescreen.popup = None
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
