import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.boxes import ItemsAround
from engine.drawable.labels import Help
from engine.utils import get_line
from screens import maingamescreen
from . import moving


label = "VIEW ITEMS AROUND"


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)
        elif event.scancode in keys.UP_KEYS:
            maingamescreen.drawables['items_around'].up()
        elif event.scancode in keys.DOWN_KEYS:
            maingamescreen.drawables['items_around'].down()


def tick():
    pass


def draw():
    items_around = maingamescreen.drawables['items_around']
    if items_around.selected is not None:
        item, pos = items_around.items[items_around.selected]
        line = get_line((engine.world.player.pos.x, engine.world.player.pos.y), (pos.x, pos.y))
        for map_x, map_y in line:
            console_x, console_y = maingamescreen.drawables['view'].map_pos_to_screen(map_x, map_y)
            if 0 <= console_x < engine.console.width and 0 <= console_y < engine.console.height:
                engine.console.tiles[console_x][console_y][2] = (*tcod.darkest_green, 255)


def start():
    maingamescreen.drawables['player_status'].hidden = True
    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height - 2, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height - 1, 'right'), (
        ((18, 29), 'Select item'),
    ))
    items_around = ItemsAround()
    maingamescreen.drawables['items_around'] = items_around


def end():
    maingamescreen.drawables['player_status'].hidden = False
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
    maingamescreen.drawables['items_around'] = None
