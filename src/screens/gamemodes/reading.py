import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.labels import Help
from screens import maingamescreen
from . import moving


label = "READING"


def _try_read(dx, dy):
    tx, ty = engine.world.player.pos.x + dx, engine.world.player.pos.y + dy
    items = list(filter(lambda i: 'read' in i.actions, engine.world.get_tile(tx, ty, False).items))
    if len(items) == 0:
        engine.log.add_line("Nothing to read here", tcod.orange)
    else:
        engine.world.player.start_activate(items[0], 'read')

    maingamescreen.change_mode(moving)
    

def call(event):

    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)
        elif event.scancode in keys.DIRECTION_KEYS:
            direction = keys.DIRECTION_KEYS[event.scancode]
            dx, dy = keys.DIRECTION_DELTA[direction]
            _try_read(dx, dy)


def tick():
    pass


def draw():
    pass


def start():
    maingamescreen.popup = maingamescreen.drawables['use_arrows_label']
    maingamescreen.popup.hidden = False

    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height-2, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height-1, 'right'), (
        ((18, 29), 'Select direction'),
    ))


def end():
    maingamescreen.popup.hidden = True
    maingamescreen.popup = None
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
