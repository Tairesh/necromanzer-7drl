import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.labels import Help
from screens import maingamescreen
from . import moving


label = "CLOSING"


def _try_close(dx, dy):
    if dx == 0 and dy == 0:
        engine.log.add_line("Some joker standing on the way!", tcod.orange)
    else:
        tx, ty = engine.world.player.pos.x + dx, engine.world.player.pos.y + dy
        tile = engine.world.get_tile(tx, ty, False)
        items = list(filter(lambda i: 'close' in i.actions, tile.items))
        if len(items) == 0:
            engine.log.add_line("Nothing to close here", tcod.orange)
        elif len(tile.units) > 0:
            engine.log.add_line(f"{tile.units[0].displayed_name} blocking the way", tcod.orange)
        else:
            engine.world.player.start_activate(items[0], 'close')

    maingamescreen.change_mode(moving)


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)
        elif event.scancode in keys.DIRECTION_KEYS:
            direction = keys.DIRECTION_KEYS[event.scancode]
            dx, dy = keys.DIRECTION_DELTA[direction]
            _try_close(dx, dy)


def tick():
    pass


def draw():
    pass


def start():
    maingamescreen.popup = maingamescreen.drawables['use_arrows_label']
    maingamescreen.popup.hidden = False

    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height - 2, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height - 1, 'right'), (
        ((18, 29), 'Select direction'),
    ))


def end():
    maingamescreen.popup.hidden = True
    maingamescreen.popup = None
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
