import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.labels import Help
from screens import maingamescreen
from . import moving, bodycaring


label = "EXAMINE"


def _try_examine(dx, dy):
    tile = engine.world.get_tile(engine.world.player.pos.x + dx, engine.world.player.pos.y + dy, False)

    item_names = [tile.base.title] + list([item.displayed_name for item in tile.items]) + list([unit.displayed_name for unit in tile.units])
    engine.log.add_line(f"Here: {', '.join(item_names)}")
    if len(tile.units):
        maingamescreen.change_mode(bodycaring, {'unit': tile.units[0]})
    else:
        maingamescreen.change_mode(moving)


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)
        elif event.scancode in keys.DIRECTION_KEYS:
            direction = keys.DIRECTION_KEYS[event.scancode]
            dx, dy = keys.DIRECTION_DELTA[direction]
            _try_examine(dx, dy)


def tick():
    pass


def draw():
    pass


def start():
    maingamescreen.popup = maingamescreen.drawables['use_arrows_label']
    maingamescreen.popup.hidden = False
    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height-2, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height-1, 'right'), (
        ((18, 29), 'Select direction'),
    ))


def end():
    maingamescreen.drawables['use_arrows_label'].hidden = True
    maingamescreen.popup = None
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
