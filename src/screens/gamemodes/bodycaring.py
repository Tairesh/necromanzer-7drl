import tcod
import tcod.event
import engine
from engine.drawable.labels import Help
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.boxes import BoxParams
from engine.drawable.playerstatus import BodyInfo
from engine.item import ItemFlag
from engine.unit_actions import CutPartOff, ReplacePart, InsertPart
from items.bodyparts import BodyPart
from screens import maingamescreen
from . import moving


label = "BODY CARING"
body_info: BodyInfo
mode = 'view'


def change_mode(new_mode):
    global mode
    mode = new_mode
    body_info.set_mode(new_mode)
    maingamescreen.popup = None
    maingamescreen.drawables['replace_bodypart_select'] = None
    maingamescreen.drawables['insert_bodypart_name_select'] = None


def call(event):
    if mode == 'view':
        if event.type == 'KEYDOWN':
            if event.scancode == tcod.event.SCANCODE_ESCAPE:
                maingamescreen.change_mode(moving)
            elif engine.world.player.alive and event.scancode == tcod.event.SCANCODE_X and not event.mod & tcod.event.KMOD_LSHIFT:
                if not engine.world.player.wield_something_with_flag(ItemFlag.CAN_BUTCH):
                    engine.log.add_line("You can't butchering without a tool in hands!", tcod.orange)
                elif body_info.unit.alive and body_info.unit is not engine.world.player:
                    engine.log.add_line("You can't butchering the living creature!", tcod.orange)
                else:
                    change_mode('cut')
                    body_info.init_select()
            elif engine.world.player.alive and event.scancode == tcod.event.SCANCODE_R and not event.mod & tcod.event.KMOD_LSHIFT:
                if len(engine.world.player.body.parts_can_grab()):
                    change_mode('replace')
                    body_info.init_select()
                else:
                    engine.log.add_line("You can't replace body part without hands!", tcod.orange)
            elif engine.world.player.alive and event.scancode == tcod.event.SCANCODE_I and not event.mod & tcod.event.KMOD_LSHIFT:
                if body_info.unit.alive:
                    engine.log.add_line("You can't add body parts to living creatures!", tcod.orange)
                elif len(_get_nearest_parts()) == 0:
                    engine.log.add_line("You have no body parts to insert!", tcod.orange)
                elif len(engine.world.player.body.parts_can_grab()):
                    change_mode('insert')
                    parts = _get_nearest_parts()
                    maingamescreen.drawables['insert_bodypart_name_select'] = InteractiveSet('center', [
                        Button(None, item.displayed_name + (f" ({pos.to_direction(engine.world.player.pos)})" if pos else ''),
                               lambda btn, item=item, pos=pos: _insert(item, pos))
                        for (item, pos) in parts
                    ], 'vertical', BoxParams('Select body part to insert:'), 31)
                else:
                    engine.log.add_line("You can't add body parts without hands!", tcod.orange)

    elif mode == 'cut':
        if event.type == 'KEYDOWN':
            if event.scancode == tcod.event.SCANCODE_ESCAPE:
                body_info.unselect()
                change_mode('view')
            elif event.scancode in {tcod.event.SCANCODE_DOWN, tcod.event.SCANCODE_KP_2}:
                body_info.next()
            elif event.scancode in {tcod.event.SCANCODE_UP, tcod.event.SCANCODE_KP_8}:
                body_info.prev()
            elif event.scancode in {tcod.event.SCANCODE_RETURN, tcod.event.SCANCODE_KP_ENTER}:
                if body_info.selected_part_key == body_info.unit.body.root:
                    engine.log.add_line(f"You can't chop {body_info.selected_part_key} from {body_info.unit.name_apostrophe_s} body!", tcod.orange)
                elif body_info.unit.body.parts[body_info.selected_part_key] is None:
                    engine.log.add_line(f"{body_info.unit.name_apostrophe_s} {body_info.selected_part_key} already removed!", tcod.orange)
                else:
                    engine.world.player.current_action = CutPartOff(engine.world.player, body_info.unit, body_info.selected_part_key)
                    body_info.unselect()
                    change_mode('view')

    elif mode == 'replace':
        if event.type == 'KEYDOWN':
            if event.scancode == tcod.event.SCANCODE_ESCAPE:
                body_info.unselect()
                change_mode('view')
            elif event.scancode in {tcod.event.SCANCODE_DOWN, tcod.event.SCANCODE_KP_2}:
                body_info.next()
            elif event.scancode in {tcod.event.SCANCODE_UP, tcod.event.SCANCODE_KP_8}:
                body_info.prev()
            elif event.scancode in {tcod.event.SCANCODE_RETURN, tcod.event.SCANCODE_KP_ENTER}:
                if body_info.selected_part_key == body_info.unit.body.root:
                    engine.log.add_line(f"You can't replace {body_info.selected_part_key} in {body_info.unit.name_apostrophe_s} body!", tcod.orange)
                elif body_info.unit.body.parts[body_info.selected_part_key] is not None and body_info.unit.body.parts[body_info.selected_part_key].hp > 0:
                    engine.log.add_line(f"You should chop {body_info.unit.name_apostrophe_s} {body_info.selected_part_key} first!", tcod.orange)
                else:
                    _set_replace_select_mode()

    elif mode == 'replace_select':
        if event.type == 'KEYDOWN':
            if event.scancode == tcod.event.SCANCODE_ESCAPE:
                change_mode('replace')

    elif mode == 'insert':
        if event.type == 'KEYDOWN':
            if event.scancode == tcod.event.SCANCODE_ESCAPE:
                change_mode('view')


def _insert(item, pos):
    new_name = body_info.unit.body.name_for_new_part(item)
    engine.world.player.current_action = InsertPart(engine.world.player, body_info.unit, new_name, item, pos)
    change_mode('view')


def _get_nearest_parts(need_class=None):
    items = []
    body_parts = filter(lambda _: isinstance(_, BodyPart), engine.world.player.inventory)
    if need_class:
        body_parts = filter(lambda _: _.__class__ is need_class or any([_.proto == k for k in need_class.replacements]), body_parts)
    for item in body_parts:
        items.append((item, None))
    for i in (0, -1, 1):
        for j in (0, -1, 1):
            tile = engine.world.get_tile(engine.world.player.pos.x+i, engine.world.player.pos.y+j, False)
            body_parts = filter(lambda _: isinstance(_, BodyPart), tile.items)
            if need_class:
                body_parts = filter(lambda _: _.__class__ is need_class or any([_.proto == k for k in need_class.replacements]), body_parts)
            for item in body_parts:
                items.append((item, tile.pos))
    return items


def _set_replace_select_mode():
    need_class = body_info.unit.body.base_part_class(body_info.selected_part_key)
    parts = _get_nearest_parts(need_class)
    if len(parts) == 0:
        engine.log.add_line(f"You can't replace {body_info.selected_part_key} without compatible parts!",
                            tcod.orange)
        return

    change_mode('replace_select')
    maingamescreen.popup = InteractiveSet('center', [
        Button(None, item.displayed_name + (f" ({pos.to_direction(engine.world.player.pos)})" if pos else ''),
               lambda btn, item=item, pos=pos: _replace(item, pos))
        for (item, pos) in parts
    ], 'vertical', BoxParams("Select body part to replace:"), 32)
    maingamescreen.drawables['replace_bodypart_select'] = maingamescreen.popup


def _replace(item, pos):
    engine.world.player.current_action = ReplacePart(engine.world.player, body_info.unit,
                                                     body_info.selected_part_key, item, pos)
    body_info.unselect()
    change_mode('view')


def tick():
    body_info.render()


def draw():
    pass


def start(data):
    global body_info
    body_info = BodyInfo(data['unit'])
    change_mode('view')
    maingamescreen.drawables['at_screen'] = body_info

    maingamescreen.drawables['help_label_row3'] = Help((engine.console.width, engine.console.height-3, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height-2, 'right'), (
        ('i', 'Insert new part'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height - 1, 'right'), (
        ('r', 'Replace'),
        ('x', 'Cut off'),
    ))


def end():
    maingamescreen.popup = None
    maingamescreen.drawables['at_screen'] = None
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
    maingamescreen.drawables['help_label_row3'] = None
    maingamescreen.drawables['replace_bodypart_select'] = None
