import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.labels import Help
from screens import maingamescreen
from time import time
from . import observe, reading, wield, drop, dig, grab, examine, smash, animate, opening, closing, bodycaring, \
    inventory, pickup, orders, view, menu


label = "MOVING"
moving_keys_pressed = set()


def call(event):
    player = engine.world.player
    if event.type == 'KEYDOWN':
        if event.scancode in keys.DIRECTION_KEYS and not event.mod & tcod.event.KMOD_LSHIFT:
            direction = keys.DIRECTION_KEYS[event.scancode]
            if direction == keys.DIR_C:
                maingamescreen.skipping_time = True
            elif not event.repeat and direction not in moving_keys_pressed:
                moving_keys_pressed.add(direction)
        elif event.scancode == tcod.event.SCANCODE_SLASH and event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.popup = maingamescreen.drawables['help_window']
            maingamescreen.popup.hidden = False
            moving_keys_pressed.clear()
        elif event.scancode == tcod.event.SCANCODE_X and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(observe)
        elif player.alive and event.scancode == tcod.event.SCANCODE_R and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(reading)
        elif player.alive and event.scancode == tcod.event.SCANCODE_W and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(wield)
        elif player.alive and event.scancode == tcod.event.SCANCODE_D and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(drop)
        elif player.alive and event.scancode == tcod.event.SCANCODE_D and event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(dig)
        elif player.alive and event.scancode == tcod.event.SCANCODE_G and event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(grab)
        elif player.alive and event.scancode == tcod.event.SCANCODE_G and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(pickup)
        elif event.scancode == tcod.event.SCANCODE_E and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(examine)
        elif player.alive and event.scancode == tcod.event.SCANCODE_S and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(smash)
        elif player.alive and event.scancode == tcod.event.SCANCODE_A and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(animate)
        elif player.alive and event.scancode == tcod.event.SCANCODE_O and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(opening)
        elif player.alive and event.scancode == tcod.event.SCANCODE_C and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(closing)
        elif event.scancode == tcod.event.SCANCODE_V and event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(view)
        elif player.alive and event.scancode == tcod.event.SCANCODE_1 and event.mod & tcod.event.KMOD_LSHIFT:
            engine.world.player.walkmode = 1 if engine.world.player.walkmode == 0 else 0
            maingamescreen.drawables['player_status'].render()
        elif event.scancode == tcod.event.SCANCODE_2 and event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(bodycaring, {'unit': engine.world.player})
        elif event.scancode in {tcod.event.SCANCODE_KP_5, tcod.event.SCANCODE_PERIOD} and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.skipping_time = True
        elif event.scancode == tcod.event.SCANCODE_P and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.popup = maingamescreen.drawables['log_window']
            maingamescreen.popup.log = engine.world.log
            maingamescreen.popup.show()
            moving_keys_pressed.clear()
        elif event.scancode == tcod.event.SCANCODE_P and event.mod & tcod.event.KMOD_LSHIFT:
            engine.log.lines.clear()
        elif event.scancode == tcod.event.SCANCODE_I and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(inventory)
        elif player.alive and event.scancode == tcod.event.SCANCODE_O and event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.change_mode(orders)
        elif event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(menu)
    elif event.type == 'KEYUP':
        if event.scancode in keys.DIRECTION_KEYS:
            direction = keys.DIRECTION_KEYS[event.scancode]
            if direction == keys.DIR_C:
                maingamescreen.skipping_time = False
            elif direction in moving_keys_pressed:
                moving_keys_pressed.remove(direction)
        elif event.scancode in {tcod.event.SCANCODE_KP_5, tcod.event.SCANCODE_PERIOD} and not event.mod & tcod.event.KMOD_LSHIFT:
            maingamescreen.skipping_time = False


def tick():
    dx, dy = 0, 0
    for direction in moving_keys_pressed:
        delta_x, delta_y = keys.DIRECTION_DELTA[direction]
        dx += delta_x
        dy += delta_y
    dx = int(dx/abs(dx)) if dx != 0 else 0
    dy = int(dy/abs(dy)) if dy != 0 else 0
    if engine.world.player.alive and engine.world.player.moving_keys_process(dx, dy):
        maingamescreen.last_world_tick = time()


def draw():
    pass


def start():
    moving_keys_pressed.clear()
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height-1, 'right', 'top'), (
        ((18, 29), 'Move'),
        ('?', 'Help'),
    ))


def end():
    maingamescreen.drawables['help_label'] = None
