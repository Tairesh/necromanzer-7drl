import tcod
import tcod.event
import engine
from engine.tile import MapPos
from screens import maingamescreen
from . import moving
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.boxes import BoxParams


label = "PICK UP"


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)


def tick():
    pass


def draw():
    pass


def _get_items_to_pickup(x, y=None) -> list:
    if type(x) is MapPos:
        x, y = x.x, x.y
    elif y is None:
        x, y = x

    items = []
    for i in (0, -1, 1):
        for j in (0, -1, 1):
            tile = engine.world.get_tile(x+i, y+j, False)
            for item in filter(lambda _: _.is_grabable and _.mass < 100.0, tile.items):
                items.append((item, tile.pos))

    return items


def start():
    items_around = _get_items_to_pickup(engine.world.player.pos)
    if len(items_around) == 0:
        engine.log.add_line("Nothing to pick up here", tcod.orange)
        maingamescreen.change_mode(moving)
    elif len(items_around) == 1:
        _pickup(*items_around[0])
    else:
        select = InteractiveSet((engine.console.width//2, engine.console.height//2, 'center', 'center'), [
            Button(None, f"{item.displayed_name} ({pos.to_direction(engine.world.player.pos)})", lambda btn, item=item, pos=pos: _pickup(item, pos)) for item, pos in items_around
        ], 'vertical', BoxParams("Select an item to pick up"), min_width=29)
        maingamescreen.drawables['select_item_to_wield'] = select
        maingamescreen.popup = select


def _pickup(item, pos):
    tile = engine.world.get_tile(pos)
    if item in tile.items:
        tile.items.remove(item)
        engine.world.player.inventory.append(item)
        engine.log.add_line(f"You picked up {item.title}")
        maingamescreen.drawables['view'].render()
    else:
        engine.log.add_line(f"{item.title.capitalize()} moved away!", tcod.red)
    maingamescreen.change_mode(moving)


def end():
    maingamescreen.drawables['select_item_to_wield'] = None
    maingamescreen.popup = None
