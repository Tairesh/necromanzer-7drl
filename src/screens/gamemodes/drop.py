import random

import tcod
import tcod.event
import engine
from engine import keys
from screens import maingamescreen
from . import moving
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.boxes import BoxParams


label = "DROPPING"
selected_to_drop = None


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)
            return

    if selected_to_drop is not None:
        if event.type == 'KEYDOWN':
            if event.scancode in keys.DIRECTION_KEYS:
                direction = keys.DIRECTION_KEYS[event.scancode]
                dx, dy = keys.DIRECTION_DELTA[direction]
                _drop(dx, dy)


def tick():
    pass


def draw():
    pass


def _drop(dx, dy):
    item, part_key = selected_to_drop
    if part_key is not None:
        for key in engine.world.player.wield:
            if engine.world.player.wield[key] is item:
                engine.world.player.wield[key] = None
    else:
        engine.world.player.inventory.remove(item)
    pos_x, pos_y = engine.world.player.pos.x + dx, engine.world.player.pos.y + dy
    engine.world.get_tile(pos_x, pos_y).items.append(item)
    engine.log.add_line(f"You dropped {item.title}")
    maingamescreen.drawables['player_status'].render()
    maingamescreen.drawables['view'].render()
    maingamescreen.change_mode(moving)


def _start_dropping(item, part_key):
    global selected_to_drop
    maingamescreen.drawables['select_item_to_drop'] = None
    maingamescreen.popup = maingamescreen.drawables['use_arrows_label']
    maingamescreen.popup.hidden = False
    selected_to_drop = (item, part_key)


def _sort_hands_func(hand_key) -> int:
    if engine.world.player.raw_params['preferred_hand'] == 0:
        return 1 if hand_key.startswith('left') else 0
    elif engine.world.player.raw_params['preferred_hand'] == 1:
        return 1 if hand_key.startswith('right') else 0
    elif engine.world.player.raw_params['preferred_hand'] == 2:
        return random.randint(0, 9)
    return 0


def _get_items_to_drop() -> list:
    items = [(item, None) for item in engine.world.player.inventory]
    keys_by_item = {}
    for key, item in sorted(engine.world.player.wield.items(), key=lambda row: _sort_hands_func(row[0])):
        if item and item not in keys_by_item:
            keys_by_item[item] = key
    return items + list(keys_by_item.items())


def start():
    items_to_drop = _get_items_to_drop()
    if len(items_to_drop) == 0:
        engine.log.add_line(f"You have nothing to drop", tcod.orange)
        maingamescreen.change_mode(moving)
        return

    select = InteractiveSet((engine.console.width//2, engine.console.height//2, 'center', 'center'), [
        Button(None, (part_key.capitalize() + ': ' if part_key else '') + item.displayed_name,
               lambda btn, item=item, part_key=part_key: _start_dropping(item, part_key)) for item, part_key in items_to_drop
    ], 'vertical', BoxParams("Select an item to drop"), min_width=26)
    maingamescreen.drawables['select_item_to_drop'] = select
    maingamescreen.popup = select


def end():
    maingamescreen.drawables['select_item_to_drop'] = None
    if maingamescreen.popup:
        maingamescreen.popup.hidden = True
        maingamescreen.popup = None
    global selected_to_drop
    selected_to_drop = None
