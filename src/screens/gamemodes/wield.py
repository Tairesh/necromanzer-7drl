import random
import tcod
import tcod.event
import engine
from engine.tile import MapPos
from screens import maingamescreen
from . import moving
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.boxes import BoxParams


label = "WIELD"
parts_can_grab: list
free_hands_keys: list


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)


def tick():
    pass


def draw():
    pass


def _get_items_to_wield(x, y=None) -> list:
    if type(x) is MapPos:
        x, y = x.x, x.y
    elif y is None:
        x, y = x

    items = []
    for item in filter(lambda _: _.hands_to_grab <= len(free_hands_keys), engine.world.player.inventory):
        items.append((item, None))
    for i in (0, -1, 1):
        for j in (0, -1, 1):
            tile = engine.world.get_tile(x+i, y+j, False)
            for item in filter(lambda _: _.is_grabable and _.hands_to_grab <= len(free_hands_keys), tile.items):
                items.append((item, tile.pos))

    return items


def _sort_hands_func(hand_key) -> int:
    if engine.world.player.raw_params['preferred_hand'] == 0:
        return 1 if hand_key.startswith('left') else 0
    elif engine.world.player.raw_params['preferred_hand'] == 1:
        return 1 if hand_key.startswith('right') else 0
    elif engine.world.player.raw_params['preferred_hand'] == 2:
        return random.randint(0, 9)
    return 0


def start():
    global parts_can_grab, free_hands_keys
    parts_can_grab = engine.world.player.body.parts_can_grab()
    if len(parts_can_grab) == 0:
        engine.log.add_line("You can't wield without arms!", tcod.orange)
        maingamescreen.change_mode(moving)
        return

    free_hands_keys = [key for key in parts_can_grab if key not in engine.world.player.wield or not engine.world.player.wield[key]]
    if len(free_hands_keys) == 0:
        engine.log.add_line("You can't wield more items!", tcod.orange)
        maingamescreen.change_mode(moving)
        return

    free_hands_keys = sorted(free_hands_keys, key=_sort_hands_func)

    items_around = _get_items_to_wield(engine.world.player.pos)
    if len(items_around) == 0:
        engine.log.add_line("Nothing to wield here", tcod.orange)
        maingamescreen.change_mode(moving)
    elif len(items_around) == 1:
        _wield(*items_around[0])
    else:
        select = InteractiveSet((engine.console.width//2, engine.console.height//2, 'center', 'center'), [
            Button(None, item.displayed_name + (f" ({pos.to_direction(engine.world.player.pos)})" if pos else ''),
                   lambda btn, item=item, pos=pos: _wield(item, pos)) for item, pos in items_around
        ], 'vertical', BoxParams("Select an item to wield"), min_width=27)
        maingamescreen.drawables['select_item_to_wield'] = select
        maingamescreen.popup = select


def _wield(item, pos):
    if pos is not None:
        tile = engine.world.get_tile(pos)
        if item not in tile.items:
            engine.log.add_line(f"{item.title.capitalize()} moved away!", tcod.red)
            maingamescreen.change_mode(moving)
            return
        else:
            tile.items.remove(item)
    else:
        engine.world.player.inventory.remove(item)

    selected_parts = free_hands_keys[:item.hands_to_grab]
    for part in selected_parts:
        engine.world.player.wield[part] = item
    engine.log.add_line(f"You wield {item.title} in your {' and '.join(selected_parts)}")
    maingamescreen.drawables['player_status'].render()
    maingamescreen.drawables['view'].render()
    maingamescreen.change_mode(moving)


def end():
    maingamescreen.drawables['select_item_to_wield'] = None
    maingamescreen.popup = None
