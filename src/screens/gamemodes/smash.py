import random
import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.labels import Help
from screens import maingamescreen
from . import moving
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.boxes import BoxParams


label = "SMASHING"
selected_target = None


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)

    if selected_target:
        return

    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)
        elif event.scancode in keys.DIRECTION_KEYS:
            direction = keys.DIRECTION_KEYS[event.scancode]
            dx, dy = keys.DIRECTION_DELTA[direction]
            _set_target(dx, dy)


def tick():
    pass


def draw():
    pass


def _get_parts_keys_to_attack() -> list:
    return [key for key, part in engine.world.player.body.parts.items() if part and part.can_attack]


def _try_attack(part_key):
    global selected_target

    tx, ty = engine.world.player.pos.x + selected_target[0], engine.world.player.pos.y + selected_target[1]
    engine.world.player.start_smash((tx, ty), part_key)

    selected_target = None
    maingamescreen.drawables['select_part_to_attack'] = None
    maingamescreen.popup = None
    maingamescreen.change_mode(moving)


def _weapon_name(part_key):
    if part_key in engine.world.player.wield and engine.world.player.wield[part_key]:
        return engine.world.player.wield[part_key].title
    else:
        return "(empty)"


def _preferred_key(keys_to_attack: list):
    prior_right = ('right arm', 'left arm', 'right leg', 'left leg')
    prior_left = ('left arm', 'right arm', 'left leg', 'right leg')
    if engine.world.player.raw_params['preferred_hand'] == 0:
        prior = prior_right
    elif engine.world.player.raw_params['preferred_hand'] == 1:
        prior = prior_left
    elif engine.world.player.raw_params['preferred_hand'] == 2:
        if random.random() > 0.5:
            prior = prior_left
        else:
            prior = prior_right
    else:
        prior = ()

    for pr in prior:
        if pr in keys_to_attack and pr in engine.world.player.wield and engine.world.player.wield[pr]:
            return pr

    for pr in prior:
        if pr in keys_to_attack:
            return pr
    return keys_to_attack[0]


def _set_target(dx, dy):
    global selected_target
    selected_target = (dx, dy)

    maingamescreen.popup.hidden = True
    maingamescreen.popup = None

    if dx == 0 and dy == 0:
        engine.log.add_line(f"You can't attack yourself!", tcod.orange)
        maingamescreen.change_mode(moving)
        return

    keys_to_attack = _get_parts_keys_to_attack()
    if len(keys_to_attack) == 0:
        engine.log.add_line(f"You have no body part to attack!", tcod.orange)
        maingamescreen.change_mode(moving)
        return

    buttons = [Button(None, f"{part_key.capitalize()}: {_weapon_name(part_key)}",
                      lambda btn, part_key=part_key: _try_attack(part_key))
               for part_key in keys_to_attack]
    preferred_button = buttons[keys_to_attack.index(_preferred_key(keys_to_attack))]

    select = InteractiveSet((engine.console.width//2, engine.console.height//2, 'center', 'center'), buttons, 'vertical', BoxParams("Select attack type"), min_width=22)

    select.set_selected_entity(preferred_button)
    maingamescreen.drawables['select_part_to_attack'] = select
    maingamescreen.popup = select


def start():
    global selected_target
    selected_target = None

    keys_to_attack = _get_parts_keys_to_attack()
    if len(keys_to_attack) == 0:
        engine.log.add_line(f"You have no body part to attack!", tcod.orange)
        maingamescreen.change_mode(moving)
        return

    maingamescreen.popup = maingamescreen.drawables['use_arrows_label']
    maingamescreen.popup.hidden = False

    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height-2, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height-1, 'right'), (
        ((18, 29), 'Select direction'),
    ))


def end():
    global selected_target
    selected_target = None
    if maingamescreen.popup:
        maingamescreen.popup.hidden = True
        maingamescreen.popup = None

    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
