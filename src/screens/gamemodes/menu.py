import tcod
import tcod.event
import engine
from engine import savefile
from engine.drawable.labels import Help
from screens import maingamescreen, mainmenu
from . import moving
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.boxes import BoxParams


label = "MENU"


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)


def tick():
    pass


def draw():
    pass


def _main_menu():
    savefile.save(engine.world)
    engine.change_screen(mainmenu)


def start():
    maingamescreen.drawables['menu'] = InteractiveSet('center', [
        Button(None, '[c] Continue', lambda btn: maingamescreen.change_mode(moving), tcod.event.SCANCODE_C),
        Button(None, '[q] Quit to menu', lambda btn: _main_menu(), tcod.event.SCANCODE_Q),
        Button(None, '[x] Exit game', lambda btn: engine.stop(), tcod.event.SCANCODE_X),
    ], 'vertical', BoxParams(" Menu "), 20, True)
    maingamescreen.popup = maingamescreen.drawables['menu']
    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height - 2, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height - 1, 'right'), (
        (18, 'Select'),
        ('Enter', 'Confirm'),
    ))


def end():
    maingamescreen.drawables['menu'] = None
    maingamescreen.popup = None
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
