import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.labels import Help
from screens import maingamescreen
from . import moving
from engine.item import ItemFlag


label = "DIGGING"


def _try_dig(dx, dy):
    if dx == 0 and dy == 0:
        engine.log.add_line(f"Don't dig straight down!", tcod.orange)
    else:
        tx, ty = engine.world.player.pos.x + dx, engine.world.player.pos.y + dy
        tile = engine.world.get_tile(tx, ty, False)
        if tile.base.can_be_digged:
            engine.world.player.start_dig((tx, ty))
        else:
            engine.log.add_line(f"You can't dig a {tile.base.title}", tcod.orange)

    maingamescreen.change_mode(moving)


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.change_mode(moving)
        elif event.scancode in keys.DIRECTION_KEYS:
            direction = keys.DIRECTION_KEYS[event.scancode]
            dx, dy = keys.DIRECTION_DELTA[direction]
            _try_dig(dx, dy)


def tick():
    pass


def draw():
    pass


def start():
    if not engine.world.player.wield_something_with_flag(ItemFlag.CAN_DIG):
        engine.log.add_line(f"You can't dig without a tool", tcod.orange)
        maingamescreen.change_mode(moving)
        return

    maingamescreen.popup = maingamescreen.drawables['use_arrows_label']
    maingamescreen.popup.hidden = False
    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height-2, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height-1, 'right'), (
        ((18, 29), 'Select direction'),
    ))


def end():
    if maingamescreen.popup:
        maingamescreen.popup.hidden = True
    maingamescreen.popup = None
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
