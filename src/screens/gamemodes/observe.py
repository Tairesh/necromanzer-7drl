import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.labels import Help
from screens import maingamescreen
from . import moving
from time import time
from engine.drawable.boxes import TileInfoBox


label = "OBSERVE"
moving_keys_pressed = set()
shift_pressed = False
last_moved_time = 0
MOVING_TIMEOUT = 0.07
blink_last_time = 0
BLINK_PERIOD = 0.5
cursor = None


def call(event):
    global shift_pressed
    if event.type == 'KEYDOWN':
        if event.scancode in keys.DIRECTION_KEYS and not event.repeat:
            direction = keys.DIRECTION_KEYS[event.scancode]
            if direction not in moving_keys_pressed:
                moving_keys_pressed.add(direction)
        elif event.scancode == tcod.event.SCANCODE_LSHIFT:
            shift_pressed = True
        elif event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.drawables['view'].move_to(engine.world.player.pos)
            maingamescreen.change_mode(moving)
    elif event.type == 'KEYUP':
        if event.scancode in keys.DIRECTION_KEYS and not event.repeat:
            direction = keys.DIRECTION_KEYS[event.scancode]
            if direction in moving_keys_pressed:
                moving_keys_pressed.remove(direction)
        elif event.scancode == tcod.event.SCANCODE_LSHIFT:
            shift_pressed = False
    elif event.type == "MOUSEMOTION":
        view = maingamescreen.drawables['view']
        global cursor, blink_last_time
        cursor = view.screen_to_map_pos(event.tile.x, event.tile.y)
        maingamescreen.drawables['observe_tile_box'] = TileInfoBox(engine.world.get_tile(cursor, change=False))
        blink_last_time = time()-BLINK_PERIOD


def tick():
    global last_moved_time, cursor
    if len(moving_keys_pressed) and time() > last_moved_time+MOVING_TIMEOUT:
        view = maingamescreen.drawables['view']
        dx, dy = 0, 0
        for direction in moving_keys_pressed:
            delta_x, delta_y = keys.DIRECTION_DELTA[direction]
            dx += delta_x
            dy += delta_y
        dx = int(dx / abs(dx)) if dx != 0 else 0
        dy = int(dy / abs(dy)) if dy != 0 else 0
        if shift_pressed:
            dx *= 10
            dy *= 10
        view.move(dx, dy)
        cursor = (view.center_x, view.center_y)
        maingamescreen.drawables['observe_tile_box'] = TileInfoBox(engine.world.get_tile(cursor, change=False))
        last_moved_time = time()


def draw():
    if maingamescreen.popup:
        return

    global blink_last_time
    if time() > blink_last_time + BLINK_PERIOD*2:
        blink_last_time = time()
    elif time() > blink_last_time + BLINK_PERIOD:
        view = maingamescreen.drawables['view']
        tx, ty = view.map_pos_to_screen(cursor)
        engine.console.default_fg = tcod.yellow
        engine.console.put_char(tx, ty, 88)


def start():
    moving_keys_pressed.clear()
    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height-2, 'right'), (
        ('Shift', 'x10 speed'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height-1, 'right'), (
        ((18, 29), 'Move cursor'),
    ))
    maingamescreen.drawables['observe_tile_box'] = TileInfoBox(engine.world.get_tile(engine.world.player.pos, change=False))

    view = maingamescreen.drawables['view']
    global cursor
    cursor = (view.center_x, view.center_y)


def end():
    maingamescreen.drawables['observe_tile_box'] = None
    maingamescreen.drawables['help_label'] = None
