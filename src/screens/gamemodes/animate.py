import tcod
import tcod.event
import engine
from engine import keys
from engine.drawable.labels import Label, Help
from engine.drawable.interactive import InteractiveSet
from engine.drawable.buttons import Button
from engine.drawable.boxes import BoxParams
from screens import maingamescreen
from . import moving
from time import time


label = "ANIMATE UNDEAD"
moving_keys_pressed = set()
shift_pressed = False
last_moved_time = 0
MOVING_TIMEOUT = 0.07
blink_last_time = 0
BLINK_PERIOD = 0.5
cursor = None


def call(event):
    global shift_pressed

    if maingamescreen.popup:
        return

    if event.type == 'KEYDOWN':
        if event.scancode in keys.DIRECTION_KEYS:
            direction = keys.DIRECTION_KEYS[event.scancode]
            if direction == keys.DIR_C:
                maingamescreen.skipping_time = True
            elif not event.repeat and direction not in moving_keys_pressed:
                moving_keys_pressed.add(direction)
        elif event.scancode == tcod.event.SCANCODE_LSHIFT:
            shift_pressed = True
        elif event.scancode == tcod.event.SCANCODE_ESCAPE:
            maingamescreen.drawables['view'].move_to(engine.world.player.pos)
            maingamescreen.change_mode(moving)
        elif event.scancode in keys.SELECT_KEYS:
            _click()
    elif event.type == 'KEYUP':
        if event.scancode in keys.DIRECTION_KEYS:
            direction = keys.DIRECTION_KEYS[event.scancode]
            if direction == keys.DIR_C:
                maingamescreen.skipping_time = False
            elif direction in moving_keys_pressed:
                moving_keys_pressed.remove(direction)
        elif event.scancode == tcod.event.SCANCODE_LSHIFT:
            shift_pressed = False
    elif event.type == "MOUSEMOTION":
        _set_cursor(event.tile.x, event.tile.y)
    elif event.type == "MOUSEBUTTONDOWN" and event.button == tcod.event.BUTTON_LEFT:
        _set_cursor(event.tile.x, event.tile.y)
        _click()


def _set_cursor(screen_x, screen_y):
    global cursor, blink_last_time
    cursor = maingamescreen.drawables['view'].screen_to_map_pos(screen_x, screen_y)
    blink_last_time = time()-BLINK_PERIOD


def _try_animate(unit):
    engine.world.player.start_animate(unit)
    maingamescreen.change_mode(moving)


def _click():
    animation_candidates = [unit for unit in engine.world.get_tile(cursor, change=False).units if not unit.alive]
    if len(animation_candidates) == 0:
        engine.log.add_line("There is no one to animate!", tcod.orange)
    elif len(animation_candidates) == 1:
        _try_animate(animation_candidates[0])
    else:
        select = InteractiveSet((engine.console.width//2, engine.console.height//2, 'center', 'center'), [
            Button(None, unit.name, lambda btn, unit=unit: _try_animate(unit)) for unit in animation_candidates
        ], 'vertical', BoxParams("Select body to animate:"), min_width=27)
        maingamescreen.drawables['animate_target_select'] = select
        maingamescreen.popup = select


def tick():
    global last_moved_time, cursor, blink_last_time
    if len(moving_keys_pressed) and time() > last_moved_time+MOVING_TIMEOUT:
        view = maingamescreen.drawables['view']
        dx, dy = 0, 0
        for direction in moving_keys_pressed:
            delta_x, delta_y = keys.DIRECTION_DELTA[direction]
            dx += delta_x
            dy += delta_y
        if shift_pressed:
            dx *= 10
            dy *= 10
        view.move(dx, dy)
        cursor = (view.center_x, view.center_y)
        last_moved_time = time()
        blink_last_time = time()-BLINK_PERIOD


def draw():
    if maingamescreen.popup:
        return

    global blink_last_time
    if time() > blink_last_time + BLINK_PERIOD*2:
        blink_last_time = time()
    elif time() > blink_last_time + BLINK_PERIOD:
        view = maingamescreen.drawables['view']
        tx, ty = view.map_pos_to_screen(cursor)
        engine.console.default_fg = tcod.yellow
        engine.console.put_char(tx, ty, 88)


def start():
    global cursor
    moving_keys_pressed.clear()
    view = maingamescreen.drawables['view']
    cursor = (view.center_x, view.center_y)

    help_label = Label((engine.console.width//2, 1, 'center', 'top'),
                       'Use arrows and Enter or mouse to select target:')
    maingamescreen.drawables['animate_help_label'] = help_label

    maingamescreen.drawables['help_label_row3'] = Help((engine.console.width, engine.console.height-3, 'right'), (
        ('Enter', 'Confirm'),
    ))
    maingamescreen.drawables['help_label_row2'] = Help((engine.console.width, engine.console.height-2, 'right'), (
        ('Esc', 'Cancel'),
    ))
    maingamescreen.drawables['help_label'] = Help((engine.console.width, engine.console.height-1, 'right'), (
        ((18, 29), 'Move cursor'),
    ))


def end():
    maingamescreen.drawables['animate_help_label'] = None
    maingamescreen.drawables['animate_target_select'] = None
    maingamescreen.popup = None
    view = maingamescreen.drawables['view']
    view.move_to(engine.world.player.pos)
    maingamescreen.drawables['help_label'] = None
    maingamescreen.drawables['help_label_row2'] = None
    maingamescreen.drawables['help_label_row3'] = None
