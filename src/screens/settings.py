import tcod
import tcod.event
import engine
from engine import consts
from . import mainmenu
from engine.settings import save as save_settings, validate as validate_settings, correct as correct_settings
from engine.drawable.interactive import InteractiveSet
from engine.drawable.inputs import NumberSelect
from engine.drawable.boxes import BoxParams, Box
from engine.drawable.labels import Label, Help


# noinspection PyTypeChecker
big_box: Box = None
big_box_pos = None
# noinspection PyTypeChecker
settings_set: InteractiveSet = None
settings_set_pos = None
# noinspection PyTypeChecker
label_width: Label = None
# noinspection PyTypeChecker
label_height: Label = None
# noinspection PyTypeChecker
help_label: Label = None
help_label_pos = None

# noinspection PyTypeChecker
new_settings: dict = None


def _upd_pos():
    global big_box_pos, settings_set_pos, help_label_pos
    big_box_pos = (engine.console.width//2-2, engine.console.height//2, 'center', 'center')
    settings_set_pos = (engine.console.width//2, engine.console.height//2, 'left', 'center')
    help_label_pos = (engine.console.width//2, engine.console.height-1, 'center', 'bottom')


def init():
    _upd_pos()

    global settings_set, big_box, label_width, label_height, help_label, new_settings

    new_settings = engine.settings.copy()

    settings_set = InteractiveSet(settings_set_pos, [
        NumberSelect(None, 5, engine.settings['width'], 80, 300, on_change=lambda inp: _value_updated('width', inp.value)),
        NumberSelect(None, 5, engine.settings['height'], 30, 100, on_change=lambda inp: _value_updated('height', inp.value)),
    ], 'vertical')
    label_x = settings_set.pos.left - 8
    label_y = settings_set.pos.top
    label_width = Label((label_x, label_y), "Width:", target=settings_set.entities[0])
    label_height = Label((label_x, label_y+1), "Height:", target=settings_set.entities[1])

    big_box = Box(big_box_pos, 18, 4, BoxParams("Settings"))
    help_label = Help(help_label_pos, (
        ('ESC', 'Back'),
        ('Enter', 'Apply'),
        (18, 'Select'),
        (29, 'Change'),
    ))


def _value_updated(key, val):
    new_settings[key] = val


def _save_settings():
    global new_settings

    if not validate_settings(new_settings):
        new_settings = correct_settings(new_settings)

    if engine.console.width == new_settings['width'] and engine.console.height == new_settings['height']:
        return

    engine.settings = new_settings.copy()

    engine.console = tcod.console_init_root(engine.settings['width'], engine.settings['height'], consts.WINDOW_TITLE, order='F', vsync=True)
    _upd_pos()
    save_settings(engine.settings)

    settings_set.set_pos(settings_set_pos)
    label_x = settings_set.pos.left - 8
    label_y = settings_set.pos.top
    label_width.set_pos((label_x, label_y))
    label_height.set_pos((label_x, label_y+1))
    settings_set.entities[0].value = engine.settings['width']
    settings_set.entities[1].value = engine.settings['height']
    big_box.set_pos(big_box_pos)
    help_label.set_pos(help_label_pos)


def call(event):
    if event.type == 'KEYDOWN':
        if event.scancode in {tcod.event.SCANCODE_RETURN, tcod.event.SCANCODE_KP_ENTER}:
            _save_settings()
    elif event.type == 'KEYUP':
        if event.scancode == tcod.event.SCANCODE_ESCAPE:
            _save_settings()
            engine.change_screen(mainmenu)


def tick():
    pass


def get_entities():
    return big_box, settings_set, label_width, label_height, help_label


def draw():
    pass
