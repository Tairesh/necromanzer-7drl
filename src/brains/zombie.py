import random

import engine
from engine import astar
from engine.astar import PathNotFound
from engine.brain import Brain, Fraction, BrainProto
from engine.orders import OrderProto


class ZombieBrain(Brain):

    fraction = Fraction.ZOMBIE
    proto = BrainProto.ZOMBIE
    need_head = False
    speed_factor = 3.0

    def plan(self):
        self._upd()
        if self.master.current_action:
            return

        from engine.unit_actions import Move

        if len(self.path_to_move) == 0 and 'path_not_found' not in self.data:
            target_point = None
            if engine.world.order.proto == OrderProto.FOLLOW_ME:
                tile = engine.world.get_tile(engine.world.player.pos.x, engine.world.player.pos.y, False, True)
                if tile:
                    target_point = tile.free_pos_around()
            elif engine.world.order.proto == OrderProto.MOVE_TO:
                tile = engine.world.get_tile(*engine.world.order.data, False, True)
                if tile:
                    target_point = tile.free_pos_around()
            elif engine.world.order.proto == OrderProto.KILL:
                unit = engine.world.units[engine.world.order.data]
                tile = engine.world.get_tile(unit.pos.x, unit.pos.y, False, True)
                if tile:
                    target_point = tile.free_pos_around()
            elif engine.world.order.proto == OrderProto.NONE:
                enemy = None
                for unit in engine.world.units.values():
                    d = unit.pos.dist_to(self.master.pos)
                    if d < 20 and unit.brain and unit.brain.fraction in self.fraction.enemies:
                        if not enemy or enemy.pos.dist_to(self.master.pos) > d:
                            enemy = unit
                if enemy:
                    tile = engine.world.get_tile(enemy.pos.x, enemy.pos.y, False, True)
                    if tile:
                        target_point = tile.free_pos_around()
            if target_point:
                try:
                    path = astar.run((self.master.pos.x, self.master.pos.y), target_point)
                    self.path_to_move = path
                except PathNotFound:
                    self.data['path_not_found'] = engine.world.current_tick

        enemy = None
        if engine.world.order.proto == OrderProto.KILL:
            unit = engine.world.units[engine.world.order.data]
            dx, dy = unit.pos.x - self.master.pos.x, unit.pos.y - self.master.pos.y
            if unit.alive and abs(dx) <= 1 and abs(dy) <= 1:
                enemy = unit

        if enemy is None:
            neighbours = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]
            random.shuffle(neighbours)
            for dx, dy in neighbours:
                tile = engine.world.get_tile(self.master.pos.x + dx, self.master.pos.y + dy, False, True)
                if tile:
                    for unit in tile.units:
                        if unit.brain and unit.brain.fraction in self.fraction.enemies:
                            enemy = unit
                            break
                if enemy:
                    break

        if enemy and len(self.master.body.parts_can_attack()):
            self.master.start_smash((enemy.pos.x, enemy.pos.y), random.choice(self.master.body.parts_can_attack()))
            return

        if len(self.path_to_move):
            dx, dy = self.path_to_move_next
            tile = engine.world.get_tile(self.master.pos.x + dx, self.master.pos.y + dy, False)
            if tile.is_walkable:
                self.master.current_action = Move(self.master, (dx, dy))
            else:
                dont_attack = False
                for unit in tile.units:
                    if (unit.brain and unit.brain.fraction not in self.fraction.enemies) or \
                            unit is engine.world.player:
                        dont_attack = True
                        break
                if not dont_attack and len(self.master.body.parts_can_attack()):
                    self.master.start_smash((tile.pos.x, tile.pos.y), random.choice(self.master.body.parts_can_attack()))
                self.path_to_move = []
            return

        self.master.current_action = Move(self.master, (random.randint(-1, 1), random.randint(-1, 1)))
