import random

import tcod

import engine
from engine import astar
from engine.astar import PathNotFound
from engine.brain import Brain, BrainProto, Fraction


class DogBrain(Brain):

    proto = BrainProto.DOG
    fraction = Fraction.VILLAGERS
    need_head = True
    speed_factor = 2.0

    @property
    def fear(self) -> bool:
        if self.data and 'fear' in self.data:
            return self.data['fear']
        else:
            return False

    @fear.setter
    def fear(self, value: bool):
        if not self.data:
            self.data = {}
        self.data['fear'] = value

    def _upd(self):
        super()._upd()

        if not self.fear:
            for unit in engine.world.units.values():
                if unit.alive and unit.brain and unit.brain.fraction in self.fraction.enemies\
                        and unit.pos.dist_to(self.master.pos) < 20:
                    self.fear = True
                    self.path_to_move = []
                    engine.log.add_line(self.master.displayed_name.capitalize() + ': Rrrrr!', tcod.lighter_azure)
                    break
        elif self.fear:
            min_dist_to_enemy = 9999
            for unit in engine.world.units.values():
                if unit.alive and unit.brain and unit.brain.fraction in self.fraction.enemies\
                        and unit.pos.dist_to(self.master.pos) < min_dist_to_enemy:
                    min_dist_to_enemy = unit.pos.dist_to(self.master.pos)
            if min_dist_to_enemy > 40:
                self.fear = False

    def _get_target_point(self):
        if not self.fear:
            return None
        enemy = None
        min_dist_to_enemy = 9999
        for unit in engine.world.units.values():
            if unit.alive and unit.brain and unit.brain.fraction in self.fraction.enemies\
                    and unit.pos.dist_to(self.master.pos) < min_dist_to_enemy:
                min_dist_to_enemy = unit.pos.dist_to(self.master.pos)
                enemy = unit

        return engine.world.get_tile(enemy.pos.x, enemy.pos.y, False).free_pos_around()

    def plan(self):
        self._upd()
        if self.master.current_action:
            return

        if random.random() < 0.02:
            engine.log.add_line(f"{self.master.displayed_name.capitalize()}: *barking*", tcod.lighter_azure)

        enemy = None
        neighbours = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]
        random.shuffle(neighbours)
        for dx, dy in neighbours:
            tile = engine.world.get_tile(self.master.pos.x + dx, self.master.pos.y + dy, False, True)
            if tile:
                for unit in tile.units:
                    if unit.brain and unit.brain.fraction in self.fraction.enemies:
                        enemy = unit
                        break
            if enemy:
                break

        if enemy and len(self.master.body.parts_can_attack()):
            self.master.start_smash((enemy.pos.x, enemy.pos.y), random.choice(self.master.body.parts_can_attack()))
            return

        if len(self.path_to_move) == 0 and 'path_not_found' not in self.data:
            target_point = self._get_target_point()
            if target_point is not None:
                try:
                    path = astar.run((self.master.pos.x, self.master.pos.y), target_point)
                    self.path_to_move = path
                except PathNotFound:
                    self.data['path_not_found'] = engine.world.current_tick

        from engine.unit_actions import Move
        if len(self.path_to_move):
            dx, dy = self.path_to_move_next
            tile = engine.world.get_tile(self.master.pos.x + dx, self.master.pos.y + dy, False)
            if tile.is_walkable:
                self.master.current_action = Move(self.master, (dx, dy))
                return
            else:
                self.path_to_move = []

        self.master.current_action = Move(self.master, (random.randint(-1, 1), random.randint(-1, 1)))

