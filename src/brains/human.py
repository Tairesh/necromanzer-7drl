import random
from math import hypot

import tcod

import engine
from engine import consts, astar
from engine.astar import PathNotFound
from engine.brain import Brain, Fraction, BrainProto
from engine.chunk import CHUNK_SIZE


def _preferred_key_to_attack(unit):
    prior_right = ('right arm', 'left arm', 'right leg', 'left leg')
    prior_left = ('left arm', 'right arm', 'left leg', 'right leg')
    if unit.raw_params['preferred_hand'] == 0:
        prior = prior_right
    elif unit.raw_params['preferred_hand'] == 1:
        prior = prior_left
    elif unit.raw_params['preferred_hand'] == 2:
        if random.random() > 0.5:
            prior = prior_left
        else:
            prior = prior_right
    else:
        prior = ()

    keys_to_attack = set(unit.body.parts_can_attack())
    for pr in prior:
        if pr in keys_to_attack and pr in unit.wield and unit.wield[pr]:
            return pr

    for pr in prior:
        if pr in keys_to_attack:
            return pr
    return keys_to_attack.pop()


class HumanBrain(Brain):

    fraction = Fraction.VILLAGERS
    proto = BrainProto.VILLAGER
    speed_factor = 2.0

    @property
    def fear(self) -> bool:
        if self.data and 'fear' in self.data:
            return self.data['fear']
        else:
            return False

    @fear.setter
    def fear(self, value: bool):
        if not self.data:
            self.data = {}
        self.data['fear'] = value

    def _get_target_point(self):
        if self.fear:
            closest_enemy = None
            for unit in engine.world.units.values():
                if unit.alive and unit.brain and unit.brain.fraction in self.fraction.enemies:
                    d = unit.pos.dist_to(self.master.pos)
                    if not closest_enemy or d < closest_enemy.pos.dist_to(self.master.pos):
                        closest_enemy = unit

            if closest_enemy and closest_enemy.pos.dist_to(self.master.pos) < 10.1:
                dx, dy = closest_enemy.pos.x - self.master.pos.x, closest_enemy.pos.y - self.master.pos.y
                target_x, target_y = self.master.pos.x - dx, self.master.pos.y - dy
                tile = engine.world.get_tile(target_x, target_y, False)
                return tile.free_pos_around()
            weapons = [weapon for weapon in self.master.wield.values() if weapon and weapon.attack_damage > 1]
            if len(weapons) == 0:
                for i in range(10):
                    for j in range(10):
                        for dx in (-i, 0, i):
                            for dy in (-j, 0, j):
                                if dx == 0 and dy == 0:
                                    continue
                                tile = engine.world.get_tile(self.master.pos.x + dx, self.master.pos.y + dy, False, True)
                                if tile:
                                    for item in tile.items:
                                        if item.is_grabable and item.hands_to_grab == 1 and item.attack_damage > 0:
                                            return tile.free_pos_around()

        chunk = self.master.pos.x // CHUNK_SIZE, self.master.pos.y // CHUNK_SIZE
        if random.random() > 0.5:
            houses_around = [(cx, cy) for cx, cy in engine.world.get_houses_chunks()
                             if (cx, cy) != chunk and hypot(chunk[0]-cx, chunk[1] - cy) < 5]
            if len(houses_around) == 0:
                target_chunk = chunk
            else:
                target_chunk = random.choice(houses_around)
        else:
            target_chunk = chunk

        target_x, target_y = target_chunk[0] * CHUNK_SIZE + 2, target_chunk[1] * CHUNK_SIZE + 2
        tile = engine.world.get_tile(target_x, target_y, False, True)
        if tile is None:
            return None
        while not tile.is_walkable:
            target_x = target_chunk[0] * CHUNK_SIZE + random.randint(0, CHUNK_SIZE)
            target_y = target_chunk[1] * CHUNK_SIZE + random.randint(0, CHUNK_SIZE)
            tile = engine.world.get_tile(target_x, target_y, False, True)
            if tile is None:
                return None

        return target_x, target_y

    def _upd(self):
        super()._upd()
        if not self.fear:
            for unit in engine.world.units.values():
                if unit.alive and unit.brain and unit.brain.fraction in self.fraction.enemies\
                        and unit.pos.dist_to(self.master.pos) < 20:
                    self.fear = True
                    self.path_to_move = []
                    fear_str = random.choice(consts.GAME_DATA['fears'])
                    engine.log.add_line(self.master.displayed_name + ': ' + fear_str, tcod.lighter_azure)
                    break
        elif self.fear:
            min_dist_to_enemy = 9999
            for unit in engine.world.units.values():
                if unit.alive and unit.brain and unit.brain.fraction in self.fraction.enemies\
                        and unit.pos.dist_to(self.master.pos) < min_dist_to_enemy:
                    min_dist_to_enemy = unit.pos.dist_to(self.master.pos)
            if min_dist_to_enemy > 50:
                self.fear = False

    def plan(self):
        self._upd()
        if self.master.current_action:
            return

        from engine.unit_actions import Move

        if len(self.path_to_move) == 0 and 'path_not_found' not in self.data:
            target_point = self._get_target_point()
            if target_point is not None:
                try:
                    path = astar.run((self.master.pos.x, self.master.pos.y), target_point)
                    self.path_to_move = path
                except PathNotFound:
                    self.data['path_not_found'] = engine.world.current_tick

        enemy = None
        neighbours = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]
        weapon = None
        part_to_wield = _preferred_key_to_attack(self.master)
        if part_to_wield in {'left arm', 'right arm'}:
            weapon = self.master.wield[part_to_wield] if part_to_wield in self.master.wield else None
        random.shuffle(neighbours)
        for dx, dy in neighbours:
            tile = engine.world.get_tile(self.master.pos.x + dx, self.master.pos.y + dy, False, True)
            if tile:
                for unit in tile.units:
                    if unit.brain and unit.brain.fraction in self.fraction.enemies:
                        enemy = unit
                if self.fear:
                    for item in [item for item in tile.items if item.is_grabable and item.hands_to_grab == 1]:
                        if item.attack_damage > 0 and (not weapon or item.attack_damage > weapon.attack_damage):
                            if part_to_wield in self.master.wield and self.master.wield[part_to_wield]:
                                tile.items.append(self.master.wield[part_to_wield])
                                engine.log.add_line(f"{self.master.name} drops {self.master.wield[part_to_wield].displayed_name} to {tile.base.title}", tcod.white)
                            self.master.wield[part_to_wield] = item
                            tile.items.remove(item)
                            engine.log.add_line(f"{self.master.name} wields {item.displayed_name} in {part_to_wield}", tcod.white)

        if enemy and len(self.master.body.parts_can_attack()):
            self.master.start_smash((enemy.pos.x, enemy.pos.y), _preferred_key_to_attack(self.master))
            return

        if self.data['last_pos']:
            tile = engine.world.get_tile(self.data['last_pos'], change=False)
            items = list(filter(lambda i: 'close' in i.actions, tile.items))
            if len(items):
                self.master.start_activate(items[0], 'close')
                return

        if len(self.path_to_move):
            dx, dy = self.path_to_move_next
            tile = engine.world.get_tile(self.master.pos.x + dx, self.master.pos.y + dy, False)
            if tile.is_walkable:
                self.master.current_action = Move(self.master, (dx, dy))
            else:
                items = list(filter(lambda i: 'open' in i.actions, tile.items))
                if len(items):
                    self.path_to_move = [(dx, dy), ] + self.path_to_move
                    self.master.start_activate(items[0], 'open')
                else:
                    self.path_to_move = []
            return

        if random.random() < 0.1:
            self.master.current_action = Move(self.master, (random.randint(-1, 1), random.randint(-1, 1)))
