from brains.dog import DogBrain
from engine.brain import BrainProto, Fraction


class SmallDaemonBrain(DogBrain):

    proto = BrainProto.SMALL_DAEMON
    fraction = Fraction.DAEMONS
    need_head = False
    speed_factor = 3.0
