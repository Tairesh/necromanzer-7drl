import unittest

import tcod

from engine import consts, savefile
from engine.chunk import CHUNK_SIZE, Chunk
from engine.item import Item
from engine.tile import MapPos
from engine.unit import Unit, Player
from items.tools import Shovel
from tools.graveyard import generate_gravestone
from tools.humans import random_villager
from worldgen.common import Structure
from worldgen import structures_by_seed, generate_chunk


class TestItemSerialize(unittest.TestCase):

    def test_shovel(self):
        shovel = Shovel()
        shovel.hp = 4
        data = shovel.serialize()
        new_shovel = Item.deserialize(data)
        self.assertEqual(shovel.hp, new_shovel.hp)

    def test_gravestone(self):
        stone = generate_gravestone()
        stone.hp = 5
        data = stone.serialize()
        new_stone = Item.deserialize(data)
        self.assertEqual(new_stone.hp, stone.hp)
        self.assertEqual(new_stone.raw_data['person'], stone.raw_data['person'])


class TestUnitSerialize(unittest.TestCase):

    def test_human(self):
        villager = random_villager((1, 1))
        villager.body.parts['head'].hp = 1
        villager.body.parts['left leg'] = None
        villager.wield['left hand'] = Shovel()
        stone = generate_gravestone()
        villager.inventory.append(stone)
        villager.brain.data = {'test': 42}

        data = villager.serialize()
        new_villager = Unit.deserialize(data)

        self.assertEqual(new_villager.name, villager.name)
        self.assertEqual(new_villager.displayed_name, villager.displayed_name)
        self.assertEqual(new_villager.body.parts['head'].hp, villager.body.parts['head'].hp)
        self.assertIsNone(new_villager.body.parts['left leg'])
        self.assertTrue(new_villager.wield['left hand'].__class__ is Shovel)
        self.assertEqual(len(villager.inventory), 1)
        self.assertEqual(villager.inventory[0].raw_data['person'], stone.raw_data['person'])
        self.assertEqual(42, villager.brain.data['test'])

    def test_chunk(self):
        seed = '42'
        structures = structures_by_seed(seed)
        chunk = generate_chunk(1, 1, seed, structures)
        data = chunk.serialize()
        chunk2 = Chunk.deserialize(data)
        self.assertEqual(CHUNK_SIZE*CHUNK_SIZE, len(chunk.tiles))
        for tile2, tile1 in zip(chunk2.tiles, chunk.tiles):
            self.assertEqual(tile2.base, tile1.base)
            self.assertEqual(len(tile2.items), len(tile1.items))
            for item1, item2 in zip(tile1.items, tile2.items):
                self.assertEqual(item2.data, item1.data)

    def test_village_chunk(self):
        seed = '42'
        structures = structures_by_seed(seed)
        chunk_x, chunk_y = structures[Structure.HOUSE][0]
        self.assertEqual(-1, chunk_x)
        self.assertEqual(-8, chunk_y)
        chunk = generate_chunk(chunk_x, chunk_y, seed, structures)
        data = chunk.serialize()
        chunk2 = Chunk.deserialize(data)
        self.assertEqual(CHUNK_SIZE*CHUNK_SIZE, len(chunk.tiles))
        for tile2, tile1 in zip(chunk2.tiles, chunk.tiles):
            self.assertEqual(tile2.base, tile1.base)
            self.assertEqual(len(tile2.items), len(tile1.items))
            for item1, item2 in zip(tile1.items, tile2.items):
                self.assertEqual(item2.data, item1.data)

    def test_save_load(self):
        new_game_params = {
            'name': "Ashley",
            'gender': 1,
            'preferred_hand': 0,
            'skin_tone': 2,
            'age': 20,
            'seed': "42",
        }
        world = savefile.create(new_game_params)
        self.assertEqual(Player, world.player.__class__)

        world.get_tile(world.player.pos.x, world.player.pos.y, False).units.remove(world.player)
        world.player.pos = MapPos(10, 10)
        world.get_tile(world.player.pos.x, world.player.pos.y, False).units.append(world.player)

        world.player.body.parts['head'].hp = 1
        world.player.body.parts['right leg'] = None
        world.units[3].body.parts['head'].hp = 2
        shovel = Shovel()
        shovel.hp = 2
        world.player.inventory.append(shovel)
        world.player.wield['left arm'] = Shovel()
        shovel2 = Shovel()
        shovel2.hp = 1
        world.get_tile(10, 10).items.append(shovel2)
        world.log.append(("Test", tcod.purple))
        world.log.append(("Asd", (1, 2, 3)))

        savefile.save(world)
        world2 = savefile.load(world.file_name)

        self.assertEqual(world2.player.name, world.player.name)
        self.assertEqual(world2.player.inventory[0].hp, world.player.inventory[0].hp)
        self.assertEqual(Shovel, world2.player.wield['left arm'].__class__)
        self.assertIsNone(world2.player.body.parts['right leg'])
        self.assertEqual(world2.player.body.parts['head'].hp, world.player.body.parts['head'].hp)
        self.assertEqual(world2.units[3].body.parts['head'].hp, world.units[3].body.parts['head'].hp)
        self.assertEqual(10, world.player.pos.x)
        self.assertEqual(10, world2.player.pos.x)
        self.assertEqual(world2.get_tile(10, 10).items[0].hp, world.get_tile(10, 10).items[0].hp)
        self.assertEqual(world2.log, world.log)
        for unit_id in world.units:
            unit1 = world.units[unit_id]
            unit2 = world2.units[unit_id]
            self.assertEqual(unit1.name, unit2.name)
            self.assertEqual(unit1.critical_bodyparts, unit2.critical_bodyparts)
            self.assertEqual(unit1.pos, unit2.pos)
            tile1 = world.get_tile(unit1.pos)
            tile2 = world2.get_tile(unit2.pos)
            self.assertEqual(tile1.units[0], unit1)
            self.assertEqual(tile2.units[0], unit2)


if __name__ == '__main__':

    consts.load_data()
    unittest.main()
