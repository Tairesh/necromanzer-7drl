import tcod
from engine.item import Item, ItemProto


class BodyPart(Item):
    can_grab = False
    can_attack = False
    can_move = False
    can_observe = False
    color = tcod.lighter_red
    replacements = ()


class HumanHead(BodyPart):
    proto = ItemProto.HUMAN_HEAD
    can_observe = True
    can_attack = True
    attack_verb = 'biting'
    attack_verb_title = 'teeth'
    attack_damage = 2
    symbol = 'o'
    title = 'human head'
    mass = 5.0
    max_hp = 10


class HumanTorso(BodyPart):
    proto = ItemProto.HUMAN_TORSO
    symbol = '0'
    title = 'human torso'
    mass = 35.0
    max_hp = 50


class HumanArm(BodyPart):
    proto = ItemProto.HUMAN_HAND
    symbol = '\\'
    title = 'human arm'
    mass = 4.0
    max_hp = 10
    can_grab = True
    can_attack = True
    attack_verb = 'punching'
    attack_damage = 1


class HumanLeg(BodyPart):
    proto = ItemProto.HUMAN_LEG
    symbol = '/'
    title = 'human leg'
    mass = 13.0
    max_hp = 20
    can_attack = True
    attack_verb = 'kicking'
    attack_damage = 2
    can_move = True


class RottenBodyPart(BodyPart):
    color = tcod.dark_green


class RottenHumanHead(HumanHead, RottenBodyPart):
    proto = ItemProto.HUMAN_HEAD_ROTTEN
    title = 'rotten human head'
    max_hp = 7
    replacements = ItemProto.HUMAN_HEAD, ItemProto.HUMAN_HEAD_SKELETAL


class RottenHumanTorso(HumanTorso, RottenBodyPart):
    proto = ItemProto.HUMAN_TORSO_ROTTEN
    title = 'rotten human torso'
    max_hp = 40
    replacements = ItemProto.HUMAN_TORSO, ItemProto.HUMAN_TORSO_SKELETAL


class RottenHumanArm(HumanArm, RottenBodyPart):
    proto = ItemProto.HUMAN_HAND_ROTTEN
    title = 'rotten human arm'
    max_hp = 7
    replacements = ItemProto.HUMAN_HAND, ItemProto.HUMAN_HAND_SKELETAL


class RottenHumanLeg(HumanLeg, RottenBodyPart):
    proto = ItemProto.HUMAN_LEG_ROTTEN
    title = 'rotten human leg'
    max_hp = 15
    replacements = ItemProto.HUMAN_LEG, ItemProto.HUMAN_LEG_SKELETAL


class SkeletonBodyPart(BodyPart):
    color = tcod.lighter_gray


class SkeletonHumanHead(HumanHead, SkeletonBodyPart):
    proto = ItemProto.HUMAN_HEAD_SKELETAL
    title = 'human skull'
    max_hp = 3
    replacements = ItemProto.HUMAN_HEAD, ItemProto.HUMAN_HEAD_ROTTEN


class SkeletonHumanTorso(HumanTorso, SkeletonBodyPart):
    proto = ItemProto.HUMAN_TORSO_SKELETAL
    title = 'human rib cage'
    max_hp = 25
    replacements = ItemProto.HUMAN_TORSO, ItemProto.HUMAN_TORSO_ROTTEN


class SkeletonHumanArm(HumanArm, SkeletonBodyPart):
    proto = ItemProto.HUMAN_HAND_SKELETAL
    title = 'human skeletal arm'
    max_hp = 5
    replacements = ItemProto.HUMAN_HAND, ItemProto.HUMAN_HAND_ROTTEN


class SkeletonHumanLeg(HumanLeg, SkeletonBodyPart):
    proto = ItemProto.HUMAN_LEG_SKELETAL
    title = 'human skeletal leg'
    max_hp = 10
    replacements = ItemProto.HUMAN_LEG, ItemProto.HUMAN_LEG_ROTTEN


class ChildHead(HumanHead):
    proto = ItemProto.CHILD_HEAD
    title = 'child head'
    mass = 2.0
    max_hp = 5


class RottenChildHead(ChildHead, RottenBodyPart):
    proto = ItemProto.CHILD_HEAD_ROTTEN
    title = 'rotten child head'
    max_hp = 3


class SkeletalChildHead(ChildHead, SkeletonBodyPart):
    proto = ItemProto.CHILD_HEAD_SKELETAL
    title = 'child skull'
    max_hp = 2


class ChildTorso(HumanTorso):
    proto = ItemProto.CHILD_TORSO
    title = 'child torso'
    mass = 15.0
    max_hp = 30


class RottenChildTorso(ChildTorso, RottenBodyPart):
    proto = ItemProto.CHILD_TORSO_ROTTEN
    title = 'rotten child torso'
    max_hp = 20


class SkeletalChildTorso(ChildTorso, SkeletonBodyPart):
    proto = ItemProto.CHILD_TORSO_SKELETAL
    title = 'child rib cage'
    max_hp = 10


class ChildLeg(HumanLeg):
    proto = ItemProto.CHILD_LEG
    title = 'child leg'
    mass = 6.0
    max_hp = 10
    attack_damage = 1


class RottenChildLeg(ChildLeg, RottenBodyPart):
    proto = ItemProto.CHILD_LEG_ROTTEN
    title = 'rotten child leg'
    max_hp = 7


class SkeletalChildLeg(ChildLeg, SkeletonBodyPart):
    proto = ItemProto.CHILD_LEG_SKELETAL
    title = 'child skeletal leg'
    max_hp = 5


class ChildArm(HumanArm):
    proto = ItemProto.CHILD_HAND
    title = 'child arm'
    mass = 2.0
    max_hp = 5
    attack_damage = 0


class RottenChildArm(ChildArm, RottenBodyPart):
    proto = ItemProto.CHILD_HAND_ROTTEN
    title = 'rotten child arm'
    max_hp = 4


class SkeletalChildArm(ChildArm, SkeletonBodyPart):
    proto = ItemProto.CHILD_HAND_SKELETAL
    title = 'child skeletal arm'
    max_hp = 2


class DogHead(BodyPart):
    proto = ItemProto.DOG_HEAD
    can_observe = True
    can_attack = True
    attack_verb = 'biting'
    attack_verb_title = 'maw'
    attack_damage = 5
    symbol = 'o'
    title = 'dog head'
    mass = 4.0
    max_hp = 7


class RottenDogHead(DogHead, RottenBodyPart):
    proto = ItemProto.DOG_HEAD_ROTTEN
    title = 'rotten dog head'
    max_hp = 5


class SkeletalDogHead(DogHead, SkeletonBodyPart):
    proto = ItemProto.DOG_HEAD_SKELETAL
    title = 'skeletal dog head'
    max_hp = 3


class DogTorso(BodyPart):
    proto = ItemProto.DOG_TORSO
    symbol = '0'
    title = 'dog carcass'
    mass = 25.0
    max_hp = 35


class RottenDogTorso(DogTorso, RottenBodyPart):
    proto = ItemProto.DOG_TORSO_ROTTEN
    title = 'rotten dog carcass'
    max_hp = 30


class SkeletalDogTorso(DogTorso, SkeletonBodyPart):
    proto = ItemProto.DOG_TORSO_SKELETAL
    title = 'skeletal dog carcass'
    max_hp = 20


class DogLeg(BodyPart):
    proto = ItemProto.DOG_LEG
    symbol = '/'
    title = 'dog paw'
    mass = 13.0
    max_hp = 20
    can_attack = False
    can_move = True


class RottenDogLeg(DogLeg, RottenBodyPart):
    proto = ItemProto.DOG_LEG_ROTTEN
    title = 'rotten dog paw'
    max_hp = 15


class SkeletalDogLeg(DogLeg, SkeletonBodyPart):
    proto = ItemProto.DOG_LEG_SKELETAL
    title = 'skeletal dog paw'
    max_hp = 10


class DogTail(BodyPart):
    proto = ItemProto.DOG_TAIL
    symbol = '~'
    title = 'dog tail'
    mass = 5.0
    max_hp = 5


class RottenDogTail(DogTail, RottenBodyPart):
    proto = ItemProto.DOG_TAIL_ROTTEN
    title = 'rotten dog tail'
    max_hp = 4


class SkeletalDogTail(DogTail, SkeletonBodyPart):
    proto = ItemProto.DOG_TAIL_SKELETAL
    title = 'skeletal dog tail'
    max_hp = 2
