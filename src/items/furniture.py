import tcod

from engine.item import Item, ItemProto


class Furniture(Item):
    count_in_blue_screen = False
    z_index = 1
    is_grabable = False


class Door(Furniture):

    proto = ItemProto.DOOR
    color = tcod.gray
    mass = 40.0
    max_hp = 20
    is_grabable = False
    attack_damage = 4
    attack_length = 20
    z_index = 0

    @property
    def is_walkable(self):
        return self.opened

    @property
    def symbol(self):
        return '_' if self.opened else 197

    @property
    def bg_color(self):
        return None if self.opened else (75, 55, 20)

    @property
    def title(self):
        return 'wooden door' + (' (open)' if self.opened else '')

    @property
    def opened(self):
        return self.raw_data and 'opened' in self.raw_data and self.raw_data['opened']

    @opened.setter
    def opened(self, value: bool):
        self.raw_data['opened'] = value

    @property
    def actions(self):
        if self.opened:
            return 'close',
        else:
            return 'open',

    def act(self, action: str, consumer):
        if action == 'open':
            self.opened = True
        elif action == 'close':
            self.opened = False

    def action_length(self, action: str, consumer) -> int:
        if action == 'open':
            return 1
        elif action == 'close':
            return 1


class Table(Furniture):
    proto = ItemProto.TABLE
    color = (75, 55, 20)
    mass = 30.0
    max_hp = 20
    attack_damage = 4
    attack_length = 10
    title = 'wooden table'
    is_walkable = False
    symbol = 209


class Stool(Furniture):
    proto = ItemProto.STOOL
    color = (75, 55, 20)
    mass = 5.0
    max_hp = 3
    attack_damage = 1
    attack_length = 3
    title = 'wooden stool'
    is_walkable = True
    symbol = 210


class Bench(Furniture):
    proto = ItemProto.BENCH
    color = (75, 55, 20)
    mass = 20.0
    max_hp = 10
    attack_damage = 3
    attack_length = 20
    title = 'wooden bench'
    is_walkable = False
    symbol = '#'


class Chest(Furniture):
    proto = ItemProto.CHEST
    color = (75, 55, 20)
    mass = 30.0
    max_hp = 20
    attack_damage = 5
    attack_length = 30
    title = 'chest'
    is_walkable = False
    symbol = 145
    z_index = 10


class Bed(Furniture):
    proto = ItemProto.BED
    color = (75, 55, 20)
    mass = 40.0
    max_hp = 30
    attack_damage = 6
    attack_length = 40
    title = 'bed'
    is_walkable = False
    symbol = 233

