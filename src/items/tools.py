import tcod
from engine.item import Item, ItemFlag, ItemProto


class Shovel(Item):
    proto = ItemProto.SHOVEL
    symbol = 140
    color = (200, 230, 255)
    title = 'shovel'
    mass = 3.0
    max_hp = 10
    attack_damage = 4
    attack_length = 1
    flags = {ItemFlag.CAN_DIG, }


class Knife(Item):
    proto = ItemProto.KNIFE
    symbol = '/'
    color = (200, 230, 255)
    title = 'knife'
    mass = 0.5
    max_hp = 1
    attack_damage = 3
    attack_length = 1
    attack_verb = 'cutting'
    flags = {ItemFlag.CAN_BUTCH, }


class Axe(Item):
    proto = ItemProto.AXE
    symbol = 169
    color = (200, 230, 255)
    title = 'axe'
    mass = 2.0
    max_hp = 20
    attack_damage = 10
    attack_length = 2
    attack_verb = 'hacking'
    flags = {ItemFlag.CAN_HACK, }


class PitchFork(Item):
    proto = ItemProto.PITCHFORK
    symbol = 139
    color = (200, 230, 255)
    title = 'pitchfork'
    mass = 5.0
    max_hp = 40
    attack_damage = 5
    attack_length = 3
    attack_verb = 'spearing'
