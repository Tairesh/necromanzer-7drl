import tcod
import engine
from engine.item import Item, ItemFlag, ItemProto


class Boulder(Item):
    proto = ItemProto.BOULDER
    symbol = 236
    color = tcod.gray
    title = 'boulder'
    mass = 500.0
    is_walkable = False
    max_hp = 500
    attack_damage = 10
    attack_length = 30
    z_index = 1


class GraveStone(Item):

    proto = ItemProto.GRAVESTONE
    color = tcod.light_gray
    title = 'gravestone'
    mass = 100.0
    max_hp = 100
    actions = ('read', )
    is_grabable = False
    attack_damage = 5
    attack_length = 15
    z_index = 1

    @property
    def symbol(self):
        if self.is_grabable:
            return '_'
        else:
            return 234

    def act(self, action: str, consumer):
        if action == 'read':
            engine.log.add_line(f"You read on gravestone: {self.data['text']}", tcod.chartreuse)

    def action_length(self, action: str, consumer) -> int:
        if action == 'read':
            return 0


class Pot(Item):
    proto = ItemProto.POT
    symbol = 'u'
    color = tcod.dark_orange
    title = 'copper pot'
    mass = 2.0
    max_hp = 5
    attack_damage = 1
    attack_length = 1


class Barrel(Item):

    proto = ItemProto.BARREL
    is_walkable = False
    symbol = 240
    color = (200, 230, 255)
    bg_color = (75, 55, 20)
    title = 'wooden barrel'
    mass = 10.0
    max_hp = 5
    attack_damage = 1
    attack_length = 5
    z_index = 2


class Jug(Item):
    proto = ItemProto.JUG
    symbol = 20
    color = tcod.light_sepia
    title = 'clay jug'
    mass = 3.0
    max_hp = 1
    attack_length = 1


class Bucket(Item):
    proto = ItemProto.BUCKET
    symbol = 150
    color = (200, 230, 255)
    title = 'bucket'
    mass = 1.0
    max_hp = 4
    attack_damage = 1
    attack_length = 1


class Cloth(Item):
    proto = ItemProto.CLOTH
    symbol = '}'
    color = tcod.light_gray
    title = 'cloth'
    mass = 0.1
    max_hp = 2


class Thread(Item):
    proto = ItemProto.THREAD
    symbol = 21
    color = tcod.light_gray
    title = 'thread'
    mass = 0.1
    max_hp = 2


class Coins(Item):
    proto = ItemProto.HANDFUL_OF_COINS
    symbol = 7
    color = tcod.yellow
    title = 'a few coins'
    mass = 0.1
    max_hp = 10
