import tcod
import engine
from engine.drawable.labels import Label


class Log:

    def __init__(self):
        self.lines = []

    def add_line(self, text, color=tcod.white, important=True):
        line = Label(None, text, color)
        self.lines.append(line)
        if important:
            engine.world.log.append((text, color))

    def get_last(self, n=5):
        return reversed(self.lines[-n:])
