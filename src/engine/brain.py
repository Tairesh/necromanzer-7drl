import json
import random
from enum import Enum, auto, IntEnum
from math import hypot

import tcod

import engine
from engine import astar, consts
from engine.astar import PathNotFound
from engine.chunk import CHUNK_SIZE
from engine.orders import OrderProto


class Fraction(Enum):
    NECROMANT = auto()
    ZOMBIE = auto()
    VILLAGERS = auto()
    DAEMONS = auto()

    @property
    def enemies(self) -> set:
        if self == self.NECROMANT:
            return {self.VILLAGERS, self.DAEMONS}
        elif self == self.ZOMBIE:
            return {self.VILLAGERS, self.DAEMONS}
        elif self == self.VILLAGERS:
            return {self.NECROMANT, self.ZOMBIE, self.DAEMONS}
        elif self == self.DAEMONS:
            return {self.NECROMANT, self.ZOMBIE, self.VILLAGERS}


class BrainProto(IntEnum):
    ZOMBIE = auto()
    VILLAGER = auto()
    DOG = auto()
    SMALL_DAEMON = auto()


class Brain:

    proto: BrainProto
    fraction: Fraction
    need_head: bool = True
    speed_factor: float = 1

    def __init__(self, master, data=None):
        from engine.unit import Unit
        self.master: Unit = master
        self.data = data

    @property
    def path_to_move(self) -> list:
        if self.data and 'path_to_move' in self.data:
            return self.data['path_to_move']
        else:
            return []

    @path_to_move.setter
    def path_to_move(self, value):
        if not self.data:
            self.data = {}
        self.data['path_to_move'] = list(value)

    @property
    def path_to_move_next(self):
        if len(self.path_to_move):
            return self.path_to_move.pop(0)
        return None

    def _upd(self):
        if not self.data:
            self.data = {'current_pos': None, 'last_pos': None}

        self.data['last_pos'] = self.data['current_pos']
        self.data['current_pos'] = self.master.pos.x, self.master.pos.y
        if 'path_not_found' in self.data and self.data['path_not_found'] == engine.world.current_tick-10:
            self.data.pop('path_not_found')

    def plan(self):
        self._upd()

    def serialize(self) -> str:
        data = self.data if self.data else {}
        return str(int(self.proto)) + ';' + json.dumps(data, separators=(',', ':'))

    @staticmethod
    def deserialize(value):
        if value is None:
            return None
        proto, data = value.split(';')
        data = json.loads(data)
        proto = BrainProto(int(proto))
        return proto, data


class PlayerBrain(Brain):
    fraction = Fraction.NECROMANT
