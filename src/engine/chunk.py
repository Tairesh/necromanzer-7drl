from enum import Enum

from engine.tile import Tile

CHUNK_SIZE = 16


class ChunkProto(Enum):
    BADLANDS = ('badlands', )
    GRAVEYARD = ('village graveyard', )

    def __init__(self, title):
        self.title = title


class Chunk:

    def __init__(self, chunk_coords, tiles):
        self.chunk_coords = chunk_coords
        self.tiles = tiles
        self.has_changed = False

    def serialize(self) -> str:
        tiles_data = [tile.serialize() for tile in self.tiles]
        return f"{self.chunk_coords[0]}x{self.chunk_coords[1]}" + '?' + '|'.join(tiles_data)

    @staticmethod
    def deserialize(value):
        value = value.split('?')
        chunk_x, chunk_y = map(int, value[0].split('x'))
        tiles = []
        chunk_left, chunk_top = chunk_x*CHUNK_SIZE, chunk_y*CHUNK_SIZE
        for i, tile_data in enumerate(value[1].split('|')):
            x = chunk_left + i // CHUNK_SIZE
            y = chunk_top + i % CHUNK_SIZE
            tiles.append(Tile.deserialize((x, y), tile_data))
        return Chunk((chunk_x, chunk_y), tiles)
