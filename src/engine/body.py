import json
from enum import IntEnum

import tcod

from engine import consts
from engine.item import Item
from items.bodyparts import BodyPart, HumanHead, HumanTorso, HumanArm, HumanLeg, RottenHumanArm, RottenHumanHead, RottenHumanTorso, RottenHumanLeg, SkeletonHumanArm, SkeletonHumanHead, SkeletonHumanTorso, SkeletonHumanLeg, ChildHead, ChildLeg, ChildArm, ChildTorso, RottenChildHead, RottenChildTorso, RottenChildLeg, SkeletalChildHead, SkeletalChildLeg, SkeletalChildTorso, DogHead, DogTorso, DogLeg, DogTail, RottenDogHead, RottenDogTorso, RottenDogLeg, RottenDogTail, SkeletalDogHead, SkeletalDogTail, SkeletalDogLeg, SkeletalDogTorso, RottenChildArm, SkeletalChildArm
from typing import Dict, Tuple


def generate_new(klass):
    parts = {}
    positions = {}
    for name, bp_class, pos in klass.schema:
        parts[name] = bp_class(None)
        positions[name] = pos
    return klass(parts, positions)


class BodyProto(IntEnum):
    HUMAN_BODY = 1
    ROTTEN_HUMAN_BODY = 2
    SKELETON_HUMAN_BODY = 3
    CHILD_BODY = 4
    ROTTEN_CHILD_BODY = 5
    SKELETON_CHILD_BODY = 6
    DOG_BODY = 7
    ROTTEN_DOG_BODY = 8
    SKELETON_DOG_BODY = 9


class Body:

    symbol = 2
    permacolor = None
    schema = ()
    root = 'torso'
    proto: BodyProto

    def __init__(self, parts: Dict[str, BodyPart], positions: Dict[str, Tuple[int, int]]):
        self.parts: Dict[str, BodyPart] = parts
        self.positions: Dict[str, Tuple[int, int]] = positions

    def parts_can_grab(self):  # actually returns keys
        return [key for key, part in self.parts.items() if part and part.can_grab]

    def parts_can_attack(self):  # actually returns keys
        return [key for key, part in self.parts.items() if part and part.can_attack]

    def parts_can_move(self):  # actually returns keys
        return [key for key, part in self.parts.items() if part and part.can_move]

    def base_part_class(self, part_key):
        for key, klass, _ in self.schema:
            if key == part_key:
                return klass

    def serialize(self) -> str:
        data = {
            'proto': int(self.proto),
            'positions': self.positions,
            'parts': {},
        }
        for key, part in self.parts.items():
            data['parts'][key] = part.serialize() if part else None
        return json.dumps(data, separators=(',', ':'))

    @staticmethod
    def deserialize(value):
        value = json.loads(value)
        parts = {}
        for key, part in value['parts'].items():
            parts[key] = Item.deserialize(part)
        positions = value['positions']

        body_class = consts.GAME_DATA['bodies'][BodyProto(value['proto'])]
        return body_class(parts, positions)

    def name_for_new_part(self, part):
        last = part.title.split(' ').pop()
        count = 0
        for c_part in self.parts.values():
            if c_part is None:
                continue
            c_last = c_part.title.split(' ').pop()
            if last == c_last:
                count += 1
        return consts.NUMERALS[count].capitalize() + ' ' + last


class HumanBody(Body):

    proto = BodyProto.HUMAN_BODY

    schema = (
        ('head', HumanHead, (0, 0)),
        ('torso', HumanTorso, (0, 0)),
        ('left arm', HumanArm, (0, 0)),
        ('right arm', HumanArm, (0, 0)),
        ('left leg', HumanLeg, (0, 0)),
        ('right leg', HumanLeg, (0, 0)),
    )


class RottenHumanBody(Body):

    permacolor = tcod.dark_green
    proto = BodyProto.ROTTEN_HUMAN_BODY

    schema = (
        ('head', RottenHumanHead, (0, 0)),
        ('torso', RottenHumanTorso, (0, 0)),
        ('left arm', RottenHumanArm, (0, 0)),
        ('right arm', RottenHumanArm, (0, 0)),
        ('left leg', RottenHumanLeg, (0, 0)),
        ('right leg', RottenHumanLeg, (0, 0)),
    )


class SkeletonHumanBody(Body):

    permacolor = tcod.light_gray
    proto = BodyProto.SKELETON_HUMAN_BODY

    schema = (
        ('head', SkeletonHumanHead, (0, 0)),
        ('torso', SkeletonHumanTorso, (0, 0)),
        ('left arm', SkeletonHumanArm, (0, 0)),
        ('right arm', SkeletonHumanArm, (0, 0)),
        ('left leg', SkeletonHumanLeg, (0, 0)),
        ('right leg', SkeletonHumanLeg, (0, 0)),
    )


class ChildBody(Body):

    proto = BodyProto.CHILD_BODY

    schema = (
        ('head', ChildHead, (0, 0)),
        ('torso', ChildTorso, (0, 0)),
        ('left arm', ChildArm, (0, 0)),
        ('right arm', ChildArm, (0, 0)),
        ('left leg', ChildLeg, (0, 0)),
        ('right leg', ChildLeg, (0, 0)),
    )


class RottenChildBody(Body):

    permacolor = tcod.dark_green
    proto = BodyProto.ROTTEN_CHILD_BODY

    schema = (
        ('head', RottenChildHead, (0, 0)),
        ('torso', RottenChildTorso, (0, 0)),
        ('left arm', RottenChildArm, (0, 0)),
        ('right arm', RottenChildArm, (0, 0)),
        ('left leg', RottenChildLeg, (0, 0)),
        ('right leg', RottenChildLeg, (0, 0)),
    )


class SkeletalChildBody(Body):

    permacolor = tcod.light_gray
    proto = BodyProto.SKELETON_CHILD_BODY

    schema = (
        ('head', SkeletalChildHead, (0, 0)),
        ('torso', SkeletalChildTorso, (0, 0)),
        ('left arm', SkeletalChildArm, (0, 0)),
        ('right arm', SkeletalChildArm, (0, 0)),
        ('left leg', SkeletalChildLeg, (0, 0)),
        ('right leg', SkeletalChildLeg, (0, 0)),
    )


class DogBody(Body):

    proto = BodyProto.DOG_BODY

    schema = (
        ('head', DogHead, (0, 0)),
        ('torso', DogTorso, (0, 0)),
        ('left fore paw', DogLeg, (0, 0)),
        ('right fore paw', DogLeg, (0, 0)),
        ('left hind paw', DogLeg, (0, 0)),
        ('right hind paw', DogLeg, (0, 0)),
        ('tail', DogTail, (0, 0)),
    )


class RottenDogBody(Body):

    permacolor = tcod.dark_green
    proto = BodyProto.ROTTEN_DOG_BODY

    schema = (
        ('head', RottenDogHead, (0, 0)),
        ('torso', RottenDogTorso, (0, 0)),
        ('left fore paw', RottenDogLeg, (0, 0)),
        ('right fore paw', RottenDogLeg, (0, 0)),
        ('left hind paw', RottenDogLeg, (0, 0)),
        ('right hind paw', RottenDogLeg, (0, 0)),
        ('tail', RottenDogTail, (0, 0)),
    )


class SkeletonDogBody(Body):

    permacolor = tcod.light_gray
    proto = BodyProto.SKELETON_DOG_BODY

    schema = (
        ('head', SkeletalDogHead, (0, 0)),
        ('torso', SkeletalDogTorso, (0, 0)),
        ('left fore paw', SkeletalDogLeg, (0, 0)),
        ('right fore paw', SkeletalDogLeg, (0, 0)),
        ('left hind paw', SkeletalDogLeg, (0, 0)),
        ('right hind paw', SkeletalDogLeg, (0, 0)),
        ('tail', SkeletalDogTail, (0, 0)),
    )
