import itertools
import random
from enum import Enum, auto, IntEnum
from math import hypot
from typing import Tuple, Optional

import tcod

import engine
from engine.item import Item


class MapPos:

    def __init__(self, x, y=None):
        if type(x) is MapPos:
            x, y = x.x, x.y
        elif y is None:
            x, y = x
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def dist_to(self, pos2):
        return hypot(pos2.x-self.x, pos2.y-self.y)

    def copy(self):
        return MapPos(self.x, self.y)

    def __str__(self):
        return f"{self.x}x{self.y}"

    def to_direction(self, from_pos):
        dx, dy = self.x - from_pos.x, self.y - from_pos.y
        if dx == -1 and dy == -1:
            return 'northwest'
        elif dx == -1 and dy == 0:
            return 'west'
        elif dx == -1 and dy == 1:
            return 'southwest'
        elif dx == 0 and dy == -1:
            return 'north'
        elif dx == 0 and dy == 0:
            return 'here'
        elif dx == 0 and dy == 1:
            return 'south'
        elif dx == 1 and dy == -1:
            return 'northeast'
        elif dx == 1 and dy == 0:
            return 'east'
        elif dx == 1 and dy == 1:
            return 'southeast'
        else:
            return 'around'


class TileBase(IntEnum):
    DIRT1 = auto()
    DIRT2 = auto()
    DIRT3 = auto()
    DIRT4 = auto()
    GRASS1 = auto()
    GRASS2 = auto()
    GRASS3 = auto()
    GRASS4 = auto()
    FENCE = auto()
    FENCE_BROKEN = auto()
    GRAVE = auto()
    HOLE = auto()
    GRAVEL1 = auto()
    GRAVEL2 = auto()
    STONE_WALL = auto()
    STONE_WALL_WITH_WINDOW = auto()          # obsolete
    STONE_WALL_WITH_BROKEN_WINDOW = auto()   # obsolete
    WOODEN_WALL = auto()
    WOODEN_WALL_WITH_WINDOW = auto()         # obsolete
    WOODEN_WALL_WITH_BROKEN_WINDOW = auto()  # obsolete
    WOODEN_FLOOR = auto()
    WOODEN_FLOOR_BELOW_DOOR = auto()
    FIREPLACE = auto()
    WELL = auto()
    ARABLE = auto()

    @property
    def symbol(self):
        return TILE_BASE_DATA[self][0]

    @property
    def color(self):
        return TILE_BASE_DATA[self][1]

    @property
    def bg_color(self):
        return None

    @property
    def title(self):
        return TILE_BASE_DATA[self][2]

    @property
    def is_walkable(self):
        return self not in {self.HOLE, self.FENCE, self.GRAVE, self.STONE_WALL, self.WOODEN_WALL,
                            self.FIREPLACE, self.WELL,
                            self.STONE_WALL_WITH_WINDOW, self.WOODEN_WALL_WITH_WINDOW}

    @property
    def can_be_digged(self) -> bool:
        return self in {self.DIRT1, self.DIRT2, self.DIRT3, self.DIRT4,
                        self.GRASS1, self.GRASS2, self.GRASS3, self.GRASS4,
                        self.GRAVE, self.GRAVEL1, self.GRAVEL2, self.ARABLE}

    @property
    def dig_result(self):
        if self.can_be_digged:
            return self.HOLE
        else:
            return self


TILE_BASE_DATA = {
    TileBase.DIRT1: ('.', tcod.sepia, 'dirt'),
    TileBase.DIRT2: (',', tcod.sepia, 'dirt'),
    TileBase.DIRT3: ('\'', tcod.sepia, 'dirt'),
    TileBase.DIRT4: ('`', tcod.sepia, 'dirt'),
    TileBase.GRASS1: ('"', tcod.darkest_green, 'grass'),
    TileBase.GRASS2: (';', tcod.darkest_green, 'grass'),
    TileBase.GRASS3: ('`', tcod.darkest_green, 'grass'),
    TileBase.GRASS4: ('\'', tcod.darkest_green, 'grass'),
    TileBase.FENCE: ('|', tcod.brass, 'wooden fence'),
    TileBase.FENCE_BROKEN: (',', tcod.brass, 'broken wooden fence'),
    TileBase.GRAVE: ('^', tcod.sepia, 'grave'),
    TileBase.HOLE: (31, tcod.dark_gray, 'hole'),
    TileBase.GRAVEL1: ('~', tcod.dark_gray, 'gravel'),
    TileBase.GRAVEL2: (247, tcod.dark_gray, 'gravel'),
    TileBase.STONE_WALL: ('wall', tcod.dark_gray, 'stone wall'),
    TileBase.WOODEN_WALL: ('wall', (75, 55, 20), 'wooden wall'),
    TileBase.WOODEN_FLOOR: ('+', (50, 35, 10), 'wooden floor'),
    TileBase.WOODEN_FLOOR_BELOW_DOOR: ('+', (50, 35, 10), 'wooden floor'),
    TileBase.FIREPLACE: (9, tcod.gray, 'fireplace'),
    TileBase.WELL: (10, tcod.light_gray, 'well'),
    TileBase.ARABLE: ('^', tcod.sepia, 'arable land'),

    # obsolete
    TileBase.STONE_WALL_WITH_WINDOW: ('wall', tcod.gray, 'stone wall with window'),
    TileBase.WOODEN_WALL_WITH_WINDOW: ('wall', (75, 55, 20), 'wooden wall with window'),
    TileBase.STONE_WALL_WITH_BROKEN_WINDOW: (',', tcod.dark_gray, 'stone wall with broken window'),
    TileBase.WOODEN_WALL_WITH_BROKEN_WINDOW: (',', (50, 35, 10), 'wooden wall with broken window'),
}

TILE_BASE_CONNECTABLE = {TileBase.STONE_WALL, TileBase.WOODEN_WALL, TileBase.WOODEN_FLOOR_BELOW_DOOR,
                         TileBase.STONE_WALL_WITH_WINDOW, TileBase.WOODEN_WALL_WITH_WINDOW}


class Tile:

    def __init__(self, pos, base: TileBase):
        self.pos = MapPos(pos) if pos else None
        self.base: TileBase = base
        self.items = []
        self.units = []

    def serialize(self) -> str:
        return str(int(self.base)) + '!' + '='.join([item.serialize() for item in self.items])

    @staticmethod
    def deserialize(pos, value):
        base, items = value.split('!')
        tile = Tile(pos, TileBase(int(base)))
        if items:
            tile.items = [Item.deserialize(item) for item in items.split('=')]
        return tile

    @property
    def top_thing(self):
        if len(self.units):
            return sorted(self.units, key=lambda unit: unit.z_index, reverse=True)[0]
        if len(self.items):
            return sorted(self.items, key=lambda item: item.z_index, reverse=True)[0]
        return self.base

    @property
    def symbol(self):
        symbol = self.top_thing.symbol
        if symbol == 'wall':
            symbol = wall(self)
        return symbol

    @property
    def color(self):
        return self.top_thing.color

    @property
    def bg_color(self):
        if self.top_thing.bg_color:
            return self.top_thing.bg_color

        if sum(1 for _ in itertools.islice(filter(lambda item: item.count_in_blue_screen, self.items), 2)) > 1:
            return tcod.dark_blue

        return tcod.black

    @property
    def is_walkable(self) -> bool:
        for unit in self.units:
            if not unit.is_walkable:
                return False
        for item in self.items:
            if not item.is_walkable:
                return False
        return self.base.is_walkable

    @property
    def can_be_opened(self) -> bool:
        if not self.base.is_walkable:
            return False
        if len(self.units) > 0:
            return False
        for item in self.items:
            if not item.is_walkable and 'open' not in item.actions:
                return False
        return True

    def free_pos_around(self) -> Optional[Tuple[int, int]]:
        if self.is_walkable:
            return self.pos.x, self.pos.y
        neighbours = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]
        random.shuffle(neighbours)
        for dx, dy in neighbours:
            neighbour = engine.world.get_tile(self.pos.x + dx, self.pos.y + dy, False, True)
            if neighbour and neighbour.is_walkable:
                return neighbour.pos.x, neighbour.pos.y
        return None


def wall(tile: Tile):
    left = engine.world.get_tile(tile.pos.x-1, tile.pos.y, False).base in TILE_BASE_CONNECTABLE
    right = engine.world.get_tile(tile.pos.x+1, tile.pos.y, False).base in TILE_BASE_CONNECTABLE
    top = engine.world.get_tile(tile.pos.x, tile.pos.y-1, False).base in TILE_BASE_CONNECTABLE
    bottom = engine.world.get_tile(tile.pos.x, tile.pos.y+1, False).base in TILE_BASE_CONNECTABLE

    if right and left and top and bottom:
        return 206  # '┼'
    elif right and left and not top and not bottom:
        return 205  # '─'
    elif not right and not left and top and bottom:
        return 186  # '│'
    elif not right and not left and not top and not bottom:
        return 9  # '*'
    elif right and not left and not top and bottom:
        return 201  # '┌'
    elif right and not left and top and bottom:
        return 204  # '├'
    elif right and left and not top and bottom:
        return 203  # '┬'
    elif not right and left and not top and not bottom:
        return 181  # '>'
    elif not right and left and top and not bottom:
        return 188  # '┘'
    elif right and left and top and not bottom:
        return 202  # '┴'
    elif not right and not left and not top and bottom:
        return 210  # '^'
    elif not right and left and not top and bottom:
        return 187  # '┐'
    elif not right and left and top and bottom:
        return 185  # '┤'
    elif not right and not left and top and not bottom:
        return 208  # 'v'
    elif right and not left and not top and not bottom:
        return 198  # '<'
    elif right and not left and top and not bottom:
        return 200  # '└'
