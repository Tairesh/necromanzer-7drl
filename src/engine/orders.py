import json
from enum import IntEnum, auto


class OrderProto(IntEnum):
    NONE = 0
    FOLLOW_ME = auto()
    MOVE_TO = auto()
    KILL = auto()


class Order:
    def __init__(self, proto: OrderProto, data=None):
        self.proto = proto
        self.data = data

    def serialize(self) -> str:
        return str(int(self.proto)) + '!' + json.dumps(self.data)

    @staticmethod
    def deserialize(value):
        proto, data = value.split('!')
        return Order(OrderProto(int(proto)), json.loads(data))

    @property
    def label(self) -> str:
        if self.proto == OrderProto.FOLLOW_ME:
            return 'follow me'
        elif self.proto == OrderProto.MOVE_TO:
            return 'move to'
        elif self.proto == OrderProto.KILL:
            return 'kill'
        else:
            return 'no order'
