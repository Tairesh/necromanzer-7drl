import json
import os

filename = 'settings.json'
default_settings = {'width': 120, 'height': 50}


def rewrite_file(settings):
    with open(filename, 'w+') as file:
        file.write(json.dumps(settings))


def validate(settings):
    validated = True

    if 'width' not in settings:
        validated = False
    elif settings['width'] < 80 or settings['width'] > 300:
        validated = False

    if 'height' not in settings:
        validated = False
    elif settings['height'] < 30 or settings['height'] > 100:
        validated = False

    return validated


def correct(settings):
    if 'width' not in settings:
        print('Settings file have not key "width", using default: ' + str(default_settings['width']))
        settings['width'] = default_settings['width']
    elif settings['width'] < 80:
        print('Window width must be between 80 and 300, using minimum: 80')
        settings['width'] = 80
    elif settings['width'] > 300:
        print('Window width must be between 80 and 300, using maximum: 300')
        settings['width'] = 300

    if 'height' not in settings:
        print('Settings file have not key "height", using default: ' + str(default_settings['height']))
        settings['height'] = default_settings['height']
    elif settings['height'] < 30:
        print('Window height must be between 30 and 100, using minimum: 30')
        settings['height'] = 30
    elif settings['height'] > 100:
        print('Window height must be between 30 and 100, using maximum: 100')
        settings['height'] = 100

    return settings


def load():
    """
     Returns dict with game settings
    """
    if os.path.exists(filename):
        with open(filename) as file:
            try:
                data = json.load(file)
                if not validate(data):
                    print('Invalid settings, replacing settings.json...')
                    data = correct(data)
                    rewrite_file(data)
                else:
                    print('Loaded settings.json')
                return data
            except json.decoder.JSONDecodeError:
                print('Invalid settings.json, creating new file...')
                rewrite_file(default_settings)
                return default_settings
    else:
        print('Creating new file settings.json...')
        rewrite_file(default_settings)
        return default_settings


def save(data):
    """
     Save new game settings
    """
    print('Rewriting file settings.json...')
    rewrite_file(data)
