import tcod
import tcod.event

DIR_N = 'north'
DIR_S = 'south'
DIR_E = 'east'
DIR_W = 'west'
DIR_NW = 'northwest'
DIR_SW = 'southwest'
DIR_NE = 'northeast'
DIR_SE = 'southeast'
DIR_C = 'here'

DIRECTION_DELTA = {
    DIR_N: (0, -1),
    DIR_S: (0, 1),
    DIR_E: (1, 0),
    DIR_W: (-1, 0),
    DIR_NW: (-1, -1),
    DIR_SW: (-1, 1),
    DIR_NE: (1, -1),
    DIR_SE: (1, 1),
    DIR_C: (0, 0),
}

DIRECTION_KEYS = {
    tcod.event.SCANCODE_LEFT: DIR_W,
    tcod.event.SCANCODE_RIGHT: DIR_E,
    tcod.event.SCANCODE_DOWN: DIR_S,
    tcod.event.SCANCODE_UP: DIR_N,
    tcod.event.SCANCODE_KP_1: DIR_SW,
    tcod.event.SCANCODE_1: DIR_SW,
    tcod.event.SCANCODE_KP_2: DIR_S,
    tcod.event.SCANCODE_2: DIR_S,
    tcod.event.SCANCODE_KP_3: DIR_SE,
    tcod.event.SCANCODE_3: DIR_SE,
    tcod.event.SCANCODE_KP_4: DIR_W,
    tcod.event.SCANCODE_4: DIR_W,
    tcod.event.SCANCODE_KP_5: DIR_C,
    tcod.event.SCANCODE_5: DIR_C,
    tcod.event.SCANCODE_KP_6: DIR_E,
    tcod.event.SCANCODE_6: DIR_E,
    tcod.event.SCANCODE_KP_7: DIR_NW,
    tcod.event.SCANCODE_7: DIR_NW,
    tcod.event.SCANCODE_KP_8: DIR_N,
    tcod.event.SCANCODE_8: DIR_N,
    tcod.event.SCANCODE_KP_9: DIR_NE,
    tcod.event.SCANCODE_9: DIR_NE,
}

UP_KEYS = set(key for key, val in DIRECTION_KEYS.items() if val == DIR_N)
DOWN_KEYS = set(key for key, val in DIRECTION_KEYS.items() if val == DIR_S)
UPDOWN_KEYS = UP_KEYS | DOWN_KEYS
SELECT_KEYS = {tcod.event.SCANCODE_KP_ENTER, tcod.event.SCANCODE_RETURN, tcod.event.SCANCODE_SPACE}
CANCEL_KEYS = {tcod.event.SCANCODE_ESCAPE}

ALL_DIRECTIONS = list(DIRECTION_DELTA.keys())
