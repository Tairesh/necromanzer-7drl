import time
from typing import List, Tuple

import engine
from engine.tile import Tile


class PathNotFound(Exception):
    pass


class Node:
    def __init__(self, x, y, weight=0):
        self.x = x
        self.y = y
        self.weight = weight
        self.heuristic = 0
        self.parent = None

    @staticmethod
    def from_tile(tile: Tile):
        if tile.is_walkable:
            weight = 1
        elif tile.can_be_opened:
            weight = 2
        else:
            weight = None
        return Node(tile.pos.x, tile.pos.y, weight)

    def __repr__(self):
        return str(self.x) + 'x' + str(self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def heuristic_delta(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y)

    def reconstruct(self) -> List[Tuple[int, int]]:
        node = self
        path = []
        while True:
            if node.parent is None:
                break
            path.append((node.x - node.parent.x, node.y - node.parent.y))
            node = node.parent
        return list(reversed(path))


def neighbours(node) -> List[Node]:
    neighbours_list = []
    tile = engine.world.get_tile(node.x, node.y, False, True)
    if tile:
        for dx, dy in ((-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)):
            neighbour = engine.world.get_tile(node.x + dx, node.y + dy, False, True)
            if neighbour:
                neighbour_node = Node.from_tile(neighbour)
                if neighbour_node.weight is not None:
                    neighbours_list.append(neighbour_node)
    return neighbours_list


def run(point_start, point_goal):
    start = Node(point_start[0], point_start[1])
    end = Node(point_goal[0], point_goal[1])
    closed_list = []
    open_list = [start]

    break_time = time.time() + 0.05
    while 0 < len(open_list) < 500:
        if time.time() >= break_time:
            break

        current_node = open_list.pop(0)

        for node in open_list:
            if node.heuristic < current_node.heuristic:
                current_node = node

        if current_node == end:
            return current_node.reconstruct()

        if current_node in open_list:
            open_list.remove(current_node)

        closed_list.append(current_node)

        for neighbour in neighbours(current_node):
            if neighbour in closed_list:
                continue
            if neighbour.heuristic < current_node.heuristic or neighbour not in open_list:
                neighbour.heuristic = neighbour.weight + neighbour.heuristic_delta(end)
                neighbour.parent = current_node
            if neighbour not in open_list:
                open_list.append(neighbour)

    raise PathNotFound()
