from typing import Optional

import tcod.event
import engine
from engine.drawable.interactive import Interactive

focused_input: Optional[Interactive] = None


def flush():
    if focused_input:
        focused_input.update()

    for entity in engine.current_screen.get_entities():
        if entity and not entity.hidden:
            entity.blit(engine.console, entity.pos.left, entity.pos.top)
    engine.current_screen.draw()

    tcod.console_flush()


def handle_events():
    global focused_input
    interactive = [o for o in engine.current_screen.get_entities() if o and not o.hidden and o.is_interactive]
    for event in tcod.event.get():
        if event.type == 'QUIT':
            engine.stop()
            return
        if event.type == 'KEYUP' and event.scancode == tcod.event.SCANCODE_RETURN and event.mod & tcod.event.KMOD_ALT:
            tcod.console_set_fullscreen(not tcod.console_is_fullscreen())

        if focused_input:
            if event.type in {'KEYDOWN', 'KEYUP'} \
                    and event.scancode in {tcod.event.SCANCODE_ESCAPE, tcod.event.SCANCODE_BACKSPACE}:
                focused_input.key_pressed(event)
                return
            elif event.type == 'TEXTINPUT' and focused_input.is_textinput:
                focused_input.text_inputed(event)

        if event.type in {'KEYDOWN', 'KEYUP'} and not event.mod & tcod.event.KMOD_ALT and not event.repeat:
            keymapped = [o for o in interactive if event.scancode in o.keys and not o.is_textinput]
            if event.type == 'KEYDOWN':
                for entity in keymapped:
                    entity.on_pressed(event.scancode)
            elif event.type == 'KEYUP':
                for entity in keymapped:
                    entity.off_pressed(event.scancode)
                    entity.key_pressed(event)

        elif event.type in {'MOUSEMOTION', 'MOUSEBUTTONDOWN', 'MOUSEBUTTONUP'}:
            collides = [o for o in interactive if o.collide(event.tile)]
            not_collides = [o for o in interactive if o not in collides]

            if event.type == 'MOUSEMOTION':
                for entity in collides:
                    entity.on_hover(event.tile)
                for entity in not_collides:
                    entity.off_hover(event.tile)
            elif event.type == 'MOUSEBUTTONDOWN' and event.button == tcod.event.BUTTON_LEFT:
                for entity in collides:
                    entity.on_mouse_pressed(event.tile)
                if focused_input and not focused_input.collide(event.tile):
                    focused_input.focused = False
                    focused_input.render()
                    focused_input = None
            elif event.type == 'MOUSEBUTTONUP' and event.button == tcod.event.BUTTON_LEFT:
                for entity in collides:
                    entity.off_mouse_pressed(event.tile)
                    entity.click(event.tile)

        engine.current_screen.call(event)
