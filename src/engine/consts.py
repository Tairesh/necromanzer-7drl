import os
import tcod

from engine.body import Body
from engine.brain import Brain
from engine.item import Item
from engine.utils import inheritors

VERSION = '0.1.3-7drl'

WINDOW_TITLE = 'NecromanZer 7DRL'
TILESET = 'res/img/terminal8x14_gs_ro.png'
TILESET_FLAGS = tcod.FONT_TYPE_GREYSCALE | tcod.FONT_LAYOUT_ASCII_INROW

NUMERALS = ('first', 'second', 'third', 'fourth', 'fifth', 'sixth',
            'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth',
            'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth',
            'seventeenth', 'eighteenth', 'nineteenth', 'twentieth',
            'twenty-first', 'twenty-second', 'twenty-third', 'twenty-fourth',
            'twenty-fifth', 'twenty-sixth', 'twenty-seventh', 'twenty-eighth',
            'twenty-ninth', 'thirty-first', 'thirty-second', 'thirty-third')

GAME_DATA = {
    'names': {0: {}, 1: {}},
    'dog_names': {0: [], 1: []},
    'professions': {0: [], 1: []},
    'children_professions': {0: [], 1: []},
    'fears': [],
    'items': {},
    'bodies': {},
    'brains': {},
}


def load_data():
    data_dir = os.path.join('res', 'data')
    files = [os.path.join(data_dir, file) for file in os.listdir(data_dir)]
    for file in files:
        if os.path.isdir(file):
            files += [os.path.join(file, c) for c in os.listdir(file)]
        elif os.path.basename(file) == "fears.txt":
            with open(file, 'r') as fp:
                for line in fp.readlines():
                    GAME_DATA['fears'].append(line.replace('\n', ''))
        elif os.path.basename(file) == "names.txt":
            with open(file, 'r') as fp:
                for line in fp.readlines():
                    name, group, gender = line.replace('\n', '').split(',')
                    gender = int(gender)-1
                    if group in GAME_DATA['names'][gender]:
                        GAME_DATA['names'][gender][group].append(name)
                    else:
                        GAME_DATA['names'][gender][group] = [name]
        elif os.path.basename(file) == "dog_names.txt":
            with open(file, 'r') as fp:
                for line in fp.readlines():
                    name, gender = line.replace('\n', '').split(',')
                    gender = int(gender)-1
                    GAME_DATA['dog_names'][gender].append(name)
        elif os.path.basename(file) == "professions.txt":
            with open(file, 'r') as fp:
                for line in fp.readlines():
                    male_title, female_title = line.replace('\n', '').split(',')
                    if male_title:
                        GAME_DATA['professions'][0].append(male_title)
                    if female_title:
                        GAME_DATA['professions'][1].append(female_title)
        elif os.path.basename(file) == "children_professions.txt":
            with open(file, 'r') as fp:
                for line in fp.readlines():
                    male_title, female_title = line.replace('\n', '').split(',')
                    if male_title:
                        GAME_DATA['children_professions'][0].append(male_title)
                    if female_title:
                        GAME_DATA['children_professions'][1].append(female_title)

    for item_class in inheritors(Item):
        if hasattr(item_class, 'proto'):
            GAME_DATA['items'][item_class.proto] = item_class

    for item_class in inheritors(Body):
        if hasattr(item_class, 'proto'):
            GAME_DATA['bodies'][item_class.proto] = item_class

    for item_class in inheritors(Brain):
        if hasattr(item_class, 'proto'):
            GAME_DATA['brains'][item_class.proto] = item_class

    print(f"Loaded raw data: {len(GAME_DATA['items'])} items, " +
          f"{len(GAME_DATA['brains'])} brain types, {len(GAME_DATA['bodies'])} body types, " +
          f"{len(GAME_DATA['fears'])} fear messages, " +
          f"{len(GAME_DATA['dog_names'][0])} dog boys names and {len(GAME_DATA['dog_names'][1])} dog girls names, " +
          f"{len(GAME_DATA['professions'][0])} male professions, {len(GAME_DATA['professions'][1])} female professions, " +
          f"{len(GAME_DATA['children_professions'][0])} boys professions, {len(GAME_DATA['children_professions'][1])} girls professions, " +
          f"{len(GAME_DATA['names'][0])} male names and {len(GAME_DATA['names'][1])} female names")


