import json

import tcod
from enum import Enum, auto, IntEnum

from engine import consts


class ItemFlag(Enum):
    CAN_DIG = auto()    # like shovel
    CAN_BUTCH = auto()  # like knife
    CAN_HACK = auto()   # like axe


class ItemProto(IntEnum):
    BOULDER = auto()
    GRAVESTONE = auto()

    DOOR = auto()
    TABLE = auto()
    STOOL = auto()
    BENCH = auto()
    CHEST = auto()
    BED = auto()

    SHOVEL = auto()
    KNIFE = auto()
    AXE = auto()

    POT = auto()
    BARREL = auto()
    BUCKET = auto()
    JUG = auto()

    HUMAN_HEAD = auto()
    HUMAN_TORSO = auto()
    HUMAN_HAND = auto()
    HUMAN_LEG = auto()

    HUMAN_HEAD_ROTTEN = auto()
    HUMAN_TORSO_ROTTEN = auto()
    HUMAN_HAND_ROTTEN = auto()
    HUMAN_LEG_ROTTEN = auto()

    HUMAN_HEAD_SKELETAL = auto()
    HUMAN_TORSO_SKELETAL = auto()
    HUMAN_HAND_SKELETAL = auto()
    HUMAN_LEG_SKELETAL = auto()

    PITCHFORK = auto()

    CLOTH = auto()
    THREAD = auto()
    HANDFUL_OF_COINS = auto()

    CHILD_HEAD = auto()
    CHILD_TORSO = auto()
    CHILD_LEG = auto()
    CHILD_HAND = auto()

    CHILD_HEAD_ROTTEN = auto()
    CHILD_TORSO_ROTTEN = auto()
    CHILD_HAND_ROTTEN = auto()
    CHILD_LEG_ROTTEN = auto()

    CHILD_HEAD_SKELETAL = auto()
    CHILD_TORSO_SKELETAL = auto()
    CHILD_HAND_SKELETAL = auto()
    CHILD_LEG_SKELETAL = auto()

    DOG_HEAD = auto()
    DOG_TORSO = auto()
    DOG_LEG = auto()
    DOG_TAIL = auto()

    DOG_HEAD_ROTTEN = auto()
    DOG_TORSO_ROTTEN = auto()
    DOG_LEG_ROTTEN = auto()
    DOG_TAIL_ROTTEN = auto()

    DOG_HEAD_SKELETAL = auto()
    DOG_TORSO_SKELETAL = auto()
    DOG_LEG_SKELETAL = auto()
    DOG_TAIL_SKELETAL = auto()


class Item:

    proto: ItemProto
    is_grabable = True  # can be grabbed if lying on the ground
    is_walkable = True
    symbol = '%'
    color = tcod.white
    bg_color = None
    title = 'dummy item'
    mass = 1.0
    max_hp = 1
    actions = ()
    flags = ()
    attack_verb = 'smashing'
    attack_verb_title = None
    attack_damage = 0
    attack_length = 1
    count_in_blue_screen = True  # False in benches and tables
    z_index = 5

    def __init__(self, data: dict = None):
        self.hp = data['hp'] if data and 'hp' in data else self.max_hp
        self.raw_data = data if data else {}

    @property
    def displayed_name(self) -> str:
        text = self.title
        if self.hp < self.max_hp:
            text += f" ({round(100*self.hp/self.max_hp)}%)"
        return text

    def serialize(self) -> str:
        return str(int(self.proto)) + ';' + json.dumps(self.data, separators=(',', ':'))

    @staticmethod
    def deserialize(value):
        if not value:
            return None
        proto, data = value.split(';')
        proto = int(proto)
        data = json.loads(data)
        return consts.GAME_DATA['items'][proto](data)

    @property
    def data(self):
        data = self.raw_data.copy()
        data['hp'] = self.hp
        return data

    def act(self, action: str, consumer):
        pass

    def action_length(self, action: str, consumer) -> int:
        return 0

    @property
    def hands_to_grab(self) -> int:
        mass_factor = (self.mass-0.001)/50
        if mass_factor < 1 and self.mass < 15:
            return 1
        else:
            return int(mass_factor) + 2
