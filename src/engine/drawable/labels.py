import tcod
from .interactive import Interactive
from . import Drawable
from typing import Tuple


class Label(Interactive):

    def __init__(self, pos, text, fg=tcod.white, fg_hover=tcod.chartreuse, target=None, min_width=0):
        self.text = text
        self.color = fg
        self.hover_color = fg_hover
        self.target = target
        super().__init__(pos, max(len(text), min_width), 1)
        self.render()

    def render(self):
        self.print(0, 0, self.text, self.hover_color if self.hover else self.color)

    @property
    def is_interactive(self):
        if self.target:
            return True
        else:
            return False

    is_textinput = False
    keys = ()

    def click(self, pos):
        if self.target:
            self.target.click(pos)


class Help(Drawable):
    def __init__(self, pos, values, fg_text=tcod.white, fg_alt=tcod.chartreuse):
        self.values = values
        self.fg_text = fg_text
        self.fg_alt = fg_alt
        super().__init__(pos, self._calc_width(), 1)
        self.render()

    def _calc_width(self) -> int:
        ar = []
        for row in self.values:
            row_w = 0
            val1, val2 = row[0], row[1]
            if type(val1) is int:
                row_w += 2
            else:
                row_w += len(val1) + 1
            if type(val2) is int:
                row_w += 1
            else:
                row_w += len(val2)
            ar.append(row_w)
        return sum(ar) + len(ar) - 1

    def render(self):
        x = 0
        for row in self.values:
            if len(row) == 2:
                val1, val2 = row
                custom_color = self.fg_text
            else:
                val1, val2, custom_color = row

            if type(val1) is str:
                v = f"{val1}:"
                self.print(x, 0, v, self.fg_alt)
                x += len(v)
            else:
                self.default_fg = self.fg_alt
                if type(val1) is tuple:
                    for symbol in val1:
                        self.put_char(x, 0, symbol)
                        x += 1
                else:
                    self.put_char(x, 0, val1)
                    x += 1
                self.print(x, 0, ":", self.fg_alt)
                x += 1
            if type(val2) is str:
                self.print(x, 0, val2, custom_color)
                x += len(val2) + 1
            else:
                self.default_fg = custom_color
                self.put_char(x, 0, val2)
                x += 2
