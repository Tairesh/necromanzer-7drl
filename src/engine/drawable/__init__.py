from tcod.console import Console
import tcod.constants

import engine

BORDER_SYMBOLS = {
    #           0    1    2    3     4    5    6    7
    'line':   ('─', '│', '┌', '┐', '└', '┘', '┬', '┴'),
    'double': ('═', '║', '╔', '╗', '╚', '╝', '╦', '╩'),
    'grate':  ['#' for i in range(8)],
    'block':  ['█' for i in range(8)],
    'empty':  [' ' for i in range(8)],
}


class Pos:
    def __init__(self, owner: Console, x: int, y: int, anchor_x='left', anchor_y='top'):
        self.owner = owner
        self.x = x
        self.y = y
        self.anchor_x = anchor_x
        self.anchor_y = anchor_y
        self.left, self.right, self.top, self.bottom = self._calc_pos()

    def set(self, x, y, anchor_x=None, anchor_y=None):
        self.x = x
        self.y = y
        if anchor_x is not None:
            self.anchor_x = anchor_x
        if anchor_y is not None:
            self.anchor_y = anchor_y
        self.left, self.right, self.top, self.bottom = self._calc_pos()

    def _calc_pos(self):
        if self.anchor_x == 'left':
            left = self.x
        elif self.anchor_x == 'center':
            left = self.x - self.owner.width//2
        elif self.anchor_x == 'right':
            left = self.x - self.owner.width
        else:
            raise Exception("Invalid anchor_x value")

        if self.anchor_y == 'top':
            top = self.y
        elif self.anchor_y == 'center':
            top = self.y - self.owner.height//2
        elif self.anchor_y == 'bottom':
            top = self.y - self.owner.height
        else:
            raise Exception("Invalid anchor_y value")

        right = left + self.owner.width
        bottom = top + self.owner.height
        return left, right, top, bottom


class Drawable(Console):
    is_interactive = False
    disabled = False
    hidden = False

    def __init__(self, pos, width, height):
        Console.__init__(self, width, height, "F")
        self.pos = self._pos_convert(pos)
        self.parent = None

    def _pos_convert(self, pos):
        if pos == 'center':
            return Pos(self, engine.console.width//2, engine.console.height//2, 'center', 'center')
        elif pos is None:
            return None
        else:
            return Pos(self, *pos)

    def set_pos(self, pos):
        self.pos = self._pos_convert(pos)

    def collide(self, pos):
        x, y = pos
        return self.pos.left <= x < self.pos.right and self.pos.top <= y < self.pos.bottom

    def draw_frame(self, x: int, y: int, width: int, height: int, title: str = "", clear: bool = True,
                   fg: tuple = None, bg: tuple = None, bg_blend: int = tcod.constants.BKGND_SET,
                   border_type: str = 'line',
                   title_fg: tuple = None, title_bg: tuple = None, title_pos='center'):
        if fg:
            self.default_fg = fg
        if bg:
            self.default_bg = bg
        symbols = BORDER_SYMBOLS[border_type]
        self.draw_rect(x+1, y+height-1, width-2, 1, ord(symbols[0]), fg, bg, bg_blend)
        self.draw_rect(x+1, y+0, width-2, 1, ord(symbols[0]), fg, bg, bg_blend)
        self.draw_rect(x+0, y+1, 1, height-2, ord(symbols[1]), fg, bg, bg_blend)
        self.draw_rect(x+width-1, y+1, 1, height-2, ord(symbols[1]), fg, bg, bg_blend)
        self.put_char(x+0, y+0, ord(symbols[2]), bg_blend)
        self.put_char(x+width-1, y+0, ord(symbols[3]), bg_blend)
        self.put_char(x+0, y+height-1, ord(symbols[4]), bg_blend)
        self.put_char(x+width-1, y+height-1, ord(symbols[5]), bg_blend)

        if title:
            if title_pos == 'center':
                tx = width//2-len(title)//2
            elif title_pos == 'left':
                tx = 2
            elif title_pos == 'right':
                tx = width - len(title) - 2
            else:
                raise Exception("Invalid title_pos")
            self.print(x+tx, y+0, title, title_fg, title_bg, bg_blend)

    def render(self):
        if self.parent:
            self.parent.render()

