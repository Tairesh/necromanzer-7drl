from .interactive import Interactive
import tcod


class Button(Interactive):

    def __init__(self, pos, text, click=lambda btn: None, key=None, disabled=False):
        self.text = text
        self.disabled = disabled
        super().__init__(pos, len(self.text), 1, click, {key, } if key else ())

    def click(self, pos):
        if not self.disabled:
            super().click(pos)

    def key_pressed(self, event):
        if not self.disabled:
            super().key_pressed(event)

    def render(self):
        self.clear()
        if self.disabled:
            fg = tcod.gray
            bg = tcod.black
        elif self.pressed:
            fg = tcod.black
            bg = tcod.dark_chartreuse
        elif self.hover:
            fg = tcod.black
            bg = tcod.chartreuse
        elif self.focused:
            fg = tcod.darkest_gray
            bg = tcod.chartreuse
        else:
            fg = tcod.white
            bg = tcod.black
        self.print(0, 0, self.text, fg, bg)
        super().render()


class BorderedButton(Interactive):

    def __init__(self, pos, text, click=lambda btn: None, key=None):
        self.text = text
        super().__init__(pos, len(self.text)+2, 3, click, {key, } if key else ())

    def render(self):
        if self.pressed:
            fg = tcod.azure
        elif self.hover:
            fg = tcod.white
        elif self.focused:
            fg = tcod.white
        else:
            fg = tcod.lightest_gray
        self.default_fg = fg
        self.print_frame(0, 0, self.width, self.height, bg_blend=tcod.BKGND_SET)
        self.print(1, 1, self.text, fg)
        super().render()
