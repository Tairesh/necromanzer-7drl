import tcod
import tcod.event

import engine
from .boxes import Box, BoxParams
from .. import keys


class LogBox(Box):

    scancodes_subscribe = keys.UPDOWN_KEYS | {tcod.event.SCANCODE_PAGEUP, tcod.event.SCANCODE_PAGEDOWN,
                                              tcod.event.SCANCODE_HOME, tcod.event.SCANCODE_END}

    def __init__(self, log):
        self._raw_log = log
        self._prepared_log = self._prepare_log()
        self.offset = 0
        super().__init__('center', engine.console.width-20, 20, BoxParams(" Game log "))
        self.render()

    @property
    def log(self):
        return self._prepared_log

    @log.setter
    def log(self, log):
        self._raw_log = log
        self._prepared_log = self._prepare_log()

    def _prepare_log(self) -> tuple:
        log = []
        w = engine.console.width - 20 - 3
        for text, color in self._raw_log:
            if len(text) > w:
                line = ''
                for word in text.split(' '):
                    if len(line) + len(word) + 1 <= w:
                        line += (' ' if len(line) else '') + word
                    else:
                        log.append((line, color))
                        line = word
                if len(line):
                    log.append((line, color))
            else:
                log.append((text, color))
        return tuple(log)

    def render(self):
        self.clear()
        self._draw_border()
        height = self.height-2
        for i, (text, color) in enumerate(self.log[-height-self.offset:][:height]):
            self.print(1, i+1, text, color)

        if len(self.log) > height:
            for i in range(height):
                self.print(self.width-2, 1+i, ' ', bg=tcod.darkest_gray)
            cursor_y = round(height - ((height - 1) * self.offset/self.max_offset))
            self.put_char(self.width-2, cursor_y, 177)

    @property
    def max_offset(self):
        return len(self.log) - (self.height-2)

    def show(self):
        self.hidden = False
        self.render()

    def call(self, event: tcod.event.KeyDown):
        if event.scancode in keys.UP_KEYS:
            self.up()
        elif event.scancode in keys.DOWN_KEYS:
            self.down()
        elif event.scancode == tcod.event.SCANCODE_HOME:
            self.offset = self.max_offset
            self.render()
        elif event.scancode == tcod.event.SCANCODE_END:
            self.offset = 0
            self.render()
        elif event.scancode == tcod.event.SCANCODE_PAGEUP:
            self.offset += self.height - 2
            if self.offset > self.max_offset:
                self.offset = self.max_offset
            self.render()
        elif event.scancode == tcod.event.SCANCODE_PAGEDOWN:
            self.offset -= self.height - 2
            if self.offset < 0:
                self.offset = 0
            self.render()

    def up(self):
        if self.offset < self.max_offset:
            self.offset += 1
            self.render()

    def down(self):
        if self.offset > 0:
            self.offset -= 1
            self.render()
