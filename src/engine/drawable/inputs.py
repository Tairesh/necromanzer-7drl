from .interactive import Interactive
from .labels import Label
import tcod
from tcod.event import *
import tcod.constants
from engine import interface
import time


TEXT_INPUT_W_KEYS = [i for i in range(4, 40)] + [SCANCODE_SPACE, ]
TEXT_INPUT_KEYS = set([SCANCODE_BACKSPACE, SCANCODE_ESCAPE] + TEXT_INPUT_W_KEYS)


class TextInput(Interactive):
    is_textinput = True
    CURSOR_BLINK_TIME = 0.5

    def __init__(self, pos, width, value='', on_change=lambda inp: None):
        self.focused = False
        self.on_change = on_change
        self.value = value
        self.cursor_state = False
        self.cursor_last_blinked = 0
        super().__init__(pos, width, 1, keys=TEXT_INPUT_KEYS)

    def render(self):
        self.clear(ord('_'), tcod.white, tcod.darkest_gray if self.pressed or self.focused else tcod.black)
        self.default_fg = tcod.white
        self.print(0, 0, self.value, self.default_fg)
        if self.cursor_state and self.focused:
            self.put_char(len(self.value), 0, 219)
        super().render()

    def click(self, pos):
        self.focused = True
        interface.focused_input = self
        self.cursor_state = True
        self.cursor_last_blinked = time.time()
        if self.parent:
            self.parent.set_selected_entity(self)
        self.render()

    def key_pressed(self, event):
        if not event:
            return
        if event.type == 'KEYUP':
            if event.scancode == SCANCODE_ESCAPE:
                self.focused = False
                interface.focused_input = None
        elif event.type == 'KEYDOWN':
            if event.scancode == SCANCODE_BACKSPACE:
                self.value = self.value[:-1:]
                self.on_change(self)
        self.render()

    def text_inputed(self, event):
        self.value += event.text
        self.on_change(self)
        self.render()

    def update(self):
        if time.time() - self.cursor_last_blinked > self.CURSOR_BLINK_TIME:
            self.cursor_state = not self.cursor_state
            self.cursor_last_blinked = time.time()
            self.render()


class NumberInput(TextInput):

    def text_inputed(self, event):
        if not event.text.isdigit():
            return
        super().text_inputed(event)


class SelectInput(Interactive):
    def __init__(self, pos, width, value=0, values=(), icons=(), icons_colors=(), on_change=lambda inp: None, roll=False):
        self.on_change = on_change
        self.value = value
        self.values = values
        self.icons = icons
        self.icons_colors = icons_colors
        self.roll = roll
        self.prev_btn_pressed = False
        self.prev_btn_hovered = False
        self.next_btn_pressed = False
        self.next_btn_hovered = False
        super().__init__(pos, width, 1, keys={SCANCODE_LEFT, SCANCODE_RIGHT, SCANCODE_ESCAPE})

    def on_mouse_pressed(self, pos):
        self.pressed = True
        if pos == (self.pos.left, self.pos.top):
            self.prev_btn_pressed = True
        if pos == (self.pos.right-1, self.pos.top):
            self.next_btn_pressed = True
        self.render()

    def off_mouse_pressed(self, pos):
        self.pressed = False
        self.prev_btn_pressed = False
        self.next_btn_pressed = False
        self.render()

    def on_hover(self, pos):
        self.hover = True
        if pos == (self.pos.left, self.pos.top):
            self.prev_btn_hovered = True
        else:
            self.prev_btn_hovered = False
        if pos == (self.pos.right-1, self.pos.top):
            self.next_btn_hovered = True
        else:
            self.next_btn_hovered = False
        self.render()

    def off_hover(self, pos):
        self.hover = False
        self.prev_btn_hovered = False
        self.next_btn_hovered = False
        self.render()

    def click(self, pos):
        self.focused = True
        interface.focused_input = self
        if self.parent:
            self.parent.set_selected_entity(self)
        if pos == (self.pos.left, self.pos.top):
            self.prev()
        if pos == (self.pos.right-1, self.pos.top):
            self.next()
        self.render()

    def update(self):
        pass

    def render(self):
        bg = tcod.darkest_gray if self.focused else tcod.black
        self.clear(bg=bg)
        self.print(0, 0, '<',
                   tcod.chartreuse if self.prev_btn_pressed else tcod.white,
                   tcod.darkest_gray if self.prev_btn_hovered else bg)
        val_w = len(self._str_value())
        if len(self.icons):
            val_w += 2
        x = self.width//2 - val_w//2
        if len(self.icons):
            if len(self.icons_colors):
                self.default_fg = self.icons_colors[self.value]
            self.put_char(x, 0, self.icons[self.value])
            x += 2
        self.print(x, 0, self._str_value(), self.icons_colors[self.value] if len(self.icons_colors) else tcod.white)
        self.print(self.width-1, 0, '>',
                   tcod.chartreuse if self.next_btn_pressed else tcod.white,
                   tcod.darkest_gray if self.next_btn_hovered else bg)
        super().render()

    def _str_value(self):
        return str(self.values[self.value])

    def _correct_val(self):
        if self.value > len(self.values)-1:
            self.value = 0 if self.roll else len(self.values)-1
        if self.value < 0:
            self.value = len(self.values)-1 if self.roll else 0

    def _change_val(self, delta):
        old_val = self.value
        self.value += delta
        self._correct_val()

        if self.value != old_val:
            self.on_change(self)
            self.render()

    def next(self):
        self._change_val(+1)

    def prev(self):
        self._change_val(-1)

    def on_pressed(self, key):
        if not self.focused:
            return
        if key == SCANCODE_RIGHT:
            self.next_btn_pressed = True
            self.next()
        elif key == SCANCODE_LEFT:
            self.prev_btn_pressed = True
            self.prev()
        elif key == SCANCODE_ESCAPE:
            self.focused = False
            interface.focused_input = None
        self.render()

    def off_pressed(self, key):
        if not self.focused:
            return
        if key == SCANCODE_RIGHT:
            self.next_btn_pressed = False
        elif key == SCANCODE_LEFT:
            self.prev_btn_pressed = False
        self.render()


class NumberSelect(SelectInput):
    def __init__(self, pos, width, value=0, value_min=0, value_max=100, on_change=lambda inp: None):
        values = [str(i) for i in range(value_min, value_max)]
        self.value_min = value_min
        self.value_max = value_max
        super().__init__(pos, width, value, values, on_change=on_change)

    def _correct_val(self):
        if self.value < self.value_min:
            self.value = self.value_min
        if self.value > self.value_max:
            self.value = self.value_max

    def _str_value(self):
        return str(self.value)
