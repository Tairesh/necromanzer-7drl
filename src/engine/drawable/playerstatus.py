import tcod
import engine
from worldgen import Structure
from . import Drawable
from .boxes import Box, BoxParams
from .labels import Help, Label
from items.bodyparts import BodyPart
from typing import Optional
from ..body import SkeletonHumanBody
from ..chunk import CHUNK_SIZE
from ..item import Item


def _part_status(part: Optional[BodyPart]):
    return f"{int(100*part.hp/part.max_hp)}%" if part else "xXxXx"


def _part_color(part: Optional[BodyPart]):
    if part is None:
        return tcod.dark_red
    elif part.hp < part.max_hp/2:
        return tcod.red
    elif part.hp < part.max_hp:
        return tcod.orange
    else:
        return tcod.chartreuse


def _road(chunk_x, chunk_y):
    left = (chunk_x-1, chunk_y) in engine.world.structures[Structure.ROAD]
    right = (chunk_x+1, chunk_y) in engine.world.structures[Structure.ROAD]
    top = (chunk_x, chunk_y-1) in engine.world.structures[Structure.ROAD]
    bottom = (chunk_x, chunk_y+1) in engine.world.structures[Structure.ROAD]

    if right and left and top and bottom:
        return 206  # '┼'
    elif right and left and not top and not bottom:
        return 205  # '─'
    elif not right and not left and top and bottom:
        return 186  # '│'
    elif not right and not left and not top and not bottom:
        return 9  # '*'
    elif right and not left and not top and bottom:
        return 201  # '┌'
    elif right and not left and top and bottom:
        return 204  # '├'
    elif right and left and not top and bottom:
        return 203  # '┬'
    elif not right and left and not top and not bottom:
        return 181  # '>'
    elif not right and left and top and not bottom:
        return 188  # '┘'
    elif right and left and top and not bottom:
        return 202  # '┴'
    elif not right and not left and not top and bottom:
        return 210  # '^'
    elif not right and left and not top and bottom:
        return 187  # '┐'
    elif not right and left and top and bottom:
        return 185  # '┤'
    elif not right and not left and top and not bottom:
        return 208  # 'v'
    elif right and not left and not top and not bottom:
        return 198  # '<'
    elif right and not left and top and not bottom:
        return 200  # '└'


class PlayerStatus(Drawable):

    def __init__(self):
        super().__init__((engine.console.width-20, 0), 20, engine.console.height-2)
        self.render()

    def render(self):
        player = engine.world.player
        head = player.body.parts['head']
        torso = player.body.parts['torso']
        left_hand = player.body.parts['left arm']
        right_hand = player.body.parts['right arm']
        left_leg = player.body.parts['left leg']
        right_leg = player.body.parts['right leg']
        right_hand_item = player.wield['right arm'] if 'right arm' in player.wield else None
        left_hand_item = player.wield['left arm'] if 'left arm' in player.wield else None

        self.clear()

        date = engine.world.current_date

        def _pad_zero(val: int) -> str:
            if val < 10:
                return f"0{val}"
            return str(val)

        time = f"{_pad_zero(date.hour)}:{_pad_zero(date.minute)}:{_pad_zero(date.second)}"
        self.print(0, 0, f"{time} {_pad_zero(date.day)}.{_pad_zero(date.month)}.{engine.world.current_year}", tcod.white)

        stamina_delta = player.stamina / player.max_stamina
        if stamina_delta > 0.8:
            stamina_color = tcod.chartreuse
        elif stamina_delta > 0.5:
            stamina_color = tcod.yellow
        elif stamina_delta > 0.2:
            stamina_color = tcod.orange
        else:
            stamina_color = tcod.red
        self.print(0, 1, 'Stamina:', stamina_color)
        sw = self.width - 9
        sw_filled = round(sw * stamina_delta)
        for i in range(sw_filled):
            self.print(9+i, 1, ' ', tcod.white, stamina_color)
        self.print(14, 1, f"{round(100*stamina_delta)}%", tcod.black if stamina_delta > 0.5 else tcod.white,
                   alignment=tcod.CENTER)

        self.print(0, 2, f"Moving mode: {'walk' if player.walkmode == 1 else 'run'}",
                   tcod.chartreuse if player.walkmode == 1 else tcod.orange)

        y = 3
        self.print(0, y, 'R. hand:', _part_color(right_hand))
        if not right_hand:
            self.print(9, y, '(can`t hold)', tcod.dark_red)
        elif right_hand_item:
            self.print(9, y, right_hand_item.title, right_hand_item.color)
        else:
            self.print(9, y, '(empty)', tcod.light_gray)
        self.print(0, y+1, 'L. hand:', _part_color(left_hand))
        if not left_hand:
            self.print(9, y+1, '(can`t hold)', tcod.dark_red)
        elif left_hand_item:
            self.print(9, y+1, left_hand_item.title, left_hand_item.color)
        else:
            self.print(9, y+1, '(empty)', tcod.light_gray)

        y = 5
        # minimap
        self.draw_frame(0, y, 20, 10, 'Map', fg=tcod.chartreuse)
        chunk_x, chunk_y = engine.world.player.pos.x // CHUNK_SIZE, engine.world.player.pos.y // CHUNK_SIZE
        for i in range(18):
            for j in range(8):
                if i == 9 and j == 4:
                    symbol = 64
                    self.default_fg = tcod.white
                else:
                    chunk = chunk_x + i - 9, chunk_y + j - 4
                    if chunk in engine.world.structures[Structure.ROAD]:
                        symbol = _road(*chunk)
                        self.default_fg = tcod.dark_gray
                    elif chunk in engine.world.structures[Structure.GRAVEYARD]:
                        symbol = 234
                        self.default_fg = tcod.lighter_gray
                    elif chunk in engine.world.structures[Structure.FIELD]:
                        symbol = 247
                        self.default_fg = tcod.sepia
                    elif chunk in engine.world.structures[Structure.HOUSE]:
                        symbol = 127
                        self.default_fg = tcod.yellow
                    else:
                        symbol = 44
                        self.default_fg = tcod.darkest_green

                self.put_char(1+i, 1+j+y, symbol)

        y = y + 10
        self.draw_frame(0, y, 20, 5, 'Body parts HP', fg=tcod.lighter_red)
        self.print(9, y+1, _part_status(head), _part_color(head), tcod.darkest_gray, alignment=tcod.CENTER)
        torso_status = _part_status(torso)
        self.print(9, y+2, torso_status, _part_color(torso), tcod.darkest_gray, alignment=tcod.CENTER)
        self.print(9-len(torso_status)//2-2, y+2, _part_status(right_hand), _part_color(right_hand), tcod.darkest_gray, alignment=tcod.RIGHT)
        self.print(9+len(torso_status)//2+2, y+2, _part_status(left_hand), _part_color(left_hand), tcod.darkest_gray, alignment=tcod.LEFT)
        self.print(7, y+3, _part_status(right_leg), _part_color(right_leg), tcod.darkest_gray, alignment=tcod.RIGHT)
        self.print(10, y+3, _part_status(left_leg), _part_color(left_leg), tcod.darkest_gray, alignment=tcod.LEFT)


class BodyInfo(Box):

    def __init__(self, unit):
        self.unit = unit
        self.selected_part_i = None
        self.mode = 'view'
        self.parts_order = self._calc_parts_order()
        if self.unit.body.permacolor == tcod.light_gray:
            skin_tone = 'Unknown'
            skin_color = tcod.white
        else:
            skin_tone = ('Albino', 'Pale', 'Olive', 'Dark')[self.unit.raw_params['skin_tone']]
            skin_color = ((0xF7, 0xEB, 0xDB), (0xF3, 0xC6, 0xA4), (0xED, 0xB9, 0x8A), (0x97, 0x5B, 0x43))[self.unit.raw_params['skin_tone']]
        self.info = Help(None, (
            ('Age', str(self.unit.age)),
            ('Sex', (11, 12)[self.unit.raw_params['gender']]),
            ('Skin', skin_tone, skin_color),
        ), fg_alt=tcod.white)
        super().__init__('center', engine.console.width-40, 20,
                         BoxParams(f" Body of {unit.displayed_name} "))
        self.render()

    def _calc_parts_order(self):
        order = []
        for key, _, __ in self.unit.body.schema:
            if key in self.unit.body.parts:
                order.append(key)
        for key in self.unit.body.parts:
            if key not in order:
                order.append(key)
        return tuple(order)

    @property
    def selected_part_key(self):
        return self.parts_order[self.selected_part_i]

    def init_select(self):
        if self.selected_part_i is None:
            self.selected_part_i = 0

    def next(self):
        if self.selected_part_i is not None and self.selected_part_i < len(self.parts_order) - 1:
            self.selected_part_i += 1
        else:
            self.selected_part_i = 0
        self.render()

    def prev(self):
        if self.selected_part_i is not None and self.selected_part_i > 0:
            self.selected_part_i -= 1
        else:
            self.selected_part_i = len(self.parts_order) - 1
        self.render()

    def unselect(self):
        self.selected_part_i = None
        self.render()

    def set_mode(self, mode):
        self.mode = mode
        self.render()

    def render(self):
        self.parts_order = self._calc_parts_order()
        self.clear()
        self._draw_border()
        if self.mode == 'cut':
            self.print(1, 1, "Select part to cut off:", tcod.chartreuse)
        elif self.mode == 'replace':
            self.print(1, 1, "Select part to replace:", tcod.chartreuse)
        elif self.mode == 'insert':
            self.print(1, 1, "Insert new body part:", tcod.chartreuse)
        else:
            self.print(1, 1, 'Examine:', tcod.chartreuse)
            self.info.blit(self, 10, 1)

        for i, key in enumerate(self.parts_order):
            part = self.unit.body.parts[key]
            if self.selected_part_i is not None and i == self.selected_part_i:
                self.draw_rect(1, 2+i, self.width-2, 1, 0, bg=tcod.darkest_green)
            elif i % 2 == 0:
                self.draw_rect(1, 2+i, self.width-2, 1, 0, bg=tcod.darkest_gray)

            self.print(1, 2+i, f"{key.capitalize()}:", tcod.white)
            if part:
                if type(part.symbol) is str:
                    self.print(self.width//2-7, 2+i, part.symbol, part.color)
                else:
                    self.default_fg = part.color
                    self.put_char(self.width//2-7, 2+i, part.symbol)
                self.print(self.width//2-5, 2+i, part.title, part.color)
            self.print(self.width-2, 2+i, _part_status(part), _part_color(part), alignment=tcod.RIGHT)


class HelpBox(Box):

    scancodes_subscribe = ()

    def __init__(self):

        self.rows = (
            Help(None, (
                ((18, 29), 'Move'),
                ('.', 'Skip time'),
                ('@', 'Check yourself'),
                ('x', 'Observe'),
                ('P', 'Clear log'),
            )),
            Help(None, (
                ('e', 'Examine'),
                ('p', 'Game log'),
                ('w', 'Wield'),
                ('d', 'Drop'),
                ('g', 'Pick up'),
                ('G', 'Grab'),
            )),
            Help(None, (
                ('r', 'Read'),
                ('o', 'Open'),
                ('c', 'Close'),
                ('D', 'Dig'),
                ('s', 'Smash'),
                ('V', 'View items around'),
            )),
            Help(None, (
                ('a', 'Animate dead body'),
                ('O', 'Give orders'),
                ('!', 'Change moving mode')
            )),
            Label(None, ""),
            Label(None, "You can also use mouse in most cases", tcod.chartreuse),
        )
        super().__init__('center', engine.console.width-40, 20, BoxParams(" Help "))
        self.render()

    def render(self):
        self.clear()
        self._draw_border()
        for i, row in enumerate(self.rows):
            row.blit(self, 1, 1+i)


def _item_status(item: Optional[Item]):
    return f"{int(100*item.hp/item.max_hp)}%"


def _item_color(item: Optional[Item]):
    if item is None:
        return tcod.dark_red
    elif item.hp < item.max_hp/2:
        return tcod.red
    elif item.hp < item.max_hp:
        return tcod.orange
    else:
        return tcod.chartreuse


class InventoryBox(Box):

    def __init__(self, unit):
        self.unit = unit
        self.selected = 0
        super().__init__('center', engine.console.width-40, 20, BoxParams(f" {self.unit.name_apostrophe_s} inventory "))
        self.render()

    def render(self):
        self.clear()
        self._draw_border()

        if len(self.unit.inventory):
            for i, item in enumerate(self.unit.inventory):
                if self.selected is not None and i == self.selected:
                    self.draw_rect(1, 1+i, self.width-2, 1, 0, bg=tcod.darkest_green)
                elif i % 2 == 1:
                    self.draw_rect(1, 1+i, self.width-2, 1, 0, bg=tcod.darkest_gray)

                if type(item.symbol) is str:
                    self.print(1, 1+i, item.symbol, item.color)
                else:
                    self.default_fg = item.color
                    self.put_char(1, 1+i, item.symbol)
                self.print(3, 1+i, item.title, item.color)
                self.print(self.width-2, 1+i, _item_status(item), _item_color(item), alignment=tcod.RIGHT)
        else:
            self.print(self.width//2, self.height//2, f"{self.unit.name_apostrophe_s} inventory is empty", tcod.white, alignment=tcod.CENTER)

    def down(self):
        if self.selected is not None and self.selected < len(self.unit.inventory) - 1:
            self.selected += 1
        else:
            self.selected = 0
        self.render()

    def up(self):
        if self.selected is not None and self.selected > 0:
            self.selected -= 1
        else:
            self.selected = len(self.unit.inventory) - 1
        self.render()
