import tcod
import engine
from . import Drawable
from ..tile import MapPos

LOADING_SYMBOLS = ('/', '-', '\\', '|', '/', '-', '\\', '|')
ARROWS = {
    (-1, -1): 23,
    (-1, 0): 27,
    (-1, 1): 249,
    (0, -1): 24,
    (0, 1): 25,
    (1, -1): 255,
    (1, 0): 26,
    (1, 1): 250,
}


class MapView(Drawable):

    def __init__(self, center_x, center_y):
        self.center_x = center_x
        self.center_y = center_y
        self.animation_tick = 0
        super().__init__((0, 0), engine.console.width-20, engine.console.height)
        self.render()

    @property
    def left(self):
        return self.center_x - self.width//2

    @property
    def right(self):
        return self.center_x + self.width//2

    @property
    def top(self):
        return self.center_y - self.height//2

    @property
    def bottom(self):
        return self.center_y + self.height//2

    def map_pos_to_screen(self, x, y=None):
        if type(x) is MapPos:
            x, y = x.x, x.y
        elif y is None:
            x, y = x
        dx, dy = x - self.left, y - self.top
        return dx, dy

    def screen_to_map_pos(self, x, y=None):
        if type(x) is MapPos:
            x, y = x.x, x.y
        elif y is None:
            x, y = x
        dx, dy = x + self.left, y + self.top
        return dx, dy

    def render(self):
        self.clear()
        for i in range(self.left, self.right):
            for j in range(self.top, self.bottom):
                tile = engine.world.get_tile(i, j, False)
                if type(tile.symbol) is str:
                    self.print(i-self.left, j-self.top, tile.symbol, tile.color, tile.bg_color)
                else:
                    self.default_fg = tile.color
                    self.default_bg = tile.bg_color
                    self.put_char(i-self.left, j-self.top, tile.symbol, tcod.BKGND_SET)

        for unit in reversed(engine.world.active_units):
            if not unit.current_action:
                continue
            cx, cy = self.map_pos_to_screen(unit.pos)
            if unit.current_action.__class__.__name__ in {'Move', 'WalkMove'}:
                dx, dy = unit.current_action.dx, unit.current_action.dy
                tile = engine.world.get_tile(unit.pos.x + dx, unit.pos.y + dy, False)
                if tile.is_walkable and (dx != 0 or dy != 0):
                    symbol = ARROWS[(dx, dy)]
                    self.default_fg = unit.color
                    self.put_char(cx + dx, cy + dy, symbol)
            elif unit is engine.world.player:
                label = LOADING_SYMBOLS[self.animation_tick] + ' ' + unit.current_action.label
                self.print(cx, cy - 1, label, tcod.white, alignment=tcod.CENTER)
                self.animation_tick += 1
                if self.animation_tick > len(LOADING_SYMBOLS) - 1:
                    self.animation_tick = 0

        if not engine.world.player.alive:
            cx, cy = self.map_pos_to_screen(engine.world.player.pos)
            self.print(cx, cy+1, 'You died', tcod.red, alignment=tcod.CENTER)

    def move_to(self, x, y=None):
        if y is None:
            x, y = x.x, x.y if type(x) is MapPos else x
        self.center_x, self.center_y = x, y
        self.render()

    def move(self, dx, dy):
        self.center_x += dx
        self.center_y += dy
        self.render()
