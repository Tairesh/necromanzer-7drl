import engine
from engine.chunk import CHUNK_SIZE
from engine.drawable import Drawable
import tcod.random

from engine.tile import wall


class BoxParams:
    def __init__(self, title='', main_color=tcod.chartreuse, border_type='line',
                 title_color=tcod.black, title_bg=tcod.chartreuse, title_pos='left'):
        self.title = title
        self.main_color = main_color
        self.border_type = border_type
        self.title_color = title_color
        self.title_bg = title_bg
        self.title_pos = title_pos


class Box(Drawable):
    def __init__(self, pos, width, height, params: BoxParams = BoxParams()):
        self.params = params
        Drawable.__init__(self, pos, width, height)
        self._draw_border()

    def _draw_border(self):
        self.draw_frame(0, 0, self.width, self.height, self.params.title, fg=self.params.main_color,
                        border_type=self.params.border_type, title_fg=self.params.title_color,
                        title_bg=self.params.title_bg, title_pos=self.params.title_pos)


class TileInfoBox(Box):

    def __init__(self, tile):
        self.tile = tile
        super().__init__(self._calc_screen_pos(), self._calc_width(), 3+len(tile.items)+len(tile.units), BoxParams(f"{tile.pos.x}x{tile.pos.y}"))
        self.render()

    def _calc_screen_pos(self):
        from screens import maingamescreen
        view = maingamescreen.drawables['view']
        screen_x, screen_y = view.map_pos_to_screen(self.tile.pos)
        return screen_x + 1, screen_y + 1

    def _calc_width(self) -> int:
        max_item = max(self.tile.items, key=lambda item: len(item.title), default=None)
        max_unit = max(self.tile.units, key=lambda unit: len(unit.displayed_name), default=None)
        return 6 + max(len(self.tile.base.title),
                       len(max_item.title) if max_item else 0,
                       len(max_unit.displayed_name) if max_unit else 0)

    def set_tile(self, tile):
        self.tile = tile
        self.render()
        self.set_pos(self._calc_screen_pos())

    def render(self):
        self.clear()
        self._draw_border()

        symbol = self.tile.base.symbol
        if symbol == 'wall':
            symbol = wall(self.tile)
        if type(symbol) is int:
            self.default_fg = self.tile.base.color
            self.put_char(2, 1, symbol)
        else:
            self.print(2, 1, symbol, self.tile.base.color)
        self.print(4, 1, self.tile.base.title, tcod.white, None)

        for i, item in enumerate(self.tile.items):
            if type(item.symbol) is int:
                self.default_fg = item.color
                if item.bg_color:
                    self.default_bg = item.bg_color
                self.put_char(2, 2 + i, item.symbol, bg_blend=tcod.BKGND_SET)
            else:
                self.print(2, 2 + i, item.symbol, item.color, item.bg_color, bg_blend=tcod.BKGND_SET)
            self.print(4, 2+i, item.displayed_name, tcod.white, None)

        y = 2+len(self.tile.items)
        for i, unit in enumerate(self.tile.units):
            if type(unit.symbol) is int:
                self.default_fg = unit.color
                self.put_char(2, y+i, unit.symbol)
            else:
                self.print(2, y+i, unit.symbol, unit.color)
            self.print(4, y+i, unit.displayed_name, unit.color)


class ItemsAround(Box):

    def __init__(self):
        self.items = []
        for i in range(engine.world.player.pos.x - 50, 100):
            for j in range(engine.world.player.pos.y - 30, 60):
                tile = engine.world.get_tile(i, j, change=False)
                for item in tile.items:
                    if item.is_grabable:
                        self.items.append((item, tile.pos))
        self.items = sorted(self.items, key=lambda row: row[1].dist_to(engine.world.player.pos))
        self.items = self.items[:engine.console.height-4]
        self.selected = 0 if len(self.items) else None
        super().__init__((engine.console.width, 0, 'right', 'top'), 20, engine.console.height-2,
                         BoxParams(" Items around: "))
        self.render()

    def render(self):
        self.default_fg = tcod.white
        self.default_bg = tcod.black
        self.clear()
        self._draw_border()
        for i, (item, pos) in enumerate(self.items):
            if self.selected is not None and i == self.selected:
                self.draw_rect(1, 1+i, self.width-2, 1, 0, bg=tcod.darkest_green)
            elif i % 2 == 1:
                self.draw_rect(1, 1+i, self.width-2, 1, 0, bg=tcod.darkest_gray)
            if type(item.symbol) is str:
                self.print(1, 1+i, item.symbol, item.color)
            else:
                self.default_fg = item.color
                self.put_char(1, 1+i, item.symbol)
            self.print(3, 1+i, item.displayed_name)

    def down(self):
        if len(self.items) == 0:
            return
        if self.selected is not None and self.selected < len(self.items) - 1:
            self.selected += 1
        else:
            self.selected = 0
        self.render()

    def up(self):
        if len(self.items) == 0:
            return
        if self.selected is not None and self.selected > 0:
            self.selected -= 1
        else:
            self.selected = len(self.items) - 1
        self.render()
