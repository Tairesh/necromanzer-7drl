import tcod
import tcod.event
import engine
from . import Drawable
from .boxes import BoxParams, Box
from typing import Optional


class Interactive(Drawable):
    is_interactive = True
    is_textinput = False

    def __init__(self, pos, width, height, click=lambda btn: None, keys=()):
        self.hover = False    # mouse over
        self.pressed = False  # mouse click or key pressed (tmp state ends when button released)
        self.focused = False  # keyboard selected
        self.click_lambda = click
        self.keys = keys

        super().__init__(pos, width, height)
        self.render()

    def key_pressed(self, event):
        return self.click_lambda(self)

    def click(self, pos):
        return self.click_lambda(self)

    def on_mouse_pressed(self, pos):
        if not self.pressed:
            self.pressed = True
            self.render()

    def off_mouse_pressed(self, pos):
        if self.pressed:
            self.pressed = False
            self.render()

    def on_pressed(self, key):
        if not self.pressed:
            self.pressed = True
            self.render()

    def off_pressed(self, key):
        if self.pressed:
            self.pressed = False
            self.render()

    def on_focused(self):
        if not self.focused:
            self.focused = True
            self.render()

    def off_focused(self):
        if self.focused:
            self.focused = False
            self.render()

    def on_hover(self, pos):
        if not self.hover:
            self.hover = True
            self.render()

    def off_hover(self, pos):
        if self.hover:
            self.hover = False
            self.render()

    def call(self, key, mouse):
        pass

    def update(self):
        pass


class InteractiveSet(Interactive):

    PADDING_HORIZONTAL = 1
    PADDING_VERTICAL = 0

    def __init__(self, pos, entities: list, entities_positions='horizontal', box_params: Optional[BoxParams] = None,
                 min_width=0, entities_centered=False):
        self.entities = entities
        self.entities_positions = entities_positions
        self.entities_centered = entities_centered
        self.selected_entity = None
        self.box_params = box_params
        self.autofocus_setted = False
        super().__init__(pos, max(self._calc_width(), min_width), self._calc_height(), keys=self._all_keys())
        self._positionate_entities()
        self.render()
        for entity in self.entities:
            entity.parent = self

    def _all_keys(self):
        keys = {tcod.event.SCANCODE_LEFT, tcod.event.SCANCODE_RIGHT,
                tcod.event.SCANCODE_UP, tcod.event.SCANCODE_DOWN,
                tcod.event.SCANCODE_KP_2, tcod.event.SCANCODE_KP_4, tcod.event.SCANCODE_KP_6, tcod.event.SCANCODE_KP_8,
                tcod.event.SCANCODE_ESCAPE, tcod.event.SCANCODE_RETURN, tcod.event.SCANCODE_KP_ENTER}
        for entity in self.entities:
            if entity.is_interactive and not entity.is_textinput and entity.keys:
                keys |= entity.keys
        return keys

    def _positionate_entities(self):
        x = self.pos.left
        y = self.pos.top
        if self.box_params:
            x += 1
            y += 1
        if self.entities_positions == 'horizontal':
            if self.entities_centered:
                x += (self.width - self._calc_width()) // 2
            for entity in self.entities:
                entity.set_pos((x, y, 'left', 'top'))
                x += entity.width + self.PADDING_HORIZONTAL
        elif self.entities_positions == 'vertical':
            for entity in self.entities:
                kx = (self.width - (2 if self.box_params else 0)) // 2 - entity.width // 2 if self.entities_centered else 0
                entity.set_pos((x+kx, y, 'left', 'top'))
                y += entity.height + self.PADDING_VERTICAL
        elif self.entities_positions in {'horizontal_custom', 'vertical_custom'}:
            for entity in self.entities:
                entity.set_pos((x+entity.pos.left, y+entity.pos.top, 'left', 'top'))

    def _calc_width(self):
        border_width = 2 if self.box_params else 0
        if self.entities_positions == 'horizontal':
            w = sum(map(lambda e: e.width, self.entities))
            return w + (self.PADDING_HORIZONTAL*(len(self.entities) - 1)) + border_width
        elif self.entities_positions == 'vertical':
            w = max(self.entities, key=lambda e: e.width).width
            return w + border_width
        else:
            min_x = min(self.entities, key=lambda e: e.pos.left).pos.left
            max_x = max(self.entities, key=lambda e: e.pos.right).pos.right
            return max_x - min_x

    def _calc_height(self):
        border_width = 2 if self.box_params else 0
        if self.entities_positions == 'horizontal':
            h = max(self.entities, key=lambda e: e.height).height
            return h + border_width
        elif self.entities_positions == 'vertical':
            h = sum(map(lambda e: e.height, self.entities))
            return h + (self.PADDING_VERTICAL*(len(self.entities) - 1)) + border_width
        else:
            min_y = min(self.entities, key=lambda e: e.pos.top).pos.top
            max_y = max(self.entities, key=lambda e: e.pos.bottom).pos.bottom
            return max_y - min_y

    def on_hover(self, pos):
        for entity in [o for o in self.entities if o.is_interactive]:
            if entity.collide(pos):
                entity.on_hover(pos)
            else:
                entity.off_hover(pos)

        self.render()

    def off_hover(self, pos):
        for entity in [o for o in self.entities if o.is_interactive]:
            entity.off_hover(pos)

        self.render()

    def get_selected_entity(self):
        if self.selected_entity is not None:
            return self.entities[self.selected_entity]
        else:
            return None

    def _change_selected(self, delta: int):
        last = len(self.entities)-1
        if self.selected_entity is None:
            self.selected_entity = last + 1
        else:
            self._set_selected_entity_unfocused()
        new_selected = self.selected_entity
        while True:
            new_selected += delta
            if new_selected < 0:
                new_selected = last
            if new_selected > last:
                new_selected = 0

            if new_selected == self.selected_entity:
                break
            if not self.entities[new_selected].disabled:
                break
        self.selected_entity = new_selected
        self._set_selected_entity_focused()

    def prev(self):
        self._change_selected(-1)

    def next(self):
        self._change_selected(+1)

    def autofocus(self, n=0):
        self.selected_entity = n
        self._change_selected(0)

    def _set_selected_entity_unfocused(self):
        self.get_selected_entity().focused = False
        self.get_selected_entity().render()
        if self.get_selected_entity() is engine.interface.focused_input:
            engine.interface.focused_input = None

    def _set_selected_entity_focused(self):
        self.get_selected_entity().focused = True
        self.get_selected_entity().render()
        if self.get_selected_entity().is_textinput:
            engine.interface.focused_input = self.get_selected_entity()

    def on_pressed(self, key):
        keymapped = [o for o in self.entities if o.is_interactive and key in o.keys and not o.is_textinput]
        flag = False
        for entity in keymapped:
            entity.on_pressed(key)
            flag = True
        if not flag:
            if self.entities_positions in {'horizontal', 'horizontal_custom'}:
                if key in {tcod.event.SCANCODE_LEFT, tcod.event.SCANCODE_KP_4}:
                    self.prev()
                elif key in {tcod.event.SCANCODE_RIGHT, tcod.event.SCANCODE_KP_6}:
                    self.next()
            elif self.entities_positions in {'vertical', 'vertical_custom'}:
                if key in {tcod.event.SCANCODE_UP, tcod.event.SCANCODE_KP_8}:
                    self.prev()
                elif key in {tcod.event.SCANCODE_DOWN, tcod.event.SCANCODE_KP_2}:
                    self.next()

            if key in {tcod.event.SCANCODE_RETURN, tcod.event.SCANCODE_KP_ENTER} and self.selected_entity is not None:
                self.get_selected_entity().on_pressed(key)
            elif key == tcod.event.SCANCODE_ESCAPE:
                if self.selected_entity is not None:
                    self.get_selected_entity().focused = False
                    self.get_selected_entity().render()
                    self.selected_entity = None
        self.render()

    def off_pressed(self, key):
        keymapped = [o for o in self.entities if o.is_interactive and key in o.keys and not o.is_textinput]
        flag = False
        for entity in keymapped:
            entity.off_pressed(key)
            flag = True
        if not flag:
            if key in {tcod.event.SCANCODE_RETURN, tcod.event.SCANCODE_KP_ENTER} and self.selected_entity is not None:
                self.get_selected_entity().off_pressed(key)
                self.get_selected_entity().key_pressed(None)
        self.render()

    def on_mouse_pressed(self, pos):
        for entity in [o for o in self.entities if o.is_interactive]:
            if entity.collide(pos):
                entity.on_mouse_pressed(pos)
        self.render()

    def off_mouse_pressed(self, pos):
        for entity in [o for o in self.entities if o.is_interactive]:
            if entity.pressed:
                entity.off_mouse_pressed(pos)
        self.render()

    def click(self, pos):
        for entity in [o for o in self.entities if o.is_interactive]:
            if entity.collide(pos):
                entity.click(pos)
        self.render()

    def key_pressed(self, event):
        keymapped = [o for o in self.entities if o.is_interactive and event.scancode in o.keys and not o.is_textinput]
        for entity in keymapped:
            entity.key_pressed(event)
        self.render()

    def render(self):
        self.clear()
        if self.box_params:
            box = Box(None, self.width, self.height, self.box_params)
            box.blit(self, 0, 0)

        x = 1 if self.box_params else 0
        y = 1 if self.box_params else 0
        if self.entities_positions == 'horizontal':
            if self.entities_centered:
                x += (self.width - self._calc_width()) // 2
            for entity in self.entities:
                entity.blit(self, x, y)
                x += entity.width + self.PADDING_HORIZONTAL
        elif self.entities_positions == 'vertical':
            for entity in self.entities:
                kx = (self.width - (2 if self.box_params else 0)) // 2 - entity.width // 2 if self.entities_centered else 0
                entity.blit(self, x+kx, y)
                y += entity.height + self.PADDING_VERTICAL
        else:
            for entity in self.entities:
                if entity.pos is None:
                    continue
                entity.blit(self, entity.pos.left-self.pos.left, entity.pos.top-self.pos.top)
        super().render()

    def set_selected_entity(self, entity):
        for i, e in enumerate(self.entities):
            if e is entity:
                self.selected_entity = i
                break
        self._set_selected_entity_focused()
