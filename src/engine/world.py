import random

from brains import HumanBrain
from tools.animals import random_daemon_dog, random_dog
from .body import HumanBody, ChildBody
from .chunk import CHUNK_SIZE, Chunk
from .orders import Order, OrderProto
from .tile import Tile, MapPos
from .unit import Unit
import worldgen
from typing import Dict, Optional
from datetime import datetime


class World:

    def __init__(self, seed):
        self.seed = seed
        self.structures = worldgen.structures_by_seed(seed)
        self.chunks: Dict[str, Chunk] = {}
        self.current_tick: int = 0
        self.units: Dict[int, Unit] = {}
        self.log = []
        self.file_name = None
        self.order: Order = Order(OrderProto.NONE)
        self.village_name = None
        self.historical_persons = {}
        self.history = {}

    def generate_units(self, name: str, persons: dict, history: dict):
        self.units = {1: self.player}
        self.village_name = name
        self.historical_persons = persons
        self.history = history

        for person in [person for person in persons.values() if not person.death]:
            chunk = random.choice(self.get_houses_chunks())
            pos = chunk[0] * CHUNK_SIZE + CHUNK_SIZE//2, chunk[1] * CHUNK_SIZE + CHUNK_SIZE//2
            while not self.get_tile(pos[0], pos[1], change=False).is_walkable:
                pos = chunk[0] * CHUNK_SIZE + random.randint(0, CHUNK_SIZE),\
                      chunk[1] * CHUNK_SIZE + random.randint(0, CHUNK_SIZE)

            unit_data = person.data
            unit_data["alive"] = True
            unit_data["body_class"] = HumanBody if 330 - unit_data['born'] >= 16 else ChildBody
            unit_data["brain"] = (HumanBrain.proto, None)
            villager = Unit(pos, unit_data)

            self.add_unit(villager)
            if random.random() < 0.1:
                free_pos = self.get_tile(pos[0], pos[1], False).free_pos_around()
                if free_pos:
                    dog = random_dog(pos)
                    self.add_unit(dog)

        houses = self.get_houses_chunks()
        min_x, min_y = 0, 0
        max_x, max_y = 10, 10
        for hx, hy in houses:
            if hx < min_x:
                min_x = hx
            if hx > max_x:
                max_x = hx
            if hy < min_y:
                min_y = hy
            if hy > max_y:
                max_y = hy
        for i in range(10):
            if random.random() > 0.5:
                x = min_x - random.randint(1, 3)
            else:
                x = max_x + random.randint(1, 3)

            if random.random() > 0.5:
                y = min_y - random.randint(1, 3)
            else:
                y = max_y + random.randint(1, 3)

            pos = x * CHUNK_SIZE + CHUNK_SIZE // 2, y * CHUNK_SIZE + CHUNK_SIZE // 2
            while not self.get_tile(pos[0], pos[1], change=False).is_walkable:
                pos = x * CHUNK_SIZE + random.randint(0, CHUNK_SIZE), \
                      y * CHUNK_SIZE + random.randint(0, CHUNK_SIZE)
            daemon = random_daemon_dog(pos)
            self.add_unit(daemon)

    def get_houses_chunks(self):
        from worldgen import Structure
        return self.structures[Structure.HOUSE]

    @property
    def player(self):
        return self.units[1]

    @property
    def current_year(self):
        return self.current_date.year - (1970-330)

    @property
    def current_date(self) -> datetime:
        return datetime.fromtimestamp(self.current_tick+60*60*24*31*6)

    def add_unit(self, unit: Unit, unit_id: int = None):
        if unit_id is None:
            unit_id = len(self.units) + 1
        unit.id = unit_id
        self.units[unit_id] = unit
        self.get_tile(unit.pos.x, unit.pos.y).units.append(unit)

    def get_chunk(self, chunk_x, chunk_y):
        chunk_key = f"{chunk_x}x{chunk_y}"
        if chunk_key not in self.chunks:
            self.chunks[chunk_key] = worldgen.generate_chunk(chunk_x, chunk_y, self.seed, self.structures)
        return self.chunks[chunk_key]

    def get_tile(self, x, y=None, change=True, if_existed=False) -> Optional[Tile]:
        if type(x) is MapPos:
            x, y = x.x, x.y
        elif y is None:
            x, y = x

        chunk_x, chunk_y = x // CHUNK_SIZE, y // CHUNK_SIZE
        if if_existed:
            chunk_key = f"{chunk_x}x{chunk_y}"
            if chunk_key not in self.chunks:
                return None

        chunk = self.get_chunk(chunk_x, chunk_y)
        if change:
            chunk.has_changed = True
            # print("Changed chunk "+str(chunk_x) + 'x' + str(chunk_y))

        chunk_left, chunk_top = chunk_x * CHUNK_SIZE, chunk_y * CHUNK_SIZE
        pos_in_chunk = (x - chunk_left) * CHUNK_SIZE + (y - chunk_top)
        return chunk.tiles[pos_in_chunk]

    @property
    def active_units(self):
        return [unit for unit in self.units.values() if unit.alive and unit.pos.dist_to(self.player.pos) < 50]

    def act(self):
        for unit in self.active_units:
            if unit.current_action:
                if not unit.alive:
                    unit.current_action = None
                elif unit.current_action.end_tick <= self.current_tick:
                    unit.current_action.act()
                    if unit is self.player:
                        unit.stamina -= unit.current_action.stamina
                    unit.current_action = None
            if unit.brain and unit.alive:
                unit.brain.plan()

    def tick(self):
        self.current_tick += 1
        self.act()
        if self.player.alive:
            self.player.stamina += 0.1
            if self.player.stamina > self.player.max_stamina:
                self.player.stamina = self.player.max_stamina
