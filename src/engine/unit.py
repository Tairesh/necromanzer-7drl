import json

import tcod

import engine
from engine import unit_actions, consts
from .brain import Brain, BrainProto, PlayerBrain
from .item import Item
from .tile import MapPos
from engine.body import generate_new, Body

SKIN_TONES = ((0xF7, 0xEB, 0xDB), (0xF3, 0xC6, 0xA4), (0xED, 0xB9, 0x8A), (0x97, 0x5B, 0x43))


class Unit:

    brain = None
    alive = False
    max_stamina = 10

    def __init__(self, pos, raw_params):
        self.id = 0
        self.pos = MapPos(pos) if pos else None
        self.current_action = None
        self.raw_params = raw_params
        self.body = raw_params['body'] if 'body' in raw_params else generate_new(raw_params['body_class'])
        if 'wield' in raw_params:
            self.wield = raw_params['wield']
        else:
            self.wield = {}
            for part_key in self.body.parts_can_grab():
                self.wield[part_key] = None
        if 'alive' in raw_params:
            self.alive = raw_params['alive']
        if 'brain' in raw_params and raw_params['brain']:
            brain_proto, brain_data = raw_params['brain']
            brain_class = consts.GAME_DATA['brains'][brain_proto]
            self.brain = brain_class(self, brain_data)

        if 'inventory' in raw_params:
            self.inventory = raw_params['inventory']
        else:
            self.inventory = []

        if 'stamina' in raw_params:
            self.stamina = raw_params['stamina']
        else:
            self.stamina = float(self.max_stamina)
        if 'walkmode' in raw_params:
            self.walkmode = raw_params['walkmode']
        else:
            self.walkmode = 1

    def wield_something_with_flag(self, flag) -> bool:
        for item in self.wield.values():
            if item and flag in item.flags:
                return True
        for part in self.body.parts.values():
            if part and flag in part.flags:
                return True
        return False

    def can_move_to(self, x, y=None):
        tile = engine.world.get_tile(x, y, False)
        return tile.is_walkable

    def teleport_to(self, pos, pos_y=None):
        if pos_y is not None:
            pos = (pos, pos_y)

        engine.world.get_tile(self.pos.x, self.pos.y).units.remove(self)
        self.pos = pos if type(pos) is MapPos else MapPos(*pos)
        engine.world.get_tile(self.pos.x, self.pos.y).units.append(self)

    def serialize(self) -> str:
        unit_data = self.raw_params
        unit_data['x'] = self.pos.x
        unit_data['y'] = self.pos.y
        if 'body_class' in unit_data:
            unit_data.pop('body_class')
        wield = {}
        for key, item in self.wield.items():
            wield[key] = item.serialize() if item else None
        unit_data['wield'] = wield
        unit_data['body'] = self.body.serialize()
        unit_data['alive'] = self.alive
        unit_data['brain'] = self.brain.serialize() if self.brain and hasattr(self.brain, 'proto') else None
        items_data = []
        for item in self.inventory:
            items_data.append(item.serialize())
        unit_data['inventory'] = items_data
        unit_data['stamina'] = self.stamina
        unit_data['walkmode'] = self.walkmode
        return json.dumps(unit_data, separators=(',', ':'))

    @staticmethod
    def deserialize(value, is_player=False):
        raw_params = json.loads(value)
        raw_params['body'] = Body.deserialize(raw_params['body'])
        wield = {}
        for key, val in raw_params['wield'].items():
            wield[key] = Item.deserialize(val)
        raw_params['wield'] = wield
        raw_params['inventory'] = [Item.deserialize(val) for val in raw_params['inventory']]
        raw_params['brain'] = Brain.deserialize(raw_params['brain'])

        if is_player:
            unit = Player((raw_params['x'], raw_params['y']), raw_params)
        else:
            unit = Unit((raw_params['x'], raw_params['y']), raw_params)
        return unit

    @property
    def name_apostrophe_s(self):
        return self.name + '`s'

    @property
    def age(self):
        last_lived = self.raw_params['death'] if 'death' in self.raw_params and self.raw_params['death'] else engine.world.current_year
        return last_lived - self.raw_params['born']

    @property
    def z_index(self):
        return 1 if self.alive else 0

    @property
    def critical_bodyparts(self) -> tuple:
        if self.alive:
            if not self.brain or self.brain.need_head:
                return 'head', 'torso'
        return 'torso',

    @property
    def speed(self) -> int:
        legs_count = len(self.body.parts_can_move())
        if legs_count == 0:
            speed = 10
        elif legs_count == 1:
            speed = 5
        elif legs_count == 2:
            speed = 1
        else:
            speed = 1 / (legs_count-1)
        if self.brain:
            speed = int(speed * self.brain.speed_factor)
        if speed < 1:
            speed = 1
        return speed

    def start_dig(self, pos):
        if self.stamina >= unit_actions.Dig.stamina:
            self.current_action = unit_actions.Dig(self, pos)
        else:
            engine.log.add_line(f"{self.displayed_name} can't dig, too tired!", tcod.orange)

    def start_activate(self, item, action: str):
        self.current_action = unit_actions.ActivateItem(self, item, action)

    def start_animate(self, target):
        if self.stamina >= unit_actions.Animate.stamina:
            self.current_action = unit_actions.Animate(self, target)
        else:
            engine.log.add_line(f"{self.displayed_name} can't use magical abilities, too tired!", tcod.orange)

    def start_smash(self, pos, part_key: str):
        if self.stamina >= unit_actions.Smash.stamina:
            self.current_action = unit_actions.Smash(self, pos, part_key)
        else:
            engine.log.add_line(f"{self.displayed_name} can't smash, too tired!", tcod.orange)

    def start_move(self, dx, dy):
        if self.stamina >= unit_actions.Move.stamina:
            self.current_action = unit_actions.Move(self, (dx, dy))
        else:
            engine.log.add_line(f"{self.displayed_name} can't run, too tired!", tcod.orange)

    def start_slow_move(self, dx, dy):
        self.current_action = unit_actions.WalkMove(self, (dx, dy))

    def die(self):
        self.alive = False
        self.brain = None
        if 'death' not in self.raw_params or not self.raw_params['death']:
            self.raw_params['death'] = engine.world.current_year
        engine.log.add_line(f"{self.name} dies!", tcod.light_red)
        # if self is engine.world.player:
        #     print('game over')

    @property
    def color(self):
        if self.body.permacolor:
            return self.body.permacolor
        return SKIN_TONES[self.raw_params['skin_tone']]

    @property
    def is_undead(self) -> bool:
        return self.brain and not self.brain.need_head

    @property
    def live_status(self):
        if self.alive:
            return 'undead' if self.is_undead else ''
        else:
            return 'dead'

    @property
    def displayed_name(self):
        if self.race == 'dog':
            if self.is_undead:
                return f"{self.name} ({self.live_status} dog)"
            return self.name + ' (' + ('dead ' if not self.alive else '') + 'dog)'
        if self.is_undead:
            return self.name + ' (' + self.live_status + ' ' + self.profession + ')'
        return self.name + ' (' + ('dead ' if not self.alive else '') + self.profession + ')'

    @property
    def profession(self):
        if 'profession' in self.raw_params and self.raw_params['profession']:
            return self.raw_params['profession']
        return 'child' if self.age < 16 else 'villager'

    @property
    def name(self):
        return self.raw_params['name']

    @property
    def is_walkable(self):
        return not self.alive

    @property
    def race(self):
        if 'race' in self.raw_params:
            return self.raw_params['race']
        return None


    @property
    def symbol(self):
        if self.race == 'dog':
            return 'd'

        if self.is_undead:
            return 'Z' if self.age >= 16 else 'z'

        if self.alive:
            return 1
        else:
            return 2

    @property
    def bg_color(self):
        if not self.alive and not self.body.permacolor:
            return tcod.red
        return None


class Player(Unit):
    symbol = 64
    z_index = 999
    alive = True

    def __init__(self, pos, raw_params):
        super().__init__(pos, raw_params)
        self.brain = PlayerBrain(self)

    @property
    def color(self):
        return SKIN_TONES[self.raw_params['skin_tone']]

    @property
    def name(self):
        return self.raw_params['name']

    @property
    def displayed_name(self):
        return "You"

    name_apostrophe_s = "your"

    def moving_keys_process(self, dx, dy) -> bool:
        if self.current_action:
            return False

        if dx == 0 and dy == 0:
            return False

        if not self.can_move_to(self.pos.x + dx, self.pos.y + dy):
            return False

        if self.walkmode == 0:
            self.start_move(dx, dy)
        else:
            self.start_slow_move(dx, dy)
        return True
