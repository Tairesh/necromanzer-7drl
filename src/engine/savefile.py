import random

from tools import humans
from tools.animals import random_dog, random_daemon_dog
from worldgen.history import Person
from .orders import Order
from .world import World
from .unit import Player, Unit
from .chunk import Chunk, CHUNK_SIZE
from .body import HumanBody

START_POSITION = (-10, -10)


def create(new_game_params: dict) -> World:
    world = World(new_game_params['seed'])
    player = Player(START_POSITION, {
        'name': new_game_params['name'],
        'race': 'human',
        'gender': new_game_params['gender'],
        'preferred_hand': new_game_params['preferred_hand'],
        'skin_tone': new_game_params['skin_tone'],
        'body_class': HumanBody,
        'born': 300-new_game_params['age'],
    })
    world.add_unit(player, 1)
    world.file_name = f"save/{world.player.raw_params['name']}.save"
    return world


def load(file_name: str) -> World:
    file = open(file_name, 'r')
    file_lines = file.readlines()
    file.close()
    meta = file_lines.pop(0).replace('\n', '').split(';')
    seed = meta[0]
    current_tick = int(meta[1])
    order = Order.deserialize(meta[2])
    name = meta[3]

    world = World(seed)
    world.file_name = file_name
    world.current_tick = current_tick
    world.order = order
    world.village_name = name

    line = file_lines.pop(0)
    while line != '/log\n':
        text, color = line[:-1].split(';')
        color = tuple(map(int, color.split(',')))
        world.log.append((text, color))
        line = file_lines.pop(0)

    line = file_lines.pop(0)
    while line != '/chunks\n':
        chunk = Chunk.deserialize(line[:-1])
        chunk.has_changed = True
        world.chunks[f"{chunk.chunk_coords[0]}x{chunk.chunk_coords[1]}"] = chunk
        line = file_lines.pop(0)

    line = file_lines.pop(0)
    while line != '/units\n':
        unit_id, unit_data = line[:-1].split('!')
        unit_id = int(unit_id)

        unit = Unit.deserialize(unit_data, unit_id == 1)
        world.add_unit(unit, unit_id)
        line = file_lines.pop(0)

    line = file_lines.pop(0)
    while line != '/history\n':
        year, log = line.replace('\n', '').split('/')
        world.history[int(year)] = log.split('%')
        line = file_lines.pop(0)

    line = file_lines.pop(0)
    while line != '/histpersons\n':
        person_id, person_data = line[:-1].split('!')
        person_id = int(person_id)
        person = Person.deserialize(person_data)
        world.historical_persons[person_id] = person
        line = file_lines.pop(0)

    print(f"Loaded {len(world.chunks)} chunks and {len(world.units)} units from {file_name}")
    return world


def save(world: World):

    file_lines = [
        world.seed + ';' + str(world.current_tick) + ';' + world.order.serialize() + ';' + world.village_name
    ]
    for text, color in world.log:
        if not color:
            color = (255, 255, 255)
        file_lines.append(text + ';' + ','.join(map(str, color)))
    file_lines.append('/log')

    for chunk in world.chunks.values():
        if chunk.has_changed:
            file_lines.append(chunk.serialize())
    file_lines.append('/chunks')

    for unit_id, unit in world.units.items():
        file_lines.append(str(unit_id) + '!' + unit.serialize())
    file_lines.append('/units')

    for year in world.history:
        file_lines.append(str(year) + '/' + '%'.join(world.history[year]))
    file_lines.append('/history')

    for person_id, person in world.historical_persons.items():
        file_lines.append(str(person_id) + '!' + person.serialize())
    file_lines.append('/histpersons')

    file = open(world.file_name, 'w')
    for line in file_lines:
        file.write(line + '\n')
    file.close()

    print(f"Saved {len(world.chunks)} chunks and {len(world.units)} units in {world.file_name}")
