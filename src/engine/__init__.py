import tcod
from tcod.console import Console
from . import consts, savefile
from .settings import load as load_settings
from .interface import handle_events, flush
from .world import World
from .log import Log
import screens.mainmenu

# noinspection PyTypeChecker
console: Console = None
current_screen = screens.mainmenu
settings: dict = {}
# noinspection PyTypeChecker
world: World = None
log: Log = Log()


def change_screen(screen):
    global current_screen

    current_screen = screen
    console.default_bg = tcod.black
    console.default_fg = tcod.white
    console.clear()
    screen.init()


def init():
    global settings, console

    settings = load_settings()
    consts.load_data()
    tcod.console_set_custom_font(consts.TILESET, consts.TILESET_FLAGS)
    console = tcod.console_init_root(settings['width'], settings['height'], consts.WINDOW_TITLE,
                                     renderer=tcod.RENDERER_OPENGL2, order='F', vsync=True)
    tcod.sys_set_fps(100)
    current_screen.init()


def loop():
    while not tcod.console_is_window_closed():
        current_screen.tick()
        flush()
        handle_events()


def stop():
    if world:
        savefile.save(world)
    raise SystemExit()
