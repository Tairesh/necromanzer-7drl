import engine
from engine.item import ItemProto
from tools.butchering import cut_off_body_part, replace_body_part, insert_body_part
from tools.damage import smash_tile
from tools.graveyard import spawn_dead_body_from_gravestone
from tools.magic import animate_dead_body


class UnitAction:

    length = 1
    stamina = 0
    label = 'some action'

    def __init__(self, unit):
        self.unit = unit
        self.end_tick = engine.world.current_tick + self.length

    def act(self):
        pass


class Move(UnitAction):

    label = 'moving'
    stamina = 0.2

    @property
    def length(self):
        return self.unit.speed

    def __init__(self, unit, delta):
        self.dx, self.dy = delta
        super().__init__(unit)

    def act(self):
        new_pos = self.unit.pos.x+self.dx, self.unit.pos.y+self.dy
        # print("moved to", new_pos)
        if self.unit.can_move_to(new_pos):
            self.unit.teleport_to(new_pos)


class WalkMove(UnitAction):

    label = 'walking'
    stamina = 0.05

    @property
    def length(self):
        return self.unit.speed * 2 + 1

    def __init__(self, unit, delta):
        self.dx, self.dy = delta
        super().__init__(unit)

    def act(self):
        new_pos = self.unit.pos.x+self.dx, self.unit.pos.y+self.dy
        # print("moved to", new_pos)
        if self.unit.can_move_to(new_pos):
            self.unit.teleport_to(new_pos)


class ActivateItem(UnitAction):

    stamina = 0

    @property
    def label(self):
        return '{} the {}'.format(self.action, self.item.title)

    @property
    def length(self):
        return self.item.action_length(self.action, self.unit)

    def __init__(self, unit, item, action):
        self.item = item
        self.action = action
        super().__init__(unit)

    def act(self):
        self.item.act(self.action, self.unit)


class Dig(UnitAction):

    label = "digging"
    length = 60
    stamina = 3

    def __init__(self, unit, pos):
        self.pos = pos
        super().__init__(unit)

    def act(self):
        tile = engine.world.get_tile(self.pos)
        tile.base = tile.base.dig_result
        for item in tile.items:
            if item.proto is ItemProto.GRAVESTONE:
                spawn_dead_body_from_gravestone(tile.pos, item)


class Smash(UnitAction):

    label = "smashing"
    stamina = 0.5

    @property
    def length(self):
        weapon = self.unit.wield[self.part_key] if self.part_key in self.unit.wield else None
        if weapon and weapon.attack_damage:
            return weapon.attack_length
        part = self.unit.body.parts[self.part_key]
        if part:
            return part.attack_length
        return 0

    def __init__(self, unit, pos, part_key):
        self.pos = pos
        self.part_key = part_key
        super().__init__(unit)

    def act(self):
        smash_tile(self.unit, self.pos, self.part_key)


class Animate(UnitAction):

    label = 'animate'
    length = 10
    stamina = 1

    def __init__(self, unit, target):
        self.target = target
        super().__init__(unit)

    def act(self):
        animate_dead_body(self.target)


class CutPartOff(UnitAction):

    label = 'cutting of body part'
    length = 5
    stamina = 0.5

    def __init__(self, unit, target_unit, part_key):
        self.target_unit = target_unit
        self.part_key = part_key
        super().__init__(unit)

    def act(self):
        cut_off_body_part(self.target_unit, self.part_key)


class ReplacePart(UnitAction):

    label = 'replacing body part'
    length = 20
    stamina = 1

    def __init__(self, unit, target_unit, part_key, item, pos):
        self.target_unit = target_unit
        self.part_key = part_key
        self.item = item
        self.pos = pos
        super().__init__(unit)

    def act(self):
        replace_body_part(self.target_unit, self.part_key, self.item, self.pos)


class InsertPart(UnitAction):

    label = 'inserting new body part'
    length = 20
    stamina = 1

    def __init__(self, unit, target_unit, part_key, item, pos):
        self.target_unit = target_unit
        self.part_key = part_key
        self.item = item
        self.pos = pos
        super().__init__(unit)

    def act(self):
        insert_body_part(self.target_unit, self.part_key, self.item, self.pos)
