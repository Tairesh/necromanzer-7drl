#!/bin/sh

rm -rf dist/*
venv/bin/python3 -O -m PyInstaller -F --distpath=dist/necromanzer_linux necromanzer.spec
cp res dist/necromanzer_linux -r
cd dist || exit
zip -r necromanzer_linux necromanzer_linux
